DROP DATABASE bookMe;
CREATE DATABASE bookMe;
USE bookMe;
create table role
(
	id int auto_increment
		primary key,
	role varchar(20) null,
	constraint role_role_uindex
		unique (role)
);

create table shops
(
	id int auto_increment
		primary key,
	shop_name varchar(50) not null,
	description varchar(200) null,
	constraint shops_shop_name_uindex
		unique (shop_name)
);

create table users
(
	id int auto_increment
		primary key,
	username varchar(30) not null,
	first_name varchar(20) not null,
	last_name varchar(20) null,
	encrypted_password varchar(250) not null,
	validated tinyint default 0 not null,
	email varchar(32) null,
	phone varchar(50) null,
	is_owner tinyint(1) default 0 not null,
	shop_id int null,
	constraint users_username_uindex
		unique (username),
	constraint users_shops_id_fk
		foreign key (shop_id) references shops (id)
);

create table appointments
(
	id int auto_increment
		primary key,
	worker_id int null,
	client_id int null,
	start date not null,
	end date not null,
	appointment_type varchar(20) null,
	constraint appointments_users_id_fk
		foreign key (client_id) references users (id),
	constraint appointments_users_id_fk_2
		foreign key (worker_id) references users (id)
);

create table employees
(
	employee_details_id int null,
	owner_id int null,
	constraint employees_employee_details_id_uindex
		unique (employee_details_id),
	constraint employees_users_id_fk
		foreign key (owner_id) references users (id),
	constraint employees_users_id_fk_2
		foreign key (owner_id) references users (id)
);

create index users_id_index
	on users (id);

create table users_roles
(
	users_id int null,
	roles_id int null,
	constraint users_roles_users_id_roles_id_pk
		unique (users_id, roles_id),
	constraint roles_fk
		foreign key (roles_id) references role (id),
	constraint users_fk
		foreign key (users_id) references users (id)
);

