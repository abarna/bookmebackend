package com.abarna.bookme.services;

import com.abarna.bookme.converters.EmployeeConverter;
import com.abarna.bookme.converters.WorkframeConverter;
import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.RoleEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.exceptions.GenericException;
import com.abarna.bookme.repositories.EmployeeRepository;
import com.abarna.bookme.repositories.RoleRepository;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.repositories.WorkframeRepository;
import com.abarna.bookme.util.ServiceUtils;
import com.abarna.bookme.vos.EmployeeVO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.abarna.bookme.enums.RolesEnum.ADMIN_ROLE;
import static com.abarna.bookme.enums.RolesEnum.EMPLOYEE_ROLE;
import static java.util.stream.Collectors.toList;

@Service
public class EmployeeService extends ServiceUtils {
    private final EmployeeRepository employeeRepository;
    private final EmployeeConverter employeeConverter;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;

    public EmployeeService(EmployeeRepository employeeRepository, EmployeeConverter employeeConverter,
                           RoleRepository roleRepository, UserRepository userRepository, WorkframeConverter workframeConverter, WorkframeRepository workframeRepository) {
        this.employeeRepository = employeeRepository;
        this.employeeConverter = employeeConverter;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    public void patchEmployee(EmployeeVO employeeVO, String token) {
        //TODO check if patches his employee and not from another shop
        UserEntity ownerEntityOptional = getUserEntityByToken(token);
        if (!ownerEntityOptional.getRoles().
                stream()
                .filter(roleEntity -> ADMIN_ROLE.equals(roleEntity.getRole()))
                .findAny().isPresent()) {
            throw new GenericException("Userul logat nu este owner.");
        }
        Optional<EmployeeEntity> employeeEntityOptional = employeeRepository.findById(employeeVO.getId());
        if (employeeEntityOptional.isPresent()) {
            EmployeeEntity employeeEntity = employeeEntityOptional.get();
            employeeEntity.getEmployeeDetails().setFirstName(employeeVO.getEmployeeDetails().getFirstName());
            employeeEntity.getEmployeeDetails().setLastName(employeeVO.getEmployeeDetails().getLastName());
            employeeEntity.getEmployeeDetails().setEmail(employeeVO.getEmployeeDetails().getEmail());
            employeeEntity.getEmployeeDetails().setPhone(employeeVO.getEmployeeDetails().getPhone());
            employeeRepository.save(employeeEntity);
        } else {
            throw new GenericException("Datele angajatului nu sunt valide.");
        }
    }

    public EmployeeVO getEmployee(String token) {
        UserEntity employeeEntity = getUserEntityByToken(token);

        if (employeeEntity.getRoles()
                .stream()
                .filter(roleEntity -> EMPLOYEE_ROLE.equals(roleEntity.getRole()))
                .findAny().isEmpty()) {
            throw new GenericException("Userul logat nu este un angajat.");
        }
        return employeeRepository.findById(employeeEntity.getId())
                .map(employeeConverter::toVO)
                .orElse(new EmployeeVO());

    }

    public List<EmployeeVO> getOwnerEmployees(String token) {
        UserEntity ownerEntity = getUserEntityByToken(token);
        throwExceptionIfNotOwner(ownerEntity);
        return employeeRepository.findByOwner_Id(ownerEntity.getId()).stream()
                .map(employeeConverter::toVO)
                .collect(toList());
    }

    public void addEmployee(EmployeeVO employeeVO, String token) {
        UserEntity ownerEntity = getUserEntityByToken(token);
        throwExceptionIfNotOwner(ownerEntity);

        if (userRepository.findByUsername(employeeVO.getEmployeeDetails().getUsername()).isPresent()) {
            throw new GenericException("Username-ul angajatului nu este valid.");
        }
        EmployeeEntity employeeEntity = employeeConverter.toEntity(employeeVO);
        employeeEntity.setOwner(ownerEntity);
        // TODO remove this when sending confirmation email
        employeeEntity.getEmployeeDetails().setValidated(true);

        Optional<RoleEntity> employeeRoleOptional = roleRepository.findByRole(EMPLOYEE_ROLE);
        if (employeeRoleOptional.isPresent()) {
            employeeEntity.getEmployeeDetails().getRoles().add(employeeRoleOptional.get());
        } else {
            RoleEntity employeeRole = new RoleEntity();
            employeeRole.setRole(EMPLOYEE_ROLE);
            roleRepository.save(employeeRole);
            employeeEntity.getEmployeeDetails().getRoles().add(employeeRole);
        }
        employeeRepository.save(employeeEntity);
    }

    public void deleteEmployee(int id, String token) {
        UserEntity ownerEntity = getUserEntityByToken(token);
        throwExceptionIfNotOwner(ownerEntity);
        if (ownerEntity.getRoles()
                .stream()
                .filter(roleEntity -> ADMIN_ROLE.equals(roleEntity.getRole()))
                .findAny().isEmpty()) {
            throw new GenericException("Userul logat nu este owner.");
        } else {
            Optional<EmployeeEntity> employeeToBeDeleted = employeeRepository.findById(id);
            if (employeeToBeDeleted.isEmpty() || ownerEntity.getId() != employeeToBeDeleted.get().getOwner().getId()) {
                throw new GenericException("Id-ul angajatului este invalid.");
            }
        }
        employeeRepository.deleteById(id);
    }
}
