package com.abarna.bookme.services;

import com.abarna.bookme.converters.AppointmentConverter;
import com.abarna.bookme.converters.AppointmentDetailsConverter;
import com.abarna.bookme.entities.AppointmentEntity;
import com.abarna.bookme.entities.ManualUsersEntity;
import com.abarna.bookme.entities.ManualUsersEntityBuilder;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.enums.AppointmentStatusType;
import com.abarna.bookme.exceptions.GenericException;
import com.abarna.bookme.repositories.AppointmentRepository;
import com.abarna.bookme.repositories.ManualUsersRepository;
import com.abarna.bookme.util.ServiceUtils;
import com.abarna.bookme.vos.AppointmentDetailsVO;
import com.abarna.bookme.vos.AppointmentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.abarna.bookme.entities.ManualUsersEntityBuilder.aManualUsersEntity;
import static com.abarna.bookme.enums.AppointmentStatusType.CONFIRMED;
import static com.abarna.bookme.enums.AppointmentStatusType.PENDING;
import static com.abarna.bookme.util.DateUtils.getNextTimeframe;

@Service
public class AppointmentService extends ServiceUtils {

    private final AppointmentRepository appointmentRepository;
    private final AppointmentConverter appointmentConverter;
    private final AppointmentDetailsConverter appointmentDetailsConverter;
    private final WorkframeService workframeService;
    @Autowired
    private ManualUsersRepository manualUsersRepository;

    public AppointmentService(AppointmentRepository appointmentRepository, AppointmentConverter appointmentConverter,
                              AppointmentDetailsConverter appointmentDetailsConverter, WorkframeService workframeService) {
        this.appointmentRepository = appointmentRepository;
        this.appointmentConverter = appointmentConverter;
        this.appointmentDetailsConverter = appointmentDetailsConverter;
        this.workframeService = workframeService;
    }


    public void addAppointment(AppointmentVO appointmentVO) {
        Date currentDate = new Date();
        if (appointmentVO.getDate().before(currentDate)) {
            throw new GenericException("Rezervarea nu poate fi facuta in trecut.");
        }
        Map<Date, Date> availableTimeframes = workframeService.getAvailabletimeframesWithEndDate(appointmentVO.getWorkerId(), appointmentVO.getAppointmentType(),
                appointmentVO.getDate());
        if (!availableTimeframes.containsKey(appointmentVO.getDate())) {
            throw new GenericException("Programarea s-a facut cu o perioada invalida.");
        } else {
            Date endDate = getNextTimeframe(appointmentVO.getDate(), appointmentVO.getAppointmentType());
            if (!availableTimeframes.get(appointmentVO.getDate()).equals(endDate)) {
                throw new GenericException("Acest tip de programare nu s-a putut face la ora solicitata");
            }
        }

        AppointmentEntity appointment = appointmentConverter.toEntity(appointmentVO);
        appointment.setStatus(PENDING);
        appointmentRepository.save(appointment);
    }

    public void addManualAppointment(AppointmentVO appointmentVO) {
        Date currentDate = new Date();
        if (appointmentVO.getDate().before(currentDate)) {
            throw new GenericException("Rezervarea nu poate fi facuta in trecut.");
        }
        Map<Date, Date> availableTimeframes = workframeService.getAvailabletimeframesWithEndDate(appointmentVO.getWorkerId(), appointmentVO.getAppointmentType(),
                appointmentVO.getDate());
        if (!availableTimeframes.containsKey(appointmentVO.getDate())) {
            throw new GenericException("Programarea s-a facut cu o perioada invalida.");
        } else {
            Date endDate = getNextTimeframe(appointmentVO.getDate(), appointmentVO.getAppointmentType());
            if (!availableTimeframes.get(appointmentVO.getDate()).equals(endDate)) {
                throw new GenericException("Acest tip de programare nu s-a putut face la ora solicitata");
            }
        }

        AppointmentEntity appointment = appointmentConverter.toEntity(appointmentVO);
        ManualUsersEntity manualUsersEntity = aManualUsersEntity()
                .withName(appointmentVO.getName())
                .withPhone(appointmentVO.getPhone())
                .build();
        appointment.setManualClient(manualUsersEntity);

        appointment.setStatus(PENDING);
        appointmentRepository.save(appointment);
    }

    public List<AppointmentDetailsVO> getWorkerAppointments(Integer workerId) {
        return appointmentRepository.findByWorker_IdAndStartGreaterThanEqual(workerId, new Date())
                .stream()
                .map(appointmentDetailsConverter::toVO)
                .collect(Collectors.toList());
    }

    public List<AppointmentDetailsVO> getClientAppointments(Integer clientId) {
        return appointmentRepository.findByClient_IdAndStartGreaterThanEqual(clientId, new Date())
                .stream()
                .map(appointmentDetailsConverter::toVO)
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteAppointment(Integer appointmentId, String token) {
        UserEntity userByToken = getUserEntityByToken(token);
        AppointmentEntity appointmentEntity = appointmentRepository.getOne(appointmentId);
        if (!appointmentEntity.getWorker().getId().equals(userByToken.getId()) &&
                !appointmentEntity.getClient().getId().equals(userByToken.getId())) {
            throw new GenericException("Nu poti sterge aceasta programare.");
        }

        appointmentRepository.deleteById(appointmentId);
    }

    public void confirmAppointment(Integer appointmentId, String token) {
        UserEntity userByToken = getUserEntityByToken(token);
        AppointmentEntity appointmentEntity = appointmentRepository.getOne(appointmentId);
        if (!appointmentEntity.getWorker().getId().equals(userByToken.getId()) &&
                !appointmentEntity.getClient().getId().equals(userByToken.getId())) {
            throw new GenericException("Nu poti confirma aceasta programare.");
        }
        appointmentEntity.setStatus(CONFIRMED);
        appointmentRepository.save(appointmentEntity);
    }
}
