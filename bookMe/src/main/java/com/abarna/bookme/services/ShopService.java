package com.abarna.bookme.services;

import com.abarna.bookme.converters.EmployeeConverter;
import com.abarna.bookme.converters.ShopConverter;
import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.ShopEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.exceptions.GenericException;
import com.abarna.bookme.repositories.EmployeeRepository;
import com.abarna.bookme.repositories.ShopRepository;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.util.ServiceUtils;
import com.abarna.bookme.vos.EmployeeVO;
import com.abarna.bookme.vos.ShopVO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.abarna.bookme.entities.EmployeeEntityBuilder.anEmployeeEntity;
import static com.abarna.bookme.enums.RolesEnum.ADMIN_ROLE;
import static com.abarna.bookme.vos.ShopVOBuilder.aShopVO;
import static java.util.stream.Collectors.toList;

@Service
public class ShopService extends ServiceUtils {
    private final ShopConverter shopConverter;
    private final ShopRepository shopRepository;
    private final UserRepository userRepository;
    private final EmployeeConverter employeeConverter;
    private final EmployeeRepository employeeRepository;

    public ShopService(ShopConverter shopConverter, ShopRepository shopRepository, UserRepository userRepository, EmployeeConverter employeeConverter, EmployeeRepository employeeRepository) {
        this.shopConverter = shopConverter;
        this.shopRepository = shopRepository;
        this.userRepository = userRepository;
        this.employeeConverter = employeeConverter;
        this.employeeRepository = employeeRepository;
    }

    public List<ShopVO> geAllShops() {
        return shopRepository.findAll().stream()
                .map(shopConverter::toVO)
                .collect(toList());
    }

    public ShopVO getShop(String token) {
        UserEntity userByToken = getUserEntityByToken(token);
        if (userByToken.getShop() == null) {
            return aShopVO().build();
        }

        throwExceptionIfNotOwner(userByToken);

        return shopConverter.toVO(userByToken.getShop());
    }

    public List<EmployeeVO> getShopEmployees(Integer shopId) {
        UserEntity ownerUserEntity = userRepository.findByShop_Id(shopId);
        List<EmployeeVO> employees = employeeRepository.findByOwner_Id(ownerUserEntity.getId()).stream()
                .map(employeeConverter::toVO)
                .collect(toList());
        UserEntity shopOwner = userRepository.findShopOwner(shopId);
        EmployeeEntity owner = anEmployeeEntity()
                .withEmployeeDetails(shopOwner)
                .withOwner(shopOwner)
                .withId(shopOwner.getId())
                .build();
        employees.add(employeeConverter.toVO(owner));
        return employees;
    }

    public void addShop(ShopVO shopVO, String token) {
        UserEntity userByToken = getUserEntityByToken(token);
        throwExceptionIfNotOwner(userByToken);
        if (userByToken.getShop() != null) {
            throw new GenericException("Este deja configurata o locatie pentru acest user.");
        }
        ShopEntity shopEntity = shopRepository.save(shopConverter.toEntity(shopVO));
        userByToken.setShop(shopEntity);
        userRepository.save(userByToken);
    }

    public void updateShop(ShopVO shopVO, String token) {
        if (shopVO.getId() == null) {
            throw new GenericException("Id-ul nu poate fi null.");
        }
        UserEntity userByToken = getUserEntityByToken(token);
        throwExceptionIfNotOwner(userByToken);
        if (userByToken.getShop() == null) {
            throw new GenericException("Ownerul nu are configurata nicio locatie.");
        }
        ShopEntity shopEntity = shopRepository.getOne(shopVO.getId());
        shopEntity.setShopName(shopVO.getShopName());
        shopEntity.setDescription(shopVO.getDescription());
        userRepository.save(userByToken);
    }

    private void throwExceptionIfNotOwner(Optional<UserEntity> userById) {
        if (!userById.get().getRoles().stream()
                .filter(roleEntity -> ADMIN_ROLE.equals(roleEntity.getRole()))
                .findAny().isPresent()) {
            throw new GenericException("Userul logat nu este owner.");
        }
    }
}
