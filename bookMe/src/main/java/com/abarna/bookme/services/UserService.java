package com.abarna.bookme.services;

import com.abarna.bookme.converters.UserConverter;
import com.abarna.bookme.entities.RoleEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.exceptions.GenericException;
import com.abarna.bookme.repositories.RoleRepository;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.security.JwtUser;
import com.abarna.bookme.util.ServiceUtils;
import com.abarna.bookme.vos.UserVO;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.abarna.bookme.enums.RolesEnum.CLIENT_ROLE;

@Service
public class UserService extends ServiceUtils {
    private final UserRepository userRepository;
    private final UserConverter userConverter;
    private final RoleRepository roleRepository;

    public UserService(UserRepository userRepository, UserConverter userConverter, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
        this.roleRepository = roleRepository;
    }

    public Optional<UserEntity> getUserById(Integer customerId) {
        return userRepository.findById(customerId).stream()
                .findAny();
    }

    @Transactional
    public void saveUser(UserVO userVO) {
        Optional<UserEntity> userEntityFromDB = userRepository.findByUsername(userVO.getUsername());

        if (userEntityFromDB.isPresent()) {
            throw new GenericException("Username-ul " + userVO.getUsername() + " nu este disponibil");
        }

        UserEntity userEntity = userConverter.toEntity(userVO);
        List<RoleEntity> roles = new ArrayList<>();

        Optional<RoleEntity> clientRoleOptional = roleRepository.findByRole(CLIENT_ROLE);

        if (!clientRoleOptional.isPresent()) {
            RoleEntity clientRole = createClientRole(roles);
            roles.add(clientRole);
        } else {
            roles.add(clientRoleOptional.get());
        }

        userEntity.setRoles(roles);
        //TODO remove it when implemented account validation
        userEntity.setValidated(true);
        userRepository.save(userEntity);
    }

    public UserVO getAuthenticatedUser(String token) {
        JwtUser userByToken = getUserByToken(token);
        return userConverter.toVO(userRepository.getOne(userByToken.getId()));
    }

    private RoleEntity createClientRole(List<RoleEntity> roles) {
        RoleEntity clientRole = new RoleEntity();
        clientRole.setRole(CLIENT_ROLE);
        return clientRole;
    }
}
