package com.abarna.bookme.services;

import com.abarna.bookme.converters.WorkframeConverter;
import com.abarna.bookme.entities.AppointmentEntity;
import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.RoleEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.entities.WorkframeEntity;
import com.abarna.bookme.enums.AppointmentTimeframeType;
import com.abarna.bookme.enums.RolesEnum;
import com.abarna.bookme.exceptions.GenericException;
import com.abarna.bookme.repositories.AppointmentRepository;
import com.abarna.bookme.repositories.EmployeeRepository;
import com.abarna.bookme.repositories.WorkframeRepository;
import com.abarna.bookme.util.ServiceUtils;
import com.abarna.bookme.vos.WorkframeVO;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.abarna.bookme.enums.AppointmentTimeframeType.HALF;
import static com.abarna.bookme.enums.AppointmentTimeframeType.HOUR;
import static com.abarna.bookme.enums.AppointmentTimeframeType.QUARTER;
import static com.abarna.bookme.enums.AppointmentTimeframeType.THREE_QUARTERS;
import static com.abarna.bookme.enums.RolesEnum.ADMIN_ROLE;
import static com.abarna.bookme.enums.RolesEnum.EMPLOYEE_ROLE;
import static com.abarna.bookme.util.DateUtils.getDateWithHour;
import static com.abarna.bookme.util.DateUtils.getDateWithMaxHour;
import static com.abarna.bookme.util.DateUtils.getDateWithMinHour;
import static com.abarna.bookme.util.DateUtils.getNextNTimeFrames;
import static com.abarna.bookme.util.DateUtils.getNextTimeframe;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Service
public class WorkframeService extends ServiceUtils {

    private final WorkframeRepository workframeRepository;
    private final WorkframeConverter workframeConverter;
    private final EmployeeRepository employeeRepository;
    private final AppointmentRepository appointmentRepository;

    public WorkframeService(WorkframeRepository workframeRepository, WorkframeConverter workframeConverter, EmployeeRepository employeeRepository, AppointmentRepository appointmentRepository) {
        this.workframeRepository = workframeRepository;
        this.workframeConverter = workframeConverter;
        this.employeeRepository = employeeRepository;
        this.appointmentRepository = appointmentRepository;
    }

    public void update(WorkframeVO workframeVO, String token) {
        UserEntity userEntity = getUserEntityByToken(token);
        List<RolesEnum> roles = userEntity.getRoles().stream().map(RoleEntity::getRole).collect(toList());
        if (roles.contains(ADMIN_ROLE)) {
            if (workframeVO.getWorkerId().equals(userEntity.getId())) {
                // modify his own workframe
                workframeRepository.save(workframeConverter.toEntity(workframeVO));
            } else {
                // modify employee workframe
                employeeRepository.findByOwner_Id(userEntity.getId())
                        .stream()
                        .filter(empEntity -> empEntity.getEmployeeDetails().getId().equals(workframeVO.getWorkerId()))
                        .findAny()
                        .orElseThrow(() -> new GenericException("Acest workframe nu iti apartine."));

                workframeRepository.save(workframeConverter.toEntity(workframeVO));

            }
        } else if (roles.contains(EMPLOYEE_ROLE)) {
            if (workframeVO.getWorkerId().equals(userEntity.getId())) {
                // modify his own workframe
                workframeRepository.save(workframeConverter.toEntity(workframeVO));
            } else {
                throw new GenericException("Acest workframe nu iti apartine.");
            }
        } else {
            throw new GenericException("Nu ai drepturi sa modifici acest workframe.");
        }

    }

    public void addWorkframe(WorkframeVO workframeVO, String token) {
        Date now = new Date();
        if ((workframeVO.getStartDate().before(now) || workframeVO.getEndDate().before(now)) && startDateIsNotToday(workframeVO)) {
            throw new GenericException("Programul nu poate fi adaugat pentru o zi din trecut.");
        }
        UserEntity userEntity = getUserEntityByToken(token);
        List<RolesEnum> roles = userEntity.getRoles().stream().map(RoleEntity::getRole).collect(toList());
        if (roles.contains(ADMIN_ROLE)) {
            //is admin
            if (workframeVO.getWorkerId() == null) {
                // add appointment for owner
                workframeVO.setWorkerId(userEntity.getId());
                saveWorkframeIfAvailable(workframeVO);
            } else {
                //add appojntment for an employee of owner
                EmployeeEntity employeeEntity = employeeRepository.getOne(workframeVO.getWorkerId());
                if (!employeeEntity.getOwner().equals(userEntity)) {
                    throw new GenericException("Angajatul este invalid.");
                }
                saveWorkframeIfAvailable(workframeVO);
            }
        } else if (roles.contains(EMPLOYEE_ROLE)) {
            // is employee
            if (workframeVO.getWorkerId() != userEntity.getId()) {
                throw new GenericException("Workframe-ul nu poate fi adaugat pentru acest angajat.");
            }
            saveWorkframeIfAvailable(workframeVO);
        } else {
            throw new GenericException("Nu ai drepturi sa adaugi acest workframe.");
        }
    }

    private boolean startDateIsNotToday(WorkframeVO workframeVO) {
        return !getDateWithMinHour(workframeVO.getStartDate()).equals(getDateWithMinHour(new Date()));
    }

    public List<WorkframeVO> getWorkframes(Integer userId, String token) {
        UserEntity userEntity = getUserEntityByToken(token);
        ArrayList<RolesEnum> roles = new ArrayList();
        roles.add(ADMIN_ROLE);
        roles.add(EMPLOYEE_ROLE);
        if (!isHavingAtLeastOneRoleOf(userEntity, roles)) {
            throw new GenericException("Nu ai drepturi pentru a verifica workframe-urile");
        }
        return workframeRepository.findByUserEntity_Id(userId).stream()
                .map(workframeConverter::toVO)
                .collect(toList());
    }

    private void saveWorkframeIfAvailable(WorkframeVO workframeVO) {
        if (!workframeIsEmpty(workframeVO)) {
            throw new GenericException("Ai deja un program stabilit pentru aceasta zi.");
        }
        workframeRepository.save(workframeConverter.toEntity(workframeVO));
    }

    private boolean workframeIsEmpty(WorkframeVO workframeVO) {
        AtomicBoolean isAvailable = new AtomicBoolean(true);
        for (WorkframeEntity nextBusyWorkframe : workframeRepository.findByUserEntity_Id(workframeVO.getWorkerId())) {
            Calendar nextBusyStartDate = Calendar.getInstance();
            nextBusyStartDate.setTime(workframeVO.getStartDate());
            if (startOrEndDateIsOverlappingWithAnotherWorkframe(workframeVO, nextBusyWorkframe) ||
                    getDateWithMinHour(workframeVO.getStartDate()).equals(nextBusyWorkframe.getStartDate()) ||
                    getDateWithMinHour(workframeVO.getEndDate()).equals(nextBusyWorkframe.getEndDate())
            ) {
                isAvailable.set(false);
            }
        }

        return isAvailable.get();
    }

    private boolean startOrEndDateIsOverlappingWithAnotherWorkframe(WorkframeVO workframeVO, WorkframeEntity nextBusyWorkframe) {
        return getDateWithMinHour(workframeVO.getStartDate()).before(nextBusyWorkframe.getEndDate()) &&
                getDateWithMinHour(workframeVO.getEndDate()).after(nextBusyWorkframe.getStartDate());
    }

    public Set<Date> getAvailabletimeframes(Integer employeeId, AppointmentTimeframeType appointmentTimeframeType, Date date) {
        return getAvailabletimeframesWithEndDate(employeeId, appointmentTimeframeType, date).keySet();
    }

    public Map<Date, Date> getAvailabletimeframesWithEndDate(Integer employeeId, AppointmentTimeframeType appointmentTimeframeType, Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String stringDate = dateFormat.format(date);
        Optional<WorkframeEntity> workframeEntityOptional = workframeRepository.findByUserEntity_IdAndDate(employeeId, stringDate);
        if (workframeEntityOptional.isEmpty()) {
            WorkframeEntity nextAvailableWorkframe = workframeRepository.findFirstByUserEntity_IdAndStartDateIsGreaterThanEqualOrderByStartDate(employeeId, date);
            String errorMessage;
            if (nextAvailableWorkframe != null && nextAvailableWorkframe.getStartDate() != null) {
                errorMessage = "Persoana selectata nu lucreaza in aceasta zi. Prima zi disponibila este " +
                        nextAvailableWorkframe.getStartDate();
            } else {
                errorMessage = "Persoana selectata nu lucreaza in aceasta zi. Momentan nicio data nu este disponibila.";
            }
            throw new GenericException(errorMessage);
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(workframeEntityOptional.get().getStartHour());
        Date currentDayWithCustomHour = getDateWithHour(date, calendar.get(Calendar.HOUR_OF_DAY)); // here should be open date for customer
        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(workframeEntityOptional.get().getEndHour());
        Date closingDate = getDateWithHour(date, endCalendar.get(Calendar.HOUR_OF_DAY)); // here should be closing date for customer
        Map<Date, Date> availableIntervals = new LinkedHashMap<>();
        Map<Date, Date> busyIntervals = appointmentRepository.findByWorker_IdAndStartGreaterThanEqualAndStartLessThanEqual(employeeId,
                getDateWithMinHour(date), getDateWithMaxHour(date))
                .stream()
                .collect(toMap(AppointmentEntity::getStart, AppointmentEntity::getEnd));
        switch (appointmentTimeframeType) {
            case QUARTER: {
                // substracting below the interval time for not going over close hour on last intervals
                while (currentDayWithCustomHour.getTime() <= closingDate.getTime() - (QUARTER.getNumberOfMinutes() - 1) * 1000 * 60) {
                    if (isIntervalAvailable(busyIntervals, currentDayWithCustomHour)) {
                        availableIntervals.put(new Date(currentDayWithCustomHour.getTime()), getNextTimeframe(currentDayWithCustomHour, QUARTER));
                        currentDayWithCustomHour.setTime(getNextTimeframe(currentDayWithCustomHour, QUARTER).getTime()); // adding 15 min
                    } else {
                        currentDayWithCustomHour.setTime(getNextTimeframe(currentDayWithCustomHour, QUARTER).getTime()); // adding 15 min
                        continue;
                    }
                }
                break;
            }
            case HALF: {
                while (currentDayWithCustomHour.getTime() <= (closingDate.getTime() - (HALF.getNumberOfMinutes() - 1) * 1000 * 60)) {
                    if (!isIntervalAvailable(busyIntervals, currentDayWithCustomHour)) {
                        currentDayWithCustomHour.setTime(getNextTimeframe(currentDayWithCustomHour, QUARTER).getTime());
                        continue;
                    } else if (!isIntervalAvailable(busyIntervals, getNextTimeframe(currentDayWithCustomHour, QUARTER))) {
                        currentDayWithCustomHour.setTime(getNextTimeframe(currentDayWithCustomHour, HALF).getTime());
                        continue;
                    } else {
                        availableIntervals.put(new Date(currentDayWithCustomHour.getTime()), getNextTimeframe(currentDayWithCustomHour, HALF));
                        currentDayWithCustomHour.setTime(getNextTimeframe(currentDayWithCustomHour, QUARTER).getTime()); // adding 15 min
                    }
                }
                break;
            }
            case HOUR:
                while (currentDayWithCustomHour.getTime() <= closingDate.getTime() - (HOUR.getNumberOfMinutes() - 1) * 1000 * 60) {
                    List<Date> nextNTimeFrames = getNextNTimeFrames(currentDayWithCustomHour, QUARTER, 3);
                    if (!isIntervalAvailable(busyIntervals, currentDayWithCustomHour)) {
                        currentDayWithCustomHour.setTime(getNextTimeframe(currentDayWithCustomHour, QUARTER).getTime());
                        continue;
                    } else if (!isIntervalAvailable(busyIntervals, nextNTimeFrames.get(0))) {
                        currentDayWithCustomHour.setTime(getNextTimeframe(currentDayWithCustomHour, HALF).getTime());
                        continue;
                    } else if (!isIntervalAvailable(busyIntervals, nextNTimeFrames.get(1))) {
                        currentDayWithCustomHour.setTime(getNextTimeframe(currentDayWithCustomHour, THREE_QUARTERS).getTime());
                        continue;
                    } else if (!isIntervalAvailable(busyIntervals, nextNTimeFrames.get(2))) {
                        currentDayWithCustomHour.setTime(getNextTimeframe(currentDayWithCustomHour, HOUR).getTime());
                        continue;
                    } else {
                        availableIntervals.put(new Date(currentDayWithCustomHour.getTime()), getNextTimeframe(currentDayWithCustomHour, HOUR));
                        currentDayWithCustomHour.setTime(getNextTimeframe(currentDayWithCustomHour, QUARTER).getTime()); // adding 15 min
                    }
                }
                break;
            default:
                throw new GenericException("Tipul programarii este invalid.");
        }
        return availableIntervals;
    }

    private boolean isIntervalAvailable(Map<Date, Date> busyIntervals, Date currentDayWithCustomHour) {
        AtomicBoolean isAvailable = new AtomicBoolean(true);
        busyIntervals.forEach(
                (start, end) -> {
                    if (currentDayWithCustomHour.getTime() >= start.getTime() &&
                            currentDayWithCustomHour.getTime() < end.getTime()) {
                        isAvailable.set(false);
                    }
                }
        );
        return isAvailable.get();
    }


    public void delete(Integer workframeId, String token) {
        UserEntity userEntity = getUserEntityByToken(token);
        List<RolesEnum> roles = userEntity.getRoles().stream().map(RoleEntity::getRole).collect(toList());
        if (roles.contains(ADMIN_ROLE)) {
            // admin
            WorkframeEntity workframe = workframeRepository.getOne(workframeId);
            if (workframe.getUserEntity().equals(userEntity)) {
                workframeRepository.deleteById(workframeId);
            } else {
                employeeRepository.findByOwner_Id(userEntity.getId())
                        .stream()
                        .filter(empEntity -> empEntity.getEmployeeDetails().getId().equals(workframe.getUserEntity().getId()))
                        .findAny()
                        .orElseThrow(() -> new GenericException("Acest workframe nu iti apartine."));
                workframeRepository.deleteById(workframeId);
            }
        } else if (roles.contains(EMPLOYEE_ROLE)) {
            WorkframeEntity workframe = workframeRepository.getOne(workframeId);
            if (workframe.getUserEntity().equals(userEntity)) {
                workframeRepository.deleteById(workframeId);
            } else {
                throw new GenericException("Nu poti sterge programul unui alt angajat.");
            }
        } else {
            throw new GenericException("Nu ai drepturi sa stergi acest program.");
        }
    }
}
