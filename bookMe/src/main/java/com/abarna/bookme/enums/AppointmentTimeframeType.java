package com.abarna.bookme.enums;

public enum AppointmentTimeframeType {
    QUARTER(15, "Barba"),
    HALF(30, "Tuns"),
    THREE_QUARTERS(45, ""),
    HOUR(60, "Tuns si barbierit");

    private final int numberOfMinutes;
    private final String serviceType;

    AppointmentTimeframeType(int numberOfMinutes, String serviceType) {
        this.numberOfMinutes = numberOfMinutes;
        this.serviceType = serviceType;
    }

    public int getNumberOfMinutes() {
        return numberOfMinutes;
    }

    public String getServiceType() {
        return serviceType;
    }
}
