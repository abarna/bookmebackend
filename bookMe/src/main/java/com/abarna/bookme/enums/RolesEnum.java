package com.abarna.bookme.enums;

public enum RolesEnum {
    ADMIN_ROLE,
    EMPLOYEE_ROLE,
    CLIENT_ROLE
}
