package com.abarna.bookme.enums;

public enum AppointmentStatusType {
    PENDING,
    CONFIRMED,
    DELETED
}
