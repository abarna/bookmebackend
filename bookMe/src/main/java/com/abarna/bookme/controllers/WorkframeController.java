package com.abarna.bookme.controllers;

import com.abarna.bookme.enums.AppointmentTimeframeType;
import com.abarna.bookme.services.WorkframeService;
import com.abarna.bookme.vos.WorkframeVO;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.abarna.bookme.util.ControllerUtils.getTokenFromRequest;

@RestController
@RequestMapping("/workframes")
public class WorkframeController {

    private final WorkframeService workframeService;

    public WorkframeController(WorkframeService workframeService) {
        this.workframeService = workframeService;
    }

    @PostMapping()
    public void addWorkframes(@RequestBody WorkframeVO workframeVO, HttpServletRequest request) {
        workframeService.addWorkframe(workframeVO, getTokenFromRequest(request));
    }

    @GetMapping("/{userId}")
    public List<WorkframeVO> getWorkframes(@PathVariable Integer userId, HttpServletRequest request) {
        return workframeService.getWorkframes(userId, getTokenFromRequest(request));
    }

    @GetMapping("/available/{employeeId}")
    public Set<Date> getEmployeeWorkframes(@PathVariable Integer employeeId,
                                           @RequestParam AppointmentTimeframeType appointmentType,
                                           @RequestParam @DateTimeFormat(pattern = "dd.MM.yyyy") Date date) {
        return workframeService.getAvailabletimeframes(employeeId, appointmentType, date);
    }

    @PutMapping()
    public void updateWorkframes(@RequestBody WorkframeVO workframeVO, HttpServletRequest request) {
        workframeService.update(workframeVO, getTokenFromRequest(request));
    }

    @DeleteMapping("/{workframeId}")
    public void deleteWorkframe(@PathVariable Integer workframeId, HttpServletRequest request) {
        workframeService.delete(workframeId, getTokenFromRequest(request));
    }
}

