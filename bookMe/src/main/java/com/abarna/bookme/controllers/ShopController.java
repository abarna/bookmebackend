package com.abarna.bookme.controllers;

import com.abarna.bookme.services.ShopService;
import com.abarna.bookme.util.ControllerUtils;
import com.abarna.bookme.vos.EmployeeVO;
import com.abarna.bookme.vos.ShopVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

import static com.abarna.bookme.util.ControllerUtils.getTokenFromRequest;

@RestController
@RequestMapping("/shops")
public class ShopController {

    private final ShopService shopService;

    public ShopController(ShopService shopService) {
        this.shopService = shopService;
    }

    @GetMapping("/all")
    public List<ShopVO> getShops(HttpServletRequest request) {
        return shopService.geAllShops();
    }

    @GetMapping()
    public ShopVO getShop(HttpServletRequest request) {
        return shopService.getShop(getTokenFromRequest(request));
    }

    @GetMapping("/{shopId}/employees")
    public List<EmployeeVO> getShopEmployees(@PathVariable("shopId") Integer shopId) {
        return shopService.getShopEmployees(shopId);
    }

    @PostMapping
    public void addShop(@RequestBody @Valid ShopVO shopVO, HttpServletRequest request) {
        shopService.addShop(shopVO, getTokenFromRequest(request));
    }

    @PutMapping
    public void updateShop(@RequestBody @Valid ShopVO shopVO, HttpServletRequest request) {
        shopService.updateShop(shopVO, getTokenFromRequest(request));
    }
}
