package com.abarna.bookme.controllers;

import com.abarna.bookme.services.EmployeeService;
import com.abarna.bookme.vos.EmployeeVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.abarna.bookme.util.ControllerUtils.getTokenFromRequest;

@RestController
@RequestMapping("/owner")
@CrossOrigin
public class OwnerController {
    private final EmployeeService employeeService;

    public OwnerController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/employees")
    public List<EmployeeVO> getOwnerEmployees(HttpServletRequest request) {
        return employeeService.getOwnerEmployees(getTokenFromRequest(request));
    }
}
