package com.abarna.bookme.controllers;

import com.abarna.bookme.services.UserService;
import com.abarna.bookme.vos.UserVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static com.abarna.bookme.util.ControllerUtils.getTokenFromRequest;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public void saveUser(@RequestBody @Valid UserVO userVO) {
        userService.saveUser(userVO);
    }

    @GetMapping
    public UserVO getAuthenticatedUser(HttpServletRequest request) {
        return userService.getAuthenticatedUser(getTokenFromRequest(request));
    }
}
