package com.abarna.bookme.controllers;

import com.abarna.bookme.enums.AppointmentTimeframeType;
import com.abarna.bookme.services.AppointmentService;
import com.abarna.bookme.vos.AppointmentDetailsVO;
import com.abarna.bookme.vos.AppointmentVO;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.abarna.bookme.util.ControllerUtils.getTokenFromRequest;

@RestController
@RequestMapping("/appointments")
public class AppointmentController {
    private final AppointmentService appointmentService;

    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @GetMapping("/{employeeId}")
    public List<AppointmentDetailsVO> getWorkerAppointments(@PathVariable Integer employeeId) {
        return appointmentService.getWorkerAppointments(employeeId);
    }

    @GetMapping("/client/{clientId}")
    public List<AppointmentDetailsVO> getClientAppointments(@PathVariable Integer clientId) {
        return appointmentService.getClientAppointments(clientId);
    }

    @PostMapping
    public void addAppointment(@RequestBody @Valid AppointmentVO appointmentVO) {
        appointmentService.addAppointment(appointmentVO);
    }

    @PostMapping("/manual")
    public void addAppointmentByEmployee(@RequestBody @Valid AppointmentVO appointmentVO) {
        appointmentService.addManualAppointment(appointmentVO);
    }

    @DeleteMapping("/{appointmentId}")
    public void deleteAppointment(@PathVariable Integer appointmentId, HttpServletRequest request){
        appointmentService.deleteAppointment(appointmentId, getTokenFromRequest(request));
    }

    @PatchMapping("/confirm/{appointmentId}")
    public void confirmAppointment(@PathVariable Integer appointmentId, HttpServletRequest request){
        appointmentService.confirmAppointment(appointmentId, getTokenFromRequest(request));
    }
}
