package com.abarna.bookme.controllers;

import com.abarna.bookme.services.EmployeeService;
import com.abarna.bookme.vos.EmployeeVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static com.abarna.bookme.util.ControllerUtils.getTokenFromRequest;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping()
    public EmployeeVO getEmployee(HttpServletRequest request) {
        return employeeService.getEmployee(getTokenFromRequest(request));
    }

    @PatchMapping()
    public void patchEmployee(@RequestBody EmployeeVO employeeVO, HttpServletRequest request) {
        employeeService.patchEmployee(employeeVO, getTokenFromRequest(request));
    }

    @PostMapping
    public void addEmployee(@RequestBody @Valid EmployeeVO employeeVO, HttpServletRequest request) {
        employeeService.addEmployee(employeeVO, getTokenFromRequest(request));
    }

    @DeleteMapping("/{id}")
    public void deleteEmployee(@PathVariable int id, HttpServletRequest request) {
        employeeService.deleteEmployee(id, getTokenFromRequest(request));
    }
}

