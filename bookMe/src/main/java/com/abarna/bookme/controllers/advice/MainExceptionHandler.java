package com.abarna.bookme.controllers.advice;

import com.abarna.bookme.vos.ApiErrorVO;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.annotation.Priority;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
@Priority(Ordered.HIGHEST_PRECEDENCE)
public class MainExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({Exception.class})
    public final ResponseEntity<ApiErrorVO> handleAllException(Exception e, WebRequest request) {
        ApiErrorVO apiErrorVO = new ApiErrorVO(BAD_REQUEST, "A aparut o eroare", e.getMessage());

        return new ResponseEntity<>(apiErrorVO, apiErrorVO.getStatus());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<ApiErrorVO> constraintViolationException(ConstraintViolationException exception) {
        List<String> errors = new ArrayList<>();
        exception.getConstraintViolations()
                .forEach(constraintViolation -> errors.add(constraintViolation.getMessage()));
        ApiErrorVO apiErrorVO = new ApiErrorVO(BAD_REQUEST, "A aparut o eroare", errors);
        return new ResponseEntity<>(apiErrorVO, apiErrorVO.getStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errorList = ex
                .getBindingResult()
                .getFieldErrors()
                .stream()
                .map(fieldError -> fieldError.getDefaultMessage())
                .collect(Collectors.toList());
        ApiErrorVO apiErrorVO = new ApiErrorVO(BAD_REQUEST, "A aparut o eroare", errorList);
        return handleExceptionInternal(ex, apiErrorVO, headers, apiErrorVO.getStatus(), request);
    }
}
