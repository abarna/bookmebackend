package com.abarna.bookme.vos;

public final class EmployeeVOBuilder {
    private Integer id;
    private UserVO employeeDetails;
    private Integer ownerId;

    private EmployeeVOBuilder() {
    }

    public static EmployeeVOBuilder anEmployeeVO() {
        return new EmployeeVOBuilder();
    }

    public EmployeeVOBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public EmployeeVOBuilder withEmployeeDetails(UserVO employeeDetails) {
        this.employeeDetails = employeeDetails;
        return this;
    }

    public EmployeeVOBuilder withOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
        return this;
    }

    public EmployeeVO build() {
        EmployeeVO employeeVO = new EmployeeVO();
        employeeVO.setId(id);
        employeeVO.setEmployeeDetails(employeeDetails);
        employeeVO.setOwnerId(ownerId);
        return employeeVO;
    }
}
