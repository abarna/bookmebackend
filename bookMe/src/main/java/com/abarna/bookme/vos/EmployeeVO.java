package com.abarna.bookme.vos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@JsonInclude(Include.NON_NULL)
public class EmployeeVO {
    @Id
    private Integer id;
    @NotNull(message = "Detaliile angajatului sunt obligatorii.")
    private UserVO employeeDetails;
    private Integer ownerId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserVO getEmployeeDetails() {
        return employeeDetails;
    }

    public void setEmployeeDetails(UserVO employeeDetails) {
        this.employeeDetails = employeeDetails;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }
}
