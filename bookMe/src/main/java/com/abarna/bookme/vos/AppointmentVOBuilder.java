package com.abarna.bookme.vos;

import com.abarna.bookme.enums.AppointmentStatusType;
import com.abarna.bookme.enums.AppointmentTimeframeType;

import java.util.Date;

public final class AppointmentVOBuilder {
    private Integer id;
    private Integer workerId;
    private Integer clientId;
    private Date date;
    // TODO  here should be another AppointmentType and be linked via a table with AppointmentTimeframeType (configurable by owner via Controller)
    private AppointmentTimeframeType appointmentType;

    private AppointmentVOBuilder() {
    }

    public static AppointmentVOBuilder anAppointmentVO() {
        return new AppointmentVOBuilder();
    }

    public AppointmentVOBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public AppointmentVOBuilder withWorkerId(Integer workerId) {
        this.workerId = workerId;
        return this;
    }

    public AppointmentVOBuilder withClientId(Integer clientId) {
        this.clientId = clientId;
        return this;
    }

    public AppointmentVOBuilder withDate(Date date) {
        this.date = date;
        return this;
    }

    public AppointmentVOBuilder withAppointmentType(AppointmentTimeframeType appointmentType) {
        this.appointmentType = appointmentType;
        return this;
    }

    public AppointmentVO build() {
        AppointmentVO appointmentVO = new AppointmentVO();
        appointmentVO.setId(id);
        appointmentVO.setWorkerId(workerId);
        appointmentVO.setClientId(clientId);
        appointmentVO.setDate(date);
        appointmentVO.setAppointmentType(appointmentType);
        return appointmentVO;
    }
}
