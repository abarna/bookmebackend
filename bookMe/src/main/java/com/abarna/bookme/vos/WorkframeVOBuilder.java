package com.abarna.bookme.vos;

import java.util.Date;

public final class WorkframeVOBuilder {
    private Integer id;
    private Integer workerId;
    private Date startDate;
    private Date endDate;
    private Date startHour;
    private Date endHour;

    private WorkframeVOBuilder() {
    }

    public static WorkframeVOBuilder aWorkframeVO() {
        return new WorkframeVOBuilder();
    }

    public WorkframeVOBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public WorkframeVOBuilder withWorkerId(Integer workerId) {
        this.workerId = workerId;
        return this;
    }

    public WorkframeVOBuilder withStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public WorkframeVOBuilder withEndDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    public WorkframeVOBuilder withStartHour(Date startHour) {
        this.startHour = startHour;
        return this;
    }

    public WorkframeVOBuilder withEndHour(Date endHour) {
        this.endHour = endHour;
        return this;
    }

    public WorkframeVO build() {
        WorkframeVO workframeVO = new WorkframeVO();
        workframeVO.setId(id);
        workframeVO.setWorkerId(workerId);
        workframeVO.setStartDate(startDate);
        workframeVO.setEndDate(endDate);
        workframeVO.setStartHour(startHour);
        workframeVO.setEndHour(endHour);
        return workframeVO;
    }
}
