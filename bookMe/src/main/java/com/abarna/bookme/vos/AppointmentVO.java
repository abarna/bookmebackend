package com.abarna.bookme.vos;

import com.abarna.bookme.enums.AppointmentTimeframeType;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class AppointmentVO {
    private Integer id;
    @NotNull(message = "Angajatul nu este valid.")
    private Integer workerId;
    private Integer clientId;
    private String clientUsername;
    private String name;
    private String phone;
    @NotNull(message = "Data nu este valida.")
    private Date date;
    @NotNull(message = "Tipul programarii nu este valid.")
    private AppointmentTimeframeType appointmentType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Integer workerId) {
        this.workerId = workerId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public AppointmentTimeframeType getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(AppointmentTimeframeType appointmentType) {
        this.appointmentType = appointmentType;
    }

    public String getClientUsername() {
        return clientUsername;
    }

    public void setClientUsername(String clientUsername) {
        this.clientUsername = clientUsername;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
