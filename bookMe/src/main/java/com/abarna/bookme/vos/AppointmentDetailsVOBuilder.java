package com.abarna.bookme.vos;

import com.abarna.bookme.enums.AppointmentStatusType;
import com.abarna.bookme.enums.AppointmentTimeframeType;

import java.util.Date;

public final class AppointmentDetailsVOBuilder {
    private Integer id;
    private UserVO workerVO;
    private UserVO clientVO;
    private ShopVO shopVO;
    private Date date;
    private Integer duration;
    private AppointmentTimeframeType appointmentType;
    private String service;
    private AppointmentStatusType status;

    private AppointmentDetailsVOBuilder() {
    }

    public static AppointmentDetailsVOBuilder anAppointmentDetailsVO() {
        return new AppointmentDetailsVOBuilder();
    }

    public AppointmentDetailsVOBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public AppointmentDetailsVOBuilder withWorkerVO(UserVO workerVO) {
        this.workerVO = workerVO;
        return this;
    }

    public AppointmentDetailsVOBuilder withClientVO(UserVO clientVO) {
        this.clientVO = clientVO;
        return this;
    }

    public AppointmentDetailsVOBuilder withShopVO(ShopVO shopVO) {
        this.shopVO = shopVO;
        return this;
    }

    public AppointmentDetailsVOBuilder withDate(Date date) {
        this.date = date;
        return this;
    }

    public AppointmentDetailsVOBuilder withDuration(Integer duration) {
        this.duration = duration;
        return this;
    }

    public AppointmentDetailsVOBuilder withAppointmentType(AppointmentTimeframeType appointmentType) {
        this.appointmentType = appointmentType;
        return this;
    }

    public AppointmentDetailsVOBuilder withService(String service) {
        this.service = service;
        return this;
    }

    public AppointmentDetailsVOBuilder withStatus(AppointmentStatusType status) {
        this.status = status;
        return this;
    }

    public AppointmentDetailsVO build() {
        AppointmentDetailsVO appointmentDetailsVO = new AppointmentDetailsVO();
        appointmentDetailsVO.setId(id);
        appointmentDetailsVO.setWorkerVO(workerVO);
        appointmentDetailsVO.setClientVO(clientVO);
        appointmentDetailsVO.setShopVO(shopVO);
        appointmentDetailsVO.setDate(date);
        appointmentDetailsVO.setDuration(duration);
        appointmentDetailsVO.setAppointmentType(appointmentType);
        appointmentDetailsVO.setService(service);
        appointmentDetailsVO.setStatus(status);
        return appointmentDetailsVO;
    }
}
