package com.abarna.bookme.vos;

import javax.validation.constraints.NotNull;

public class ShopVO {
    private Integer id;
    @NotNull(message = "Numele magazinului este obligatoriu.")
    private String shopName;
    @NotNull(message = "Descrierea este obligatoriu.")
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
