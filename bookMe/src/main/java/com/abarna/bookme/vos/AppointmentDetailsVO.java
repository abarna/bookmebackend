package com.abarna.bookme.vos;

import com.abarna.bookme.enums.AppointmentStatusType;
import com.abarna.bookme.enums.AppointmentTimeframeType;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class AppointmentDetailsVO {
    private Integer id;
    private UserVO workerVO;
    private UserVO clientVO;
    private ShopVO shopVO;
    private Date date;
    private Integer duration;
    private AppointmentTimeframeType appointmentType;
    private String service;
    private AppointmentStatusType status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserVO getWorkerVO() {
        return workerVO;
    }

    public void setWorkerVO(UserVO workerVO) {
        this.workerVO = workerVO;
    }

    public UserVO getClientVO() {
        return clientVO;
    }

    public void setClientVO(UserVO clientVO) {
        this.clientVO = clientVO;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AppointmentTimeframeType getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(AppointmentTimeframeType appointmentType) {
        this.appointmentType = appointmentType;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public ShopVO getShopVO() {
        return shopVO;
    }

    public void setShopVO(ShopVO shopVO) {
        this.shopVO = shopVO;
    }

    public AppointmentStatusType getStatus() {
        return status;
    }

    public void setStatus(AppointmentStatusType status) {
        this.status = status;
    }
}
