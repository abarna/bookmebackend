package com.abarna.bookme.vos;

import com.abarna.bookme.enums.RolesEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
@JsonInclude(Include.NON_NULL)
public class UserVO {
    private Integer id;
    @NotNull(message = "Prenumele este obligatoriu")
    private String firstName;
    @NotNull(message = "Numele este obligatoriu")
    private String lastName;
    @NotNull(message = "Email-ul este obligatoriu")
    private String email;
    @NotNull(message = "Username-ul este obligatoriu")
    private String username;
    @NotNull(message = "Parola este obligatoriu")
    private String password;
    @JsonIgnore
    private String encryptedPassword;
    @NotNull(message = "Confirmarea parolei este obligatorie.")
    private String confirmPassword;
    @NotNull(message = "Numarul de telefon este obligatoriu.")
    private String phone;
    private Integer shopId;
    private boolean isOwner;
    private List<RolesEnum> roles = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<RolesEnum> getRoles() {
        return roles;
    }

    public void setRoles(List<RolesEnum> roles) {
        this.roles = roles;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public boolean isOwner() {
        return isOwner;
    }

    public void setIsOwner(boolean owner) {
        isOwner = owner;
    }
}
