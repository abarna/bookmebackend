package com.abarna.bookme.vos;

import com.abarna.bookme.enums.RolesEnum;

import java.util.ArrayList;
import java.util.List;

public final class UserVOBuilder {
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private String encryptedPassword;
    private String confirmPassword;
    private String phone;
    private Integer shopId;
    private boolean isOwner;
    private List<RolesEnum> roles = new ArrayList<>();

    private UserVOBuilder() {
    }

    public static UserVOBuilder anUserVO() {
        return new UserVOBuilder();
    }

    public UserVOBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public UserVOBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserVOBuilder withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UserVOBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public UserVOBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UserVOBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserVOBuilder withEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
        return this;
    }

    public UserVOBuilder withConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
        return this;
    }

    public UserVOBuilder withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public UserVOBuilder withShopId(Integer shopId) {
        this.shopId = shopId;
        return this;
    }

    public UserVOBuilder withRoles(List<RolesEnum> roles) {
        this.roles = roles;
        return this;
    }

    public UserVOBuilder withIsOwner(boolean isOwner) {
        this.isOwner = isOwner;
        return this;
    }

    public UserVO build() {
        UserVO userVO = new UserVO();
        userVO.setId(id);
        userVO.setFirstName(firstName);
        userVO.setLastName(lastName);
        userVO.setEmail(email);
        userVO.setUsername(username);
        userVO.setPassword(password);
        userVO.setEncryptedPassword(encryptedPassword);
        userVO.setConfirmPassword(confirmPassword);
        userVO.setPhone(phone);
        userVO.setShopId(shopId);
        userVO.setRoles(roles);
        userVO.setIsOwner(isOwner);
        return userVO;
    }
}
