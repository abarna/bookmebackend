package com.abarna.bookme.vos;

import org.springframework.http.HttpStatus;

import java.util.List;

public final class ApiErrorVOBuilder {
    private HttpStatus status;
    private String message;
    private List<String> errors;

    private ApiErrorVOBuilder() {
    }

    public static ApiErrorVOBuilder anApiErrorVO() {
        return new ApiErrorVOBuilder();
    }

    public ApiErrorVOBuilder withStatus(HttpStatus status) {
        this.status = status;
        return this;
    }

    public ApiErrorVOBuilder withMessage(String message) {
        this.message = message;
        return this;
    }

    public ApiErrorVOBuilder withErrors(List<String> errors) {
        this.errors = errors;
        return this;
    }

    public ApiErrorVO build() {
        return new ApiErrorVO(status, message, errors);
    }
}
