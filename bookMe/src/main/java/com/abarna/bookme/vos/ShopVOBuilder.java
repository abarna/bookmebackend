package com.abarna.bookme.vos;

public final class ShopVOBuilder {
    private Integer id;
    private String shopName;
    private String description;

    private ShopVOBuilder() {
    }

    public static ShopVOBuilder aShopVO() {
        return new ShopVOBuilder();
    }

    public ShopVOBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public ShopVOBuilder withShopName(String shopName) {
        this.shopName = shopName;
        return this;
    }

    public ShopVOBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public ShopVO build() {
        ShopVO shopVO = new ShopVO();
        shopVO.setId(id);
        shopVO.setShopName(shopName);
        shopVO.setDescription(description);
        return shopVO;
    }
}
