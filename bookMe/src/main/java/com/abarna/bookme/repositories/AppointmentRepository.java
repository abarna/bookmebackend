package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.AppointmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

public interface AppointmentRepository extends JpaRepository<AppointmentEntity, Integer> {

    List<AppointmentEntity> findByWorker_IdAndStartGreaterThanEqualAndStartLessThanEqual(Integer employeeId, Date endDate, Date startDate);
    List<AppointmentEntity> findByWorker_IdAndStartGreaterThanEqual(Integer employeeId, Date endDate);
    List<AppointmentEntity> findByClient_IdAndStartGreaterThanEqual(Integer employeeId, Date endDate);
}
