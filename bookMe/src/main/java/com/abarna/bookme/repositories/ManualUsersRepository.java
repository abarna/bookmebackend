package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.ManualUsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManualUsersRepository extends JpaRepository<ManualUsersEntity, Integer> {
}
