package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.ShopEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShopRepository extends JpaRepository<ShopEntity, Integer> {
}
