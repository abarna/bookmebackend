package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    Optional<UserEntity> findByUsername(String name);

    UserEntity findByShop_Id(Integer shopId);

    @Query(value = "Select * FROM users WHERE shop_id = ?1 and is_owner = true", nativeQuery = true)
    UserEntity findShopOwner(Integer shopId);

}
