package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Integer> {
    List<EmployeeEntity> findByOwner_Id(Integer ownerId);
}
