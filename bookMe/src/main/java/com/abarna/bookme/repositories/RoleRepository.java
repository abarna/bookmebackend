package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.RoleEntity;
import com.abarna.bookme.enums.RolesEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {
    Optional<RoleEntity> findByRole(RolesEnum role);
}
