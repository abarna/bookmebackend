package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.WorkframeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface WorkframeRepository extends JpaRepository<WorkframeEntity, Integer> {

    @Query(value = "Select * FROM workframes WHERE user_id = ?1 and ?2 >= start_date and ?2 <= end_date ", nativeQuery = true)
        // the date should be in "yyyy-MM-dd" format
    Optional<WorkframeEntity> findByUserEntity_IdAndDate(Integer userId, String date);

    @Query(value = "Select * FROM workframes WHERE user_id = ?1 and start_date >= ?2 and start_date >= CAST( NOW() AS Date ) " +
            "ORDER BY start_date", nativeQuery = true)
    List<WorkframeEntity> findAvailableWorkframesByUserEntity_IdAndDate(Integer userId, String date);

    List<WorkframeEntity> findByUserEntity_Id(Integer userId);

    WorkframeEntity findFirstByUserEntity_IdAndStartDateIsGreaterThanEqualOrderByStartDate(Integer userId, Date date);
}
