package com.abarna.bookme.entities;

import java.util.Date;

public final class WorkframeEntityBuilder {
    private Integer id;
    private UserEntity userEntity;
    private Date startDate;
    private Date endDate;
    private Date startHour;
    private Date endHour;

    private WorkframeEntityBuilder() {
    }

    public static WorkframeEntityBuilder aWorkframeEntity() {
        return new WorkframeEntityBuilder();
    }

    public WorkframeEntityBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public WorkframeEntityBuilder withUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
        return this;
    }

    public WorkframeEntityBuilder withStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public WorkframeEntityBuilder withEndDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    public WorkframeEntityBuilder withStartHour(Date startHour) {
        this.startHour = startHour;
        return this;
    }

    public WorkframeEntityBuilder withEndHour(Date endHour) {
        this.endHour = endHour;
        return this;
    }

    public WorkframeEntity build() {
        WorkframeEntity workframeEntity = new WorkframeEntity();
        workframeEntity.setId(id);
        workframeEntity.setUserEntity(userEntity);
        workframeEntity.setStartDate(startDate);
        workframeEntity.setEndDate(endDate);
        workframeEntity.setStartHour(startHour);
        workframeEntity.setEndHour(endHour);
        return workframeEntity;
    }
}
