package com.abarna.bookme.entities;

import java.util.List;

public final class UserEntityBuilder {
    private Integer id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private String phone;
    private ShopEntity shop;
    private String encryptedPassword;
    private boolean validated;
    private List<RoleEntity> roles;
    private boolean isOwner;

    private UserEntityBuilder() {
    }

    public static UserEntityBuilder anUserEntity() {
        return new UserEntityBuilder();
    }

    public UserEntityBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public UserEntityBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserEntityBuilder withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UserEntityBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UserEntityBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public UserEntityBuilder withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public UserEntityBuilder withShop(ShopEntity shop) {
        this.shop = shop;
        return this;
    }

    public UserEntityBuilder withEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
        return this;
    }

    public UserEntityBuilder withValidated(boolean validated) {
        this.validated = validated;
        return this;
    }

    public UserEntityBuilder withRoles(List<RoleEntity> roles) {
        this.roles = roles;
        return this;
    }

    public UserEntityBuilder withIsOwner(boolean isOwner) {
        this.isOwner = isOwner;
        return this;
    }

    public UserEntity build() {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(id);
        userEntity.setFirstName(firstName);
        userEntity.setLastName(lastName);
        userEntity.setUsername(username);
        userEntity.setEmail(email);
        userEntity.setPhone(phone);
        userEntity.setShop(shop);
        userEntity.setEncryptedPassword(encryptedPassword);
        userEntity.setValidated(validated);
        userEntity.setRoles(roles);
        userEntity.setIsOwner(isOwner);
        return userEntity;
    }
}
