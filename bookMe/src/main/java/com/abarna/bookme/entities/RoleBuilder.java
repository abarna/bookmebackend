package com.abarna.bookme.entities;

import com.abarna.bookme.enums.RolesEnum;

import java.util.ArrayList;
import java.util.List;

public final class RoleBuilder {
    private Integer id;
    private RolesEnum role;
    private List<UserEntity> users = new ArrayList();

    private RoleBuilder() {
    }

    public static RoleBuilder aRole() {
        return new RoleBuilder();
    }

    public RoleBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public RoleBuilder withRole(RolesEnum role) {
        this.role = role;
        return this;
    }

    public RoleBuilder withUsers(List<UserEntity> users) {
        this.users = users;
        return this;
    }

    public RoleEntity build() {
        RoleEntity returnedRole = new RoleEntity();
        returnedRole.setId(id);
        returnedRole.setRole(role);
        returnedRole.setUsers(users);
        return returnedRole;
    }
}
