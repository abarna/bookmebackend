package com.abarna.bookme.entities;

import com.abarna.bookme.enums.AppointmentStatusType;
import com.abarna.bookme.enums.AppointmentTimeframeType;

import java.util.Date;

public final class AppointmentEntityBuilder {
    private Integer id;
    private UserEntity worker;
    private UserEntity client;
    private ManualUsersEntity manualClient;
    private AppointmentTimeframeType appointmentType;
    private AppointmentStatusType status;
    private Date start;
    private Date end;

    private AppointmentEntityBuilder() {
    }

    public static AppointmentEntityBuilder anAppointmentEntity() {
        return new AppointmentEntityBuilder();
    }

    public AppointmentEntityBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public AppointmentEntityBuilder withWorker(UserEntity worker) {
        this.worker = worker;
        return this;
    }

    public AppointmentEntityBuilder withClient(UserEntity client) {
        this.client = client;
        return this;
    }

    public AppointmentEntityBuilder withManualClient(ManualUsersEntity manualClient) {
        this.manualClient = manualClient;
        return this;
    }

    public AppointmentEntityBuilder withAppointmentType(AppointmentTimeframeType appointmentType) {
        this.appointmentType = appointmentType;
        return this;
    }

    public AppointmentEntityBuilder withStatus(AppointmentStatusType status) {
        this.status = status;
        return this;
    }

    public AppointmentEntityBuilder withStart(Date start) {
        this.start = start;
        return this;
    }

    public AppointmentEntityBuilder withEnd(Date end) {
        this.end = end;
        return this;
    }

    public AppointmentEntity build() {
        AppointmentEntity appointmentEntity = new AppointmentEntity();
        appointmentEntity.setId(id);
        appointmentEntity.setWorker(worker);
        appointmentEntity.setClient(client);
        appointmentEntity.setManualClient(manualClient);
        appointmentEntity.setAppointmentType(appointmentType);
        appointmentEntity.setStatus(status);
        appointmentEntity.setStart(start);
        appointmentEntity.setEnd(end);
        return appointmentEntity;
    }
}
