package com.abarna.bookme.entities;

public final class EmployeeEntityBuilder {
    private Integer id;
    private UserEntity employeeDetails;
    private UserEntity owner;

    private EmployeeEntityBuilder() {
    }

    public static EmployeeEntityBuilder anEmployeeEntity() {
        return new EmployeeEntityBuilder();
    }

    public EmployeeEntityBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public EmployeeEntityBuilder withEmployeeDetails(UserEntity employeeDetails) {
        this.employeeDetails = employeeDetails;
        return this;
    }

    public EmployeeEntityBuilder withOwner(UserEntity owner) {
        this.owner = owner;
        return this;
    }

    public EmployeeEntity build() {
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setId(id);
        employeeEntity.setEmployeeDetails(employeeDetails);
        employeeEntity.setOwner(owner);
        return employeeEntity;
    }
}
