package com.abarna.bookme.entities;

import com.abarna.bookme.enums.AppointmentStatusType;
import com.abarna.bookme.enums.AppointmentTimeframeType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity(name = "appointments")
public class AppointmentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    @JoinColumn(name = "worker_id", nullable = false)
    private UserEntity worker;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", nullable = false)
    private UserEntity client;
    @JoinColumn(name = "manual_client_id", nullable = false)
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    private ManualUsersEntity manualClient;
    @Enumerated(EnumType.STRING)
    private AppointmentTimeframeType appointmentType;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private AppointmentStatusType status;

    private Date start;
    private Date end;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserEntity getWorker() {
        return worker;
    }

    public void setWorker(UserEntity userEntity) {
        this.worker = userEntity;
    }

    public UserEntity getClient() {
        return client;
    }

    public void setClient(UserEntity client) {
        this.client = client;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public AppointmentTimeframeType getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(AppointmentTimeframeType appointmentType) {
        this.appointmentType = appointmentType;
    }

    public AppointmentStatusType getStatus() {
        return status;
    }

    public void setStatus(AppointmentStatusType appointmentStatusType) {
        this.status = appointmentStatusType;
    }

    public ManualUsersEntity getManualClient() {
        return manualClient;
    }

    public void setManualClient(ManualUsersEntity manualClient) {
        this.manualClient = manualClient;
    }
}
