package com.abarna.bookme.entities;

public final class EmployeeBuilder {
    private Integer id;
    private UserEntity employeeDetails;
    private UserEntity owner;

    private EmployeeBuilder() {
    }

    public static EmployeeBuilder anEmployee() {
        return new EmployeeBuilder();
    }

    public EmployeeBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public EmployeeBuilder withEmployeeDetails(UserEntity employeeDetails) {
        this.employeeDetails = employeeDetails;
        return this;
    }

    public EmployeeBuilder withOwner(UserEntity owner) {
        this.owner = owner;
        return this;
    }

    public EmployeeEntity build() {
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setId(id);
        employeeEntity.setEmployeeDetails(employeeDetails);
        employeeEntity.setOwner(owner);
        return employeeEntity;
    }
}
