package com.abarna.bookme.entities;

public final class ShopEntityBuilder {
    private Integer id;
    private String shopName;
    private String description;

    private ShopEntityBuilder() {
    }

    public static ShopEntityBuilder aShopEntity() {
        return new ShopEntityBuilder();
    }

    public ShopEntityBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public ShopEntityBuilder withShopName(String shopName) {
        this.shopName = shopName;
        return this;
    }

    public ShopEntityBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public ShopEntity build() {
        ShopEntity shopEntity = new ShopEntity();
        shopEntity.setId(id);
        shopEntity.setShopName(shopName);
        shopEntity.setDescription(description);
        return shopEntity;
    }
}
