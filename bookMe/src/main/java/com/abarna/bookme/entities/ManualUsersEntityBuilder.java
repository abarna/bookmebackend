package com.abarna.bookme.entities;

public final class ManualUsersEntityBuilder {
    private Integer id;
    private String name;
    private String phone;

    private ManualUsersEntityBuilder() {
    }

    public static ManualUsersEntityBuilder aManualUsersEntity() {
        return new ManualUsersEntityBuilder();
    }

    public ManualUsersEntityBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public ManualUsersEntityBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public ManualUsersEntityBuilder withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public ManualUsersEntity build() {
        ManualUsersEntity manualUsersEntity = new ManualUsersEntity();
        manualUsersEntity.setId(id);
        manualUsersEntity.setName(name);
        manualUsersEntity.setPhone(phone);
        return manualUsersEntity;
    }
}
