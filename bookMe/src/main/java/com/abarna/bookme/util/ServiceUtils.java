package com.abarna.bookme.util;

import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.enums.RolesEnum;
import com.abarna.bookme.exceptions.GenericException;
import com.abarna.bookme.security.JwtTokenUtil;
import com.abarna.bookme.security.JwtUser;
import com.abarna.bookme.security.service.JwtUserDetailsServiceImpl;
import com.abarna.bookme.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.abarna.bookme.enums.RolesEnum.ADMIN_ROLE;

@Component
public class ServiceUtils {
    @Autowired
    private UserService userService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private JwtUserDetailsServiceImpl jwtUserDetailsService;

    protected UserEntity getUserEntityByToken(String token) {
        JwtUser userByToken = getUserByToken(token);
        return userService.getUserById(userByToken.getId()).get();
    }

    protected void throwExceptionIfNotOwner(UserEntity ownerEntity) {
        if (!ownerEntity.getRoles().
                stream()
                .anyMatch(roleEntity -> ADMIN_ROLE.equals(roleEntity.getRole()))) {
            throw new GenericException("Userul logat nu este owner.");
        }
    }

    protected boolean isHavingAtLeastOneRoleOf(UserEntity userEntity, List<RolesEnum> roles) {
        return userEntity.getRoles()
                .stream()
                .anyMatch(roleEntity -> roles.contains(roleEntity.getRole()));
    }

    protected JwtUser getUserByToken(String token) {
        String username = jwtTokenUtil.getUsernameFromToken(token.substring(7));
        JwtUser user = (JwtUser) jwtUserDetailsService.loadUserByUsername(username);
        return user;
    }

}
