package com.abarna.bookme.util;

import javax.servlet.http.HttpServletRequest;

public class ControllerUtils {

    public static String getTokenFromRequest(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        return token;
    }
}
