package com.abarna.bookme.util;

import com.abarna.bookme.enums.AppointmentTimeframeType;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateUtils {

    public static Date getDateWithMinHour(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));
        return cal.getTime();
    }

    public static Date getDateWithHour(Date date, int hour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));
        return cal.getTime();
    }

    public static Date getDateWithMaxHour(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getActualMaximum(Calendar.MILLISECOND));
        return cal.getTime();
    }

    public static Date getNextTimeframe(Date currentDayWithCustomHour, AppointmentTimeframeType appointmentTimeframeType) {
        return new Date(currentDayWithCustomHour.getTime() + appointmentTimeframeType.getNumberOfMinutes() * 1000 * 60);
    }

    public static List<Date> getNextNTimeFrames(Date currentDayWithCustomHour, AppointmentTimeframeType appointmentTimeframeType, int numberOfTimeFrames) {
        List<Date> timeframes = new ArrayList<>();
        Date timeFrameToBeAdded = currentDayWithCustomHour;
        for (int i = 0; i < numberOfTimeFrames; i++) {
            timeFrameToBeAdded = new Date(timeFrameToBeAdded.getTime() + appointmentTimeframeType.getNumberOfMinutes() * 1000 * 60);
            timeframes.add(timeFrameToBeAdded);
        }
        return timeframes;
    }
}
