package com.abarna.bookme.converters;

import com.abarna.bookme.entities.ManualUsersEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.vos.UserVO;
import org.springframework.stereotype.Component;

import static com.abarna.bookme.vos.UserVOBuilder.anUserVO;

@Component
public class ManualUserConverter {

    public UserVO toVO(ManualUsersEntity user) {
        if (user == null) {
            return null;
        }
        UserVO userVO = anUserVO()
                .withFirstName(user.getName())
                .withPhone(user.getPhone())
                .build();
        return userVO;
    }
}
