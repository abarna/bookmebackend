package com.abarna.bookme.converters;

import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.vos.EmployeeVO;
import org.springframework.stereotype.Component;

import static com.abarna.bookme.entities.EmployeeBuilder.anEmployee;
import static com.abarna.bookme.vos.EmployeeVOBuilder.anEmployeeVO;

@Component
public class EmployeeConverter {

    private final UserConverter userConverter;
    private final UserRepository userRepository;

    public EmployeeConverter(UserConverter userConverter, UserRepository userRepository) {
        this.userConverter = userConverter;
        this.userRepository = userRepository;
    }

    public EmployeeVO toVO(EmployeeEntity employeeEntity) {
        if (employeeEntity == null) {
            return null;
        }
        return anEmployeeVO()
                .withId(employeeEntity.getId())
                .withEmployeeDetails(userConverter.toVO(employeeEntity.getEmployeeDetails()))
                .withOwnerId(employeeEntity.getOwner().getId())
                .build();
    }

    public EmployeeEntity toEntity(EmployeeVO employeeVO) {
        if (employeeVO == null) {
            return null;
        }
        return anEmployee()
                .withId(employeeVO.getId())
                .withEmployeeDetails(userConverter.toEntity(employeeVO.getEmployeeDetails()))
                .withOwner(getOwner(employeeVO))
                .build();
    }

    private UserEntity getOwner(EmployeeVO employeeVO) {
        if (employeeVO.getOwnerId() == null) {
            return null;
        }
        return userRepository.getOne(employeeVO.getOwnerId());
    }
}
