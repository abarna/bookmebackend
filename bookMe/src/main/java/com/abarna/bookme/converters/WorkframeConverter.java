package com.abarna.bookme.converters;

import com.abarna.bookme.entities.WorkframeEntity;
import com.abarna.bookme.entities.WorkframeEntityBuilder;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.vos.WorkframeVO;
import org.springframework.stereotype.Component;

import static com.abarna.bookme.entities.WorkframeEntityBuilder.aWorkframeEntity;
import static com.abarna.bookme.vos.WorkframeVOBuilder.aWorkframeVO;

@Component
public class WorkframeConverter {
    private final UserRepository userRepository;

    public WorkframeConverter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public WorkframeVO toVO(WorkframeEntity workframeEntity) {
        if (workframeEntity == null) {
            return null;
        }
        return aWorkframeVO()
                .withId(workframeEntity.getId())
                .withWorkerId(workframeEntity.getUserEntity().getId())
                .withStartDate(workframeEntity.getStartDate())
                .withStartHour(workframeEntity.getStartHour())
                .withEndHour(workframeEntity.getEndHour())
                .withEndDate(workframeEntity.getEndDate())
                .build();
    }

    public WorkframeEntity toEntity(WorkframeVO workframeVO) {
        if (workframeVO == null) {
            return null;
        }
        return aWorkframeEntity()
                .withId(workframeVO.getId())
                .withUserEntity(userRepository.getOne(workframeVO.getWorkerId()))
                .withStartDate(workframeVO.getStartDate())
                .withStartHour(workframeVO.getStartHour())
                .withEndHour(workframeVO.getEndHour())
                .withEndDate(workframeVO.getEndDate())
                .build();
    }
}
