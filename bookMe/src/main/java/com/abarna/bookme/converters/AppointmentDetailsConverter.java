package com.abarna.bookme.converters;

import com.abarna.bookme.entities.AppointmentEntity;
import com.abarna.bookme.entities.ManualUsersEntity;
import com.abarna.bookme.entities.ShopEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.repositories.EmployeeRepository;
import com.abarna.bookme.repositories.ManualUsersRepository;
import com.abarna.bookme.repositories.ShopRepository;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.vos.AppointmentDetailsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.abarna.bookme.vos.AppointmentDetailsVOBuilder.anAppointmentDetailsVO;

@Component
public class AppointmentDetailsConverter {
    private final UserRepository userRepository;
    private final UserConverter userConverter;
    private final ShopConverter shopConverter;
    private final EmployeeRepository employeeRepository;
    private final ManualUsersRepository manualUsersRepository;
    private final ManualUserConverter manualUserConverter;

    public AppointmentDetailsConverter(UserRepository userRepository, UserConverter userConverter, ShopConverter shopConverter, EmployeeRepository employeeRepository, ManualUsersRepository manualUsersRepository, ManualUserConverter manualUserConverter) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
        this.shopConverter = shopConverter;
        this.employeeRepository = employeeRepository;
        this.manualUsersRepository = manualUsersRepository;
        this.manualUserConverter = manualUserConverter;
    }

    public AppointmentDetailsVO toVO(AppointmentEntity appointmentEntity) {
        if (appointmentEntity == null) {
            return null;
        }
        ShopEntity shopEntity;
        if (appointmentEntity.getWorker().isIsOwner()) {
            // is admin
            shopEntity = appointmentEntity.getWorker().getShop();
        } else {
            // is employess
            shopEntity = employeeRepository.getOne(appointmentEntity.getWorker().getId()).getOwner().getShop();
        }
        AppointmentDetailsVO appointmentDetailsVO = anAppointmentDetailsVO()
                .withId(appointmentEntity.getId())
                .withWorkerVO(userConverter.toVO(userRepository.getOne(appointmentEntity.getWorker().getId())))
                .withDate(appointmentEntity.getStart())
                .withAppointmentType(appointmentEntity.getAppointmentType())
                .withDuration(appointmentEntity.getAppointmentType().getNumberOfMinutes())
                .withService(appointmentEntity.getAppointmentType().getServiceType())
                .withShopVO(shopConverter.toVO(shopEntity))
                .withStatus(appointmentEntity.getStatus())
                .build();

        if (appointmentEntity.getClient() != null) {
            UserEntity client = userRepository.getOne(appointmentEntity.getClient().getId());
            appointmentDetailsVO.setClientVO(userConverter.toVO(client));
        }
        if (appointmentEntity.getManualClient() != null) {
            ManualUsersEntity manualUsersEntity = manualUsersRepository.getOne(
                    appointmentEntity.getManualClient().getId()
            );
            appointmentDetailsVO.setClientVO(manualUserConverter.toVO(appointmentEntity.getManualClient()));
        }
        return appointmentDetailsVO;
    }
}
