package com.abarna.bookme.converters;

import com.abarna.bookme.entities.RoleEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.repositories.ShopRepository;
import com.abarna.bookme.vos.UserVO;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import static com.abarna.bookme.entities.RoleBuilder.aRole;
import static com.abarna.bookme.entities.UserEntityBuilder.anUserEntity;
import static com.abarna.bookme.vos.UserVOBuilder.anUserVO;
import static java.util.stream.Collectors.toList;

@Component
public class UserConverter {

    private final PasswordEncoder passwordEncoder;
    private final ShopRepository shopRepository;

    public UserConverter(PasswordEncoder passwordEncoder, ShopRepository shopRepository) {
        this.passwordEncoder = passwordEncoder;
        this.shopRepository = shopRepository;
    }

    public UserVO toVO(UserEntity user) {
        if (user == null) {
            return null;
        }
        UserVO userVO = anUserVO()
                .withId(user.getId())
                .withFirstName(user.getFirstName())
                .withLastName(user.getLastName())
                .withUsername(user.getUsername())
                .withEmail(user.getEmail())
                .withPhone(user.getPhone())
                .withEncryptedPassword(user.getEncryptedPassword())
                .withRoles(user.getRoles().stream()
                        .map(RoleEntity::getRole)
                        .collect(toList()))
                .withIsOwner(user.isIsOwner())
                .build();
        if (user.getShop() != null) {
            userVO.setShopId(user.getShop().getId());
        }
        return userVO;
    }

    public UserEntity toEntity(UserVO userVO) {
        if (userVO == null) {
            return null;
        }
        UserEntity userEntity = anUserEntity()
                .withId(userVO.getId())
                .withFirstName(userVO.getFirstName())
                .withLastName(userVO.getLastName())
                .withUsername(userVO.getUsername())
                .withEmail(userVO.getEmail())
                .withPhone(userVO.getPhone())
                .withEncryptedPassword(passwordEncoder.encode(userVO.getPassword()))
                .withRoles(userVO.getRoles().stream()
                        .map(role -> aRole()
                                .withRole(role)
                                .build())
                        .collect(toList()))
                .build();
        if(userVO.getShopId() != null){
            userEntity.setShop(shopRepository.getOne(userVO.getShopId()));
        }
        return userEntity;
    }
}
