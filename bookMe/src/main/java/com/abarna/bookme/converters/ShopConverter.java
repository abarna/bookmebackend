package com.abarna.bookme.converters;

import com.abarna.bookme.entities.ShopEntity;
import com.abarna.bookme.vos.ShopVO;
import org.springframework.stereotype.Component;

import static com.abarna.bookme.entities.ShopEntityBuilder.aShopEntity;
import static com.abarna.bookme.vos.ShopVOBuilder.aShopVO;

@Component
public class ShopConverter {

    public ShopConverter() {
    }

    public ShopVO toVO(ShopEntity shopEntity) {
        if (shopEntity == null) {
            return null;
        }
        return aShopVO()
                .withId(shopEntity.getId())
                .withShopName(shopEntity.getShopName())
                .withDescription(shopEntity.getDescription())
                .build();
    }

    public ShopEntity toEntity(ShopVO shopVO) {
        if (shopVO == null) {
            return null;
        }
        return aShopEntity()
                .withId(shopVO.getId())
                .withDescription(shopVO.getDescription())
                .withShopName(shopVO.getShopName())
                .build();
    }
}
