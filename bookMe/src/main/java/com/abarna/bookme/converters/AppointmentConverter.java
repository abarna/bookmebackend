package com.abarna.bookme.converters;

import com.abarna.bookme.entities.AppointmentEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.exceptions.GenericException;
import com.abarna.bookme.repositories.ShopRepository;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.vos.AppointmentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.abarna.bookme.entities.AppointmentEntityBuilder.anAppointmentEntity;
import static com.abarna.bookme.util.DateUtils.getNextTimeframe;
import static com.abarna.bookme.vos.AppointmentVOBuilder.anAppointmentVO;

@Component
public class AppointmentConverter {
    private final UserRepository userRepository;

    public AppointmentConverter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public AppointmentVO toVO(AppointmentEntity appointmentEntity) {
        if (appointmentEntity == null) {
            return null;
        }
        return anAppointmentVO()
                .withId(appointmentEntity.getId())
                .withClientId(appointmentEntity.getClient().getId())
                .withWorkerId(appointmentEntity.getWorker().getId())
                .withDate(appointmentEntity.getStart())
                .withAppointmentType(appointmentEntity.getAppointmentType())
                .build();
    }

    public AppointmentEntity toEntity(AppointmentVO appointmentVO) {
        if (appointmentVO == null) {
            return null;
        }
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withId(appointmentVO.getId())
                .withWorker(userRepository.getOne(appointmentVO.getWorkerId()))
                .withStart(appointmentVO.getDate())
                .withEnd(getNextTimeframe(appointmentVO.getDate(), appointmentVO.getAppointmentType()))
                .withAppointmentType(appointmentVO.getAppointmentType())
                .build();
        if (appointmentVO.getClientId() != null) {
            appointmentEntity.setClient(userRepository.getOne(appointmentVO.getClientId()));
        }

        if (appointmentVO.getClientUsername() != null) {
            Optional<UserEntity> client = userRepository.findByUsername(appointmentVO.getClientUsername());
            if (client.isEmpty()) {
                throw new GenericException("Username-ul nu este valid.");
            }
            appointmentEntity.setClient(client.get());
        }
        return appointmentEntity;
    }
}
