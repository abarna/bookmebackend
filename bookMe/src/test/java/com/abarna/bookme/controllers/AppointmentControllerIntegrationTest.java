package com.abarna.bookme.controllers;

import com.abarna.bookme.BookmeApplication;
import com.abarna.bookme.converters.AppointmentConverter;
import com.abarna.bookme.entities.AppointmentEntity;
import com.abarna.bookme.entities.AppointmentEntityBuilder;
import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.ShopEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.entities.WorkframeEntity;
import com.abarna.bookme.enums.RolesEnum;
import com.abarna.bookme.repositories.AppointmentRepository;
import com.abarna.bookme.repositories.EmployeeRepository;
import com.abarna.bookme.repositories.ShopRepository;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.repositories.WorkframeRepository;
import com.abarna.bookme.utils.IntegrationTestHelper;
import com.abarna.bookme.vos.AppointmentVO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

import static com.abarna.bookme.entities.AppointmentEntityBuilder.anAppointmentEntity;
import static com.abarna.bookme.entities.ShopEntityBuilder.aShopEntity;
import static com.abarna.bookme.entities.WorkframeEntityBuilder.aWorkframeEntity;
import static com.abarna.bookme.enums.AppointmentStatusType.CONFIRMED;
import static com.abarna.bookme.enums.AppointmentStatusType.PENDING;
import static com.abarna.bookme.enums.AppointmentTimeframeType.HALF;
import static com.abarna.bookme.enums.AppointmentTimeframeType.HOUR;
import static com.abarna.bookme.enums.AppointmentTimeframeType.QUARTER;
import static com.abarna.bookme.enums.RolesEnum.ADMIN_ROLE;
import static com.abarna.bookme.enums.RolesEnum.CLIENT_ROLE;
import static com.abarna.bookme.enums.RolesEnum.EMPLOYEE_ROLE;
import static com.abarna.bookme.util.DateUtils.getDateWithHour;
import static com.abarna.bookme.util.DateUtils.getDateWithMaxHour;
import static com.abarna.bookme.util.DateUtils.getDateWithMinHour;
import static com.abarna.bookme.util.DateUtils.getNextTimeframe;
import static com.abarna.bookme.utils.IntegrationTestHelper.asJsonString;
import static com.abarna.bookme.vos.AppointmentVOBuilder.anAppointmentVO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.DateUtil.now;
import static org.assertj.core.util.Lists.newArrayList;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = BookmeApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class AppointmentControllerIntegrationTest extends AuthenticationUtils {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private AppointmentRepository appointmentRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private IntegrationTestHelper integrationTestHelper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AppointmentConverter appointmentConverter;
    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private WorkframeRepository workframeRepository;

    @AfterEach
    void tearDown() {
        workframeRepository.deleteAll();
        appointmentRepository.deleteAll();
        employeeRepository.deleteAll();
        userRepository.deleteAll();
        shopRepository.deleteAll();
    }

    @Test
    @Transactional
    public void addAppointmentAsUser() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("asd2");
        userRepository.save(employeeEntity.getOwner());
        employeeRepository.save(employeeEntity);

        Date startDate = getDateWithHour(new Date(), 22);
        AppointmentVO appointmentVO = anAppointmentVO()
                .withDate(startDate)
                .withWorkerId(employeeEntity.getId())
                .withClientId(client.getId())
                .withAppointmentType(QUARTER)
                .build();
        Date start = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity.getEmployeeDetails())
                .withStartDate(start)
                .withStartHour(getDateWithMinHour(startDate))
                .withEndHour(getDateWithMaxHour(startDate))
                .withEndDate(start)
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);
        mvc.perform(post("/appointments/")
                .header("Authorization", token)
                .content(asJsonString(appointmentVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<AppointmentEntity> appointmentEntities = appointmentRepository.findAll();
        assertThat(appointmentEntities).hasSize(1);
        assertThat(appointmentEntities.get(0)).isEqualToIgnoringGivenFields(
                appointmentConverter.toEntity(appointmentVO),
                "id", "status"
        );
        assertThat(appointmentEntities.get(0).getStatus()).isEqualTo(PENDING);
    }

    @Test
    @Transactional
    public void addAppointmentAsEmployee() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("asd3");
        userRepository.save(employeeEntity.getOwner());
        employeeRepository.save(employeeEntity);

        Date startDate = getDateWithHour(new Date(), 22);
        AppointmentVO appointmentVO = anAppointmentVO()
                .withDate(startDate)
                .withWorkerId(employeeEntity.getId())
                .withClientId(client.getId())
                .withAppointmentType(QUARTER)
                .build();
        final String token = getTokenForUser(employeeEntity.getEmployeeDetails());
        Date start = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity.getEmployeeDetails())
                .withStartDate(start)
                .withStartHour(getDateWithMinHour(startDate))
                .withEndHour(getDateWithMaxHour(startDate))
                .withEndDate(start)
                .build();
        workframeRepository.save(workframe);
        mvc.perform(post("/appointments/")
                .header("Authorization", token)
                .content(asJsonString(appointmentVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<AppointmentEntity> appointmentEntities = appointmentRepository.findAll();
        assertThat(appointmentEntities).hasSize(1);
        assertThat(appointmentEntities.get(0)).isEqualToIgnoringGivenFields(
                appointmentConverter.toEntity(appointmentVO),
                "id", "status"
        );
        assertThat(appointmentEntities.get(0).getStatus()).isEqualTo(PENDING);
    }

    @Test
    @Transactional
    public void addAppointmentAsOwner() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("asd2");
        userRepository.save(employeeEntity.getOwner());
        employeeRepository.save(employeeEntity);
        UserEntity owner = integrationTestHelper.getUserWithUserNameAndRole("owner", ADMIN_ROLE);

        Date aDate = getDateWithMinHour(new Date());
        Date appointmentDate = getDateWithHour(aDate, 22);
        AppointmentVO appointmentVO = anAppointmentVO()
                .withDate(appointmentDate)
                .withWorkerId(employeeEntity.getEmployeeDetails().getId())
                .withClientId(client.getId())
                .withAppointmentType(QUARTER)
                .build();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity.getEmployeeDetails())
                .withStartDate(aDate)
                .withStartHour(getDateWithMinHour(aDate))
                .withEndHour(getDateWithMaxHour(aDate))
                .withEndDate(aDate)
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(owner);
        mvc.perform(post("/appointments/")
                .header("Authorization", token)
                .content(asJsonString(appointmentVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<AppointmentEntity> appointmentEntities = appointmentRepository.findAll();
        assertThat(appointmentEntities).hasSize(1);
        assertThat(appointmentEntities.get(0)).isEqualToIgnoringGivenFields(
                appointmentConverter.toEntity(appointmentVO),
                "id", "status"
        );
        assertThat(appointmentEntities.get(0).getStatus()).isEqualTo(PENDING);
    }

    @Test
    @Transactional
    public void addAppointmentAsOwner_withInvalidAppointment() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("asd2");
        userRepository.save(employeeEntity.getOwner());
        employeeRepository.save(employeeEntity);
        UserEntity owner = integrationTestHelper.getUserWithUserNameAndRole("owner", ADMIN_ROLE);

        Date startDate = getDateWithHour(new Date(), 23);
        AppointmentVO appointmentVO = anAppointmentVO()
                .withDate(startDate)
                .withWorkerId(employeeEntity.getId())
                .withClientId(client.getId())
                .build();
        final String token = getTokenForUser(owner);
        mvc.perform(post("/appointments/")
                .header("Authorization", token)
                .content(asJsonString(appointmentVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Tipul programarii nu este valid.")));

        assertThat(appointmentRepository.findAll()).isEmpty();
    }

    @Test
    @Transactional
    public void addAppointmentAsOwner_withInvalidClientId() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("asd2");
        userRepository.save(employeeEntity.getOwner());
        employeeRepository.save(employeeEntity);
        UserEntity owner = integrationTestHelper.getUserWithUserNameAndRole("owner", ADMIN_ROLE);

        Date startDate = getDateWithHour(new Date(), 23);
        AppointmentVO appointmentVO = anAppointmentVO()
                .withDate(startDate)
                .withWorkerId(employeeEntity.getId())
                .withAppointmentType(QUARTER)
                .build();
        final String token = getTokenForUser(owner);
        mvc.perform(post("/appointments/")
                .header("Authorization", token)
                .content(asJsonString(appointmentVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Clientul nu este valid.")));

        assertThat(appointmentRepository.findAll()).isEmpty();
    }

    @Test
    @Transactional
    public void addAppointmentAsOwner_withInvalidEmployeeId() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("asd2");
        userRepository.save(employeeEntity.getOwner());
        employeeRepository.save(employeeEntity);
        UserEntity owner = integrationTestHelper.getUserWithUserNameAndRole("owner", ADMIN_ROLE);

        Date startDate = getDateWithHour(new Date(), 23);
        AppointmentVO appointmentVO = anAppointmentVO()
                .withDate(startDate)
                .withClientId(client.getId())
                .withAppointmentType(QUARTER)
                .build();
        final String token = getTokenForUser(owner);
        mvc.perform(post("/appointments/")
                .header("Authorization", token)
                .content(asJsonString(appointmentVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Angajatul nu este valid.")));

        assertThat(appointmentRepository.findAll()).isEmpty();
    }

    @Test
    public void addAppointment_whenAddingSameAppointment_shouldFail() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("asd2");
        userRepository.save(employeeEntity.getOwner());
        employeeRepository.save(employeeEntity);

        Date startDate = getDateWithHour(new Date(), 23);
        AppointmentVO appointmentVO = anAppointmentVO()
                .withDate(startDate)
                .withWorkerId(employeeEntity.getId())
                .withClientId(client.getId())
                .withAppointmentType(QUARTER)
                .build();
        appointmentRepository.save(appointmentConverter.toEntity(appointmentVO));
        Date start = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity.getEmployeeDetails())
                .withStartDate(start)
                .withStartHour(getDateWithHour(start, 5))
                .withEndHour(getDateWithHour(start, 24))
                .withEndDate(start)
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);
        mvc.perform(post("/appointments/")
                .header("Authorization", token)
                .content(asJsonString(appointmentVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Programarea s-a facut cu o perioada invalida.")));

        List<AppointmentEntity> appointmentEntities = appointmentRepository.findAll();
        assertThat(appointmentEntities).hasSize(1);
    }

    @Test
    // WARNING
    // THIS TEST WILL FAIL OUTSIDE OPENING SHOP HOURS (UNTIL WE WILL LET THE OWNER TO MANAGE THOSE HOURS)
    // CURRENTLY IS SET YO 09 - 23
    public void addAppointmentAsUser_throwException_whenReservationIsInThePast() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("asd2");
        userRepository.save(employeeEntity.getOwner());
        employeeRepository.save(employeeEntity);

        Date startDate = getDateWithHour(new Date(), 0);
        AppointmentVO appointmentVO = anAppointmentVO()
                .withDate(now())
                .withWorkerId(employeeEntity.getId())
                .withClientId(client.getId())
                .withAppointmentType(QUARTER)
                .withDate(startDate)
                .build();
        Date start = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity.getEmployeeDetails())
                .withStartDate(start)
                .withStartHour(getDateWithHour(start, 5))
                .withEndHour(getDateWithHour(start, 15))
                .withEndDate(start)
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);
        mvc.perform(post("/appointments/")
                .header("Authorization", token)
                .content(asJsonString(appointmentVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Rezervarea nu poate fi facuta in trecut.")));

        assertThat(appointmentRepository.findAll()).isEmpty();
    }

    @Test
    public void addAppointment_whenStartDateIsBusy_throwException() throws Exception {
        UserEntity owner = integrationTestHelper.getUserWithRole(ADMIN_ROLE);
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("aClient", CLIENT_ROLE);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withStart(getDateWithHour(new Date(), 20))
                .withEnd(getDateWithHour(new Date(), 21))
                .withAppointmentType(HALF)
                .withWorker(owner)
                .withClient(client)
                .build();
        userRepository.saveAll(newArrayList(owner, client));
        appointmentRepository.save(appointmentEntity);

        AppointmentVO appointmentVO = anAppointmentVO()
                .withWorkerId(owner.getId())
                .withClientId(client.getId())
                .withAppointmentType(QUARTER)
                .withDate(appointmentEntity.getStart())
                .build();

        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(owner)
                .withStartDate(appointmentEntity.getStart())
                .withStartHour(getDateWithHour(appointmentEntity.getStart(), 5))
                .withEndHour(getDateWithHour(appointmentEntity.getStart(), 21))
                .withEndDate(appointmentEntity.getStart())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);
        mvc.perform(post("/appointments/")
                .header("Authorization", token)
                .content(asJsonString(appointmentVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Programarea s-a facut cu o perioada invalida.")));

        assertThat(appointmentRepository.findAll()).hasSize(1);
    }

    @Test
    public void addAppointment_whenStartDateAvailableButEndDateIsBusy_throwException() throws Exception {
        UserEntity owner = integrationTestHelper.getUserWithRole(ADMIN_ROLE);
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("aClient", CLIENT_ROLE);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withStart(getNextTimeframe(getDateWithHour(new Date(), 21), HALF))
                .withEnd(getDateWithHour(new Date(), 21))
                .withAppointmentType(HALF)
                .withWorker(owner)
                .withClient(client)
                .build();
        userRepository.saveAll(newArrayList(owner, client));
        appointmentRepository.save(appointmentEntity);

        AppointmentVO appointmentVO = anAppointmentVO()
                .withDate(getDateWithHour(new Date(), 21))
                .withWorkerId(owner.getId())
                .withClientId(client.getId())
                .withAppointmentType(HOUR)
                .build();

        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(owner)
                .withStartDate(appointmentEntity.getStart())
                .withStartHour(getDateWithHour(appointmentEntity.getStart(), 5))
                .withEndHour(getDateWithHour(appointmentEntity.getStart(), 21))
                .withEndDate(appointmentEntity.getStart())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);
        mvc.perform(post("/appointments/")
                .header("Authorization", token)
                .content(asJsonString(appointmentVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Programarea s-a facut cu o perioada invalida.")));

        assertThat(appointmentRepository.findAll()).hasSize(1);
    }

    @Test
    public void addAppontment_whenInvalidStartDate() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("asd2");
        userRepository.save(employeeEntity.getOwner());
        employeeRepository.save(employeeEntity);
        Date start = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity.getEmployeeDetails())
                .withStartDate(start)
                .withStartHour(getDateWithHour(start, 5))
                .withEndHour(getDateWithHour(start, 23))
                .withEndDate(start)
                .build();
        workframeRepository.save(workframe);

        AppointmentVO appointmentVO = anAppointmentVO()
                .withDate(getNextTimeframe(getDateWithHour(start, 22), HALF))
                .withWorkerId(employeeEntity.getId())
                .withClientId(client.getId())
                .withAppointmentType(HOUR)
                .build();
        final String token = getTokenForUser(client);
        mvc.perform(post("/appointments/")
                .header("Authorization", token)
                .content(asJsonString(appointmentVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Programarea s-a facut cu o perioada invalida.")));

        assertThat(appointmentRepository.findAll()).isEmpty();
    }

    @Test
    @Transactional
    public void getWorkerAppointments_shouldRetrieveOnlyTheDataFromFuture() throws Exception {
        ShopEntity shop = aShopEntity()
                .withDescription("A random description")
                .withShopName("A random shop name")
                .build();
        shopRepository.save(shop);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("anEmployee");
        employeeEntity.getOwner().setIsOwner(true);
        employeeEntity.getOwner().setShop(shop);
        userRepository.save(employeeEntity.getOwner());
        employeeRepository.save(employeeEntity);

        UserEntity anotherEmployee = integrationTestHelper.getUserWithUserNameAndRole("anotherEmp",
                EMPLOYEE_ROLE);
        UserEntity client = integrationTestHelper.getUserWithRole(CLIENT_ROLE);
        userRepository.saveAll(newArrayList(anotherEmployee, client));
        AppointmentEntity appointment = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity.getEmployeeDetails())
                .withStart(getDateWithMaxHour(new Date()))
                .withAppointmentType(QUARTER)
                .build();
        AppointmentEntity appointmentInThePast = anAppointmentEntity()
                .withClient(employeeEntity.getEmployeeDetails())
                .withWorker(anotherEmployee)
                .withStart(getDateWithMinHour(new Date()))
                .build();

        AppointmentEntity appointmentWithAnotherEmployee = anAppointmentEntity()
                .withClient(anotherEmployee)
                .withWorker(anotherEmployee)
                .withStart(getDateWithMaxHour(new Date()))
                .build();
        appointmentRepository.saveAll(newArrayList(appointment, appointmentInThePast, appointmentWithAnotherEmployee));

        final String token = getTokenForUser(employeeEntity.getEmployeeDetails());
        mvc.perform(get("/appointments/" + employeeEntity.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(1)))
                .andExpect(jsonPath("$[0].id", is(appointment.getId())))
                .andExpect(jsonPath("$[0].workerVO.id", is(appointment.getWorker().getId())))
                .andExpect(jsonPath("$[0].workerVO.firstName", is(appointment.getWorker().getFirstName())))
                .andExpect(jsonPath("$[0].workerVO.lastName", is(appointment.getWorker().getLastName())))
                .andExpect(jsonPath("$[0].workerVO.email", is(appointment.getWorker().getEmail())))
                .andExpect(jsonPath("$[0].workerVO.username", is(appointment.getWorker().getUsername())))
                .andExpect(jsonPath("$[0].workerVO.roles[0]", is(appointment.getWorker().getRoles().get(0).getRole().name())))
                .andExpect(jsonPath("$[0].clientVO.id", is(appointment.getClient().getId())))
                .andExpect(jsonPath("$[0].clientVO.firstName", is(appointment.getClient().getFirstName())))
                .andExpect(jsonPath("$[0].clientVO.lastName", is(appointment.getClient().getLastName())))
                .andExpect(jsonPath("$[0].clientVO.email", is(appointment.getClient().getEmail())))
                .andExpect(jsonPath("$[0].clientVO.username", is(appointment.getClient().getUsername())))
                .andExpect(jsonPath("$[0].clientVO.roles[0]", is(appointment.getClient().getRoles().get(0).getRole().name())))
                .andExpect(jsonPath("$[0].date", is(appointment.getStart().getTime())))
                .andExpect(jsonPath("$[0].duration", is(appointment.getAppointmentType().getNumberOfMinutes())))
                .andExpect(jsonPath("$[0].service", is(appointment.getAppointmentType().getServiceType())))
                .andExpect(jsonPath("$[0].appointmentType", is(appointment.getAppointmentType().name())))
                .andExpect(jsonPath("$[0].shopVO.id", is(shop.getId())))
                .andExpect(jsonPath("$[0].shopVO.shopName", is(shop.getShopName())))
                .andExpect(jsonPath("$[0].shopVO.description", is(shop.getDescription())));
    }

    @Test
    @Transactional
    public void getClientAppointments_shouldRetrieveOnlyTheDataFromFuture() throws Exception {
        ShopEntity shop = aShopEntity()
                .withDescription("A random description")
                .withShopName("A random shop name")
                .build();
        shopRepository.save(shop);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("anEmployee");
        employeeEntity.getOwner().setIsOwner(true);
        employeeEntity.getOwner().setShop(shop);
        userRepository.save(employeeEntity.getOwner());
        employeeRepository.save(employeeEntity);

        UserEntity anotherEmployee = integrationTestHelper.getUserWithUserNameAndRole("anotherEmp",
                EMPLOYEE_ROLE);
        UserEntity client = integrationTestHelper.getUserWithRole(CLIENT_ROLE);
        userRepository.saveAll(newArrayList(anotherEmployee, client));
        AppointmentEntity appointment = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity.getEmployeeDetails())
                .withStart(getDateWithMaxHour(new Date()))
                .withAppointmentType(QUARTER)
                .build();
        AppointmentEntity appointmentInThePast = anAppointmentEntity()
                .withClient(employeeEntity.getEmployeeDetails())
                .withWorker(anotherEmployee)
                .withStart(getDateWithMinHour(new Date()))
                .build();

        AppointmentEntity appointmentWithAnotherEmployee = anAppointmentEntity()
                .withClient(anotherEmployee)
                .withWorker(anotherEmployee)
                .withStart(getDateWithMaxHour(new Date()))
                .build();
        appointmentRepository.saveAll(newArrayList(appointment, appointmentInThePast, appointmentWithAnotherEmployee));

        final String token = getTokenForUser(employeeEntity.getEmployeeDetails());
        mvc.perform(get("/appointments/client/" + client.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(1)))
                .andExpect(jsonPath("$[0].id", is(appointment.getId())))
                .andExpect(jsonPath("$[0].workerVO.id", is(appointment.getWorker().getId())))
                .andExpect(jsonPath("$[0].workerVO.firstName", is(appointment.getWorker().getFirstName())))
                .andExpect(jsonPath("$[0].workerVO.lastName", is(appointment.getWorker().getLastName())))
                .andExpect(jsonPath("$[0].workerVO.email", is(appointment.getWorker().getEmail())))
                .andExpect(jsonPath("$[0].workerVO.username", is(appointment.getWorker().getUsername())))
                .andExpect(jsonPath("$[0].workerVO.roles[0]", is(appointment.getWorker().getRoles().get(0).getRole().name())))
                .andExpect(jsonPath("$[0].clientVO.id", is(appointment.getClient().getId())))
                .andExpect(jsonPath("$[0].clientVO.firstName", is(appointment.getClient().getFirstName())))
                .andExpect(jsonPath("$[0].clientVO.lastName", is(appointment.getClient().getLastName())))
                .andExpect(jsonPath("$[0].clientVO.email", is(appointment.getClient().getEmail())))
                .andExpect(jsonPath("$[0].clientVO.username", is(appointment.getClient().getUsername())))
                .andExpect(jsonPath("$[0].clientVO.roles[0]", is(appointment.getClient().getRoles().get(0).getRole().name())))
                .andExpect(jsonPath("$[0].date", is(appointment.getStart().getTime())))
                .andExpect(jsonPath("$[0].duration", is(appointment.getAppointmentType().getNumberOfMinutes())))
                .andExpect(jsonPath("$[0].service", is(appointment.getAppointmentType().getServiceType())))
                .andExpect(jsonPath("$[0].appointmentType", is(appointment.getAppointmentType().name())))
                .andExpect(jsonPath("$[0].shopVO.id", is(shop.getId())))
                .andExpect(jsonPath("$[0].shopVO.shopName", is(shop.getShopName())))
                .andExpect(jsonPath("$[0].shopVO.description", is(shop.getDescription())));
    }

    @Test
    public void deleteAppointment() throws Exception {
        UserEntity admin = integrationTestHelper.getUserWithRole(ADMIN_ROLE);
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("aClient", CLIENT_ROLE);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withWorker(admin)
                .withClient(client)
                .withAppointmentType(QUARTER)
                .withStart(new Date())
                .withEnd(new Date())
                .build();
        userRepository.saveAll(newArrayList(admin, client));
        appointmentRepository.save(appointmentEntity);

        final String token = getTokenForUser(admin);

        mvc.perform(delete("/appointments/" + appointmentEntity.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(appointmentRepository.findAll()).isEmpty();
    }

    @Test
    public void deleteEmployeeAppointmentAsOwner() throws Exception {
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsernameAndOwnerUsername("employee", "ownerEmp");
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("aClient", CLIENT_ROLE);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withWorker(employeeEntity.getEmployeeDetails())
                .withClient(client)
                .withAppointmentType(QUARTER)
                .withStart(new Date())
                .withEnd(new Date())
                .build();
        userRepository.saveAll(newArrayList(employeeEntity.getOwner(), employeeEntity.getEmployeeDetails(), client));
        appointmentRepository.save(appointmentEntity);

        final String token = getTokenForUser(employeeEntity.getOwner());

        mvc.perform(delete("/appointments/" + appointmentEntity.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Nu poti sterge aceasta programare.")));

        assertThat(appointmentRepository.findAll()).hasSize(1);
    }

    @Test
    public void deleteAppointment_whenTryingToDeleteFromAnotherOwner_shouldFail() throws Exception {
        UserEntity admin = integrationTestHelper.getUserWithRole(ADMIN_ROLE);
        UserEntity anotherAdmin = integrationTestHelper.getUserWithUserNameAndRole("anotherAdmin", ADMIN_ROLE);
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("aClient", CLIENT_ROLE);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withWorker(admin)
                .withClient(client)
                .withAppointmentType(QUARTER)
                .withStart(new Date())
                .withEnd(new Date())
                .build();
        userRepository.saveAll(newArrayList(admin, client, anotherAdmin));
        appointmentRepository.save(appointmentEntity);

        final String token = getTokenForUser(anotherAdmin);

        mvc.perform(delete("/appointments/" + appointmentEntity.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Nu poti sterge aceasta programare.")));

        assertThat(appointmentRepository.findAll()).hasSize(1);
    }

    @Test
    public void deleteAppointment_whenTryingToDeleteFromAnotherClient_shouldFail() throws Exception {
        UserEntity admin = integrationTestHelper.getUserWithRole(ADMIN_ROLE);
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("aClient", CLIENT_ROLE);
        UserEntity anotherClient = integrationTestHelper.getUserWithUserNameAndRole("anotherCLient", CLIENT_ROLE);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withWorker(admin)
                .withClient(client)
                .withAppointmentType(QUARTER)
                .withStart(new Date())
                .withEnd(new Date())
                .build();
        userRepository.saveAll(newArrayList(admin, client, anotherClient));
        appointmentRepository.save(appointmentEntity);

        final String token = getTokenForUser(anotherClient);

        mvc.perform(delete("/appointments/" + appointmentEntity.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Nu poti sterge aceasta programare.")));

        assertThat(appointmentRepository.findAll()).hasSize(1);
    }

    @Test
    public void confirmAppointment() throws Exception {
        UserEntity admin = integrationTestHelper.getUserWithRole(ADMIN_ROLE);
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("aClient", CLIENT_ROLE);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withWorker(admin)
                .withClient(client)
                .withAppointmentType(QUARTER)
                .withStart(new Date())
                .withEnd(new Date())
                .withStatus(PENDING)
                .build();
        userRepository.saveAll(newArrayList(admin, client));
        appointmentRepository.save(appointmentEntity);

        final String token = getTokenForUser(admin);

        mvc.perform(patch("/appointments/confirm/" + appointmentEntity.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(appointmentRepository.findAll().get(0).getStatus()).isEqualTo(CONFIRMED);
    }

    @Test
    public void confirmInvalidAppointment() throws Exception {
        UserEntity admin = integrationTestHelper.getUserWithRole(ADMIN_ROLE);
        UserEntity anotherAdmin = integrationTestHelper.getUserWithUserNameAndRole("anotherAdmin", ADMIN_ROLE);
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("aClient", CLIENT_ROLE);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withWorker(anotherAdmin)
                .withClient(client)
                .withAppointmentType(QUARTER)
                .withStart(new Date())
                .withEnd(new Date())
                .withStatus(PENDING)
                .build();
        userRepository.saveAll(newArrayList(admin, anotherAdmin, client));
        appointmentRepository.save(appointmentEntity);

        final String token = getTokenForUser(admin);

        mvc.perform(patch("/appointments/confirm/" + appointmentEntity.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Nu poti confirma aceasta programare.")));;

        assertThat(appointmentRepository.findAll().get(0).getStatus()).isEqualTo(PENDING);
    }
}
