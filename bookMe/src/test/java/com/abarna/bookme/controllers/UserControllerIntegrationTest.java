package com.abarna.bookme.controllers;

import com.abarna.bookme.BookmeApplication;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.abarna.bookme.entities.UserEntityBuilder.anUserEntity;
import static com.abarna.bookme.utils.IntegrationTestHelper.asJsonString;
import static com.abarna.bookme.vos.UserVOBuilder.anUserVO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = BookmeApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        userRepository.deleteAll();
    }

    @Test
    void createAccount() throws Exception {
        mvc.perform(post("/users")
                .content(asJsonString(anUserVO()
                        .withFirstName("asd")
                        .withLastName("asd")
                        .withPassword("asd")
                        .withConfirmPassword("asd")
                        .withPhone("asd")
                        .withEmail("asd@asd.com")
                        .withUsername("asd11")
                        .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(userRepository.findAll()).hasSize(1);
    }

    @Test
    void createAccount_usernameNotAvailable() throws Exception {
        UserEntity userEntity = anUserEntity()
                .withUsername("asd")
                .build();
        userRepository.save(userEntity);

        mvc.perform(post("/users")
                .content(asJsonString(anUserVO()
                        .withFirstName("asd")
                        .withLastName("asd")
                        .withPassword("asd")
                        .withConfirmPassword("asd")
                        .withPhone("asd")
                        .withEmail("asd@asd.com")
                        .withUsername("asd")
                        .build()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", 
                        is("Username-ul asd nu este disponibil")));

        List<UserEntity> allUsers = userRepository.findAll();
        assertThat(allUsers).hasSize(1);
        assertThat(allUsers.get(0)).isEqualToIgnoringNullFields(userEntity);
    }

}
