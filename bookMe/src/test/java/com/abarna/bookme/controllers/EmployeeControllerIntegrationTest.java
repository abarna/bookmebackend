package com.abarna.bookme.controllers;

import com.abarna.bookme.BookmeApplication;
import com.abarna.bookme.converters.EmployeeConverter;
import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.ShopEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.enums.RolesEnum;
import com.abarna.bookme.repositories.EmployeeRepository;
import com.abarna.bookme.repositories.RoleRepository;
import com.abarna.bookme.repositories.ShopRepository;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.utils.IntegrationTestHelper;
import com.abarna.bookme.vos.EmployeeVO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.LinkedHashMap;

import static com.abarna.bookme.entities.ShopEntityBuilder.aShopEntity;
import static com.abarna.bookme.enums.RolesEnum.CLIENT_ROLE;
import static com.abarna.bookme.enums.RolesEnum.EMPLOYEE_ROLE;
import static com.abarna.bookme.utils.IntegrationTestHelper.asJsonString;
import static com.abarna.bookme.vos.EmployeeVOBuilder.anEmployeeVO;
import static com.abarna.bookme.vos.UserVOBuilder.anUserVO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = BookmeApplication.class)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
@AutoConfigureMockMvc(addFilters = false)
class EmployeeControllerIntegrationTest extends AuthenticationUtils {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmployeeConverter employeeConverter;
    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private IntegrationTestHelper integrationTestHelper;

    @BeforeEach
    void setUp() {
        employeeRepository.deleteAll();
        userRepository.deleteAll();
        shopRepository.deleteAll();
        roleRepository.deleteAll();
    }

    @Test
    void getEmployee() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("employee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        final String token = getTokenForUser(employee.getEmployeeDetails());

        mvc.perform(get("/employees")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(employee.getId())))
                .andExpect(jsonPath("$.employeeDetails.id", is(employee.getEmployeeDetails().getId())))
                .andExpect(status().isOk());
    }

    @Test
    void getEmployeeEmptyResult() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("employee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        UserEntity owner = integrationTestHelper.getUserWithUserNameAndRole("anOwner", EMPLOYEE_ROLE);
        userRepository.save(owner);
        final String token = getTokenForUser(owner);

        mvc.perform(get("/employees")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(new LinkedHashMap())));
    }

    @Test
    public void getEmployee_loggedUserIsClient() throws Exception {

        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        final String token = getTokenForUser(client);

        mvc.perform(get("/employees")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este un angajat.")));

    }

    @Test
    public void getEmployee_loggedUserIsAdmin() throws Exception {

        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        userRepository.save(client);
        final String token = getTokenForUser(client);

        mvc.perform(get("/employees")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este un angajat.")));

    }

    @Test
    @Transactional
    public void patchEmployee() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("employee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        EmployeeVO employeeVO = employeeConverter.toVO(employee);
        employeeVO.getEmployeeDetails().setFirstName("newName");
        employeeVO.getEmployeeDetails().setLastName("newLastName");
        employeeVO.getEmployeeDetails().setPhone("newPhone");
        employeeVO.getEmployeeDetails().setEmail("newEmail");
        final String token = getTokenForUser(employee.getOwner());

        mvc.perform(patch("/employees")
                .header("Authorization", token)
                .content(asJsonString(employeeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        EmployeeEntity persistedEmployee = employeeRepository.getOne(employee.getId());
        assertThat(persistedEmployee.getEmployeeDetails().getFirstName()).isEqualTo("newName");
        assertThat(persistedEmployee.getEmployeeDetails().getLastName()).isEqualTo("newLastName");
        assertThat(persistedEmployee.getEmployeeDetails().getPhone()).isEqualTo("newPhone");
        assertThat(persistedEmployee.getEmployeeDetails().getEmail()).isEqualTo("newEmail");
    }

    @Test
    public void patchEmployee_whenLoggedUserNotOwner() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("employee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        EmployeeVO employeeVO = employeeConverter.toVO(employee);
        employeeVO.getEmployeeDetails().setFirstName("newName");
        final String token = getTokenForUser(employee.getEmployeeDetails());

        mvc.perform(patch("/employees")
                .header("Authorization", token)
                .content(asJsonString(employeeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));
    }

    @Test
    public void pathEmployeeOfAnotherOwner_shouldFail() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("employee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        EmployeeVO employeeVO = employeeConverter.toVO(employee);
        employeeVO.getEmployeeDetails().setFirstName("newName");
        employeeVO.setId(99);
        final String token = getTokenForUser(employee.getOwner());

        mvc.perform(patch("/employees")
                .header("Authorization", token)
                .content(asJsonString(employeeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Datele angajatului nu sunt valide.")));
    }

    @Test
    @Transactional
    public void addEmployee() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shop name")
                .withDescription("shop description")
                .build();
        ShopEntity persistedShop = shopRepository.save(shopEntity);
        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        owner.setShop(shopEntity);
        userRepository.save(owner);
        EmployeeVO employeeVO = anEmployee(persistedShop, owner);
        final String token = getTokenForUser(owner);

        mvc.perform(post("/employees")
                .header("Authorization", token)
                .content(asJsonString(employeeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(employeeRepository.findAll()).hasSize(1);
        EmployeeEntity employeeEntity = employeeRepository.findAll().get(0);
        assertThat(employeeEntity.getOwner().getId()).isEqualTo(owner.getId());
        assertEmployee(employeeVO, employeeEntity);
    }

    @Test
    public void addEmployee_missingEmployeeDetails_shouldFail() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shop name")
                .withDescription("shop description")
                .build();
        ShopEntity persistedShop = shopRepository.save(shopEntity);
        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        owner.setShop(shopEntity);
        userRepository.save(owner);
        EmployeeVO employeeVO = anEmployee(persistedShop, owner);
        employeeVO.setEmployeeDetails(null);
        final String token = getTokenForUser(owner);

        mvc.perform(post("/employees")
                .header("Authorization", token)
                .content(asJsonString(employeeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Detaliile angajatului sunt obligatorii.")));

        assertThat(employeeRepository.findAll()).isEmpty();
    }

    @Test
    public void addEmployee_whenSameUserShouldFail() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shop name")
                .withDescription("shop description")
                .build();
        ShopEntity persistedShop = shopRepository.save(shopEntity);

        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        owner.setShop(shopEntity);
        userRepository.save(owner);
        EmployeeVO employeeVO = anEmployee(persistedShop, owner);
        //persist an user with same username
        UserEntity userEntity = integrationTestHelper.getUserWithUserNameAndRole(employeeVO.getEmployeeDetails().getUsername(), CLIENT_ROLE);
        userRepository.save(userEntity);
        final String token = getTokenForUser(owner);

        mvc.perform(post("/employees")
                .header("Authorization", token)
                .content(asJsonString(employeeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Username-ul angajatului nu este valid.")));
    }

    @Test
    public void addEmployee_whenClientLogged_shouldFail() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shop name")
                .withDescription("shop description")
                .build();
        ShopEntity persistedShop = shopRepository.save(shopEntity);

        UserEntity owner = integrationTestHelper.getUserWithRole(CLIENT_ROLE);
        owner.setShop(shopEntity);
        userRepository.save(owner);
        EmployeeVO employeeVO = anEmployee(persistedShop, owner);
        final String token = getTokenForUser(owner);

        mvc.perform(post("/employees")
                .header("Authorization", token)
                .content(asJsonString(employeeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));
    }

    @Test
    public void addEmployee_whenEmployeeLoggedShouldFail() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shop name")
                .withDescription("shop description")
                .build();
        ShopEntity persistedShop = shopRepository.save(shopEntity);

        UserEntity owner = integrationTestHelper.getUserWithRole(EMPLOYEE_ROLE);
        owner.setShop(shopEntity);
        userRepository.save(owner);
        EmployeeVO employeeVO = anEmployee(persistedShop, owner);
        final String token = getTokenForUser(owner);

        mvc.perform(post("/employees")
                .header("Authorization", token)
                .content(asJsonString(employeeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));
    }

    @Test
    public void deleteEmployee() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("employee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        final String token = getTokenForUser(employee.getOwner());

        mvc.perform(delete("/employees/" + employee.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThat(employeeRepository.findAll()).isEmpty();
    }

    @Test
    public void deleteEmployee_whenEmployeeLoggedShouldFail() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("employee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        final String token = getTokenForUser(employee.getEmployeeDetails());

        mvc.perform(delete("/employees/" + employee.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));
        assertThat(employeeRepository.findAll()).hasSize(1);
    }

    @Test
    public void deleteEmployee_whenClientLoggedShouldFail() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("employee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        UserEntity userWithUserNameAndPasswordAndRole = integrationTestHelper.getUserWithUserNameAndRole("ddd", CLIENT_ROLE);
        userRepository.save(userWithUserNameAndPasswordAndRole);
        final String token = getTokenForUser(userWithUserNameAndPasswordAndRole);

        mvc.perform(delete("/employees/" + employee.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));
        assertThat(employeeRepository.findAll()).hasSize(1);
    }

    @Test
    public void deleteEmployee_shouldFailWhenTryingToDeleteEmployeeOfAnotherOwner() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("employee");
        EmployeeEntity employee2 = integrationTestHelper.getEmployeeWithUsernameAndOwnerUsername("employee2",
                "anOwner");
        userRepository.saveAll(newArrayList(employee.getOwner(),employee2.getOwner()));
        employeeRepository.saveAll(newArrayList(employee, employee2));
        final String token = getTokenForUser(employee2.getOwner());

        mvc.perform(delete("/employees/" + employee.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Id-ul angajatului este invalid.")));
        assertThat(employeeRepository.findAll()).hasSize(2);
    }

    @Test
    public void deleteEmployee_shouldFailWhenDeletingInexistingEmployee() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("employee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        final String token = getTokenForUser(employee.getOwner());

        mvc.perform(delete("/employees/" + 999)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Id-ul angajatului este invalid.")));
        assertThat(employeeRepository.findAll()).hasSize(1);
    }

    private void assertEmployee(EmployeeVO employeeVO, EmployeeEntity employeeEntity) {
        assertThat(employeeEntity.getEmployeeDetails().getEmail()).isEqualTo(employeeVO.getEmployeeDetails().getEmail());
        assertThat(employeeEntity.getEmployeeDetails().getFirstName()).isEqualTo(employeeVO.getEmployeeDetails().getFirstName());
        assertThat(employeeEntity.getEmployeeDetails().getLastName()).isEqualTo(employeeVO.getEmployeeDetails().getLastName());
        assertThat(employeeEntity.getEmployeeDetails().getPhone()).isEqualTo(employeeVO.getEmployeeDetails().getPhone());
        assertThat(employeeEntity.getEmployeeDetails().getRoles().get(0).getRole()).isEqualTo(EMPLOYEE_ROLE);
        assertThat(employeeEntity.getEmployeeDetails().getShop().getId()).isEqualTo(employeeVO.getEmployeeDetails().getShopId());
    }

    private EmployeeVO anEmployee(ShopEntity persistedShop, UserEntity owner) {
        return anEmployeeVO()
                .withOwnerId(owner.getId())
                .withEmployeeDetails(anUserVO()
                        .withFirstName("firstName")
                        .withLastName("lastName")
                        .withUsername("username")
                        .withPhone("12311")
                        .withPassword("aPassword")
                        .withEmail("anEmail@asd.com")
                        .withRoles(newArrayList(EMPLOYEE_ROLE))
                        .withShopId(persistedShop.getId())
                        .build())
                .build();
    }
}
