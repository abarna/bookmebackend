package com.abarna.bookme.controllers;

import com.abarna.bookme.BookmeApplication;
import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.ShopEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.enums.RolesEnum;
import com.abarna.bookme.repositories.EmployeeRepository;
import com.abarna.bookme.repositories.ShopRepository;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.utils.IntegrationTestHelper;
import com.abarna.bookme.vos.ShopVO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.abarna.bookme.entities.EmployeeEntityBuilder.anEmployeeEntity;
import static com.abarna.bookme.entities.ShopEntityBuilder.aShopEntity;
import static com.abarna.bookme.utils.IntegrationTestHelper.asJsonString;
import static com.abarna.bookme.vos.ShopVOBuilder.aShopVO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = BookmeApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class ShopControllerIntegrationTest extends AuthenticationUtils {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private IntegrationTestHelper integrationTestHelper;

    @AfterEach
    void setUp() {
        employeeRepository.deleteAll();
        userRepository.deleteAll();
        shopRepository.deleteAll();
    }

    @Test
    void getShop() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shopName")
                .withDescription("shopDescription")
                .build();
        shopRepository.save(shopEntity);

        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        owner.setShop(shopEntity);
        userRepository.save(owner);

        final String token = getTokenForUser(owner);
        mvc.perform(get("/shops")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(shopEntity.getId())))
                .andExpect(jsonPath("$.shopName", is(shopEntity.getShopName())))
                .andExpect(jsonPath("$.description", is(shopEntity.getDescription())));
    }

    @Test
    public void getShop_whenNotExisting() throws Exception {
        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        userRepository.save(owner);

        final String token = getTokenForUser(owner);
        mvc.perform(get("/shops")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(nullValue())))
                .andExpect(jsonPath("$.shopName", is(nullValue())))
                .andExpect(jsonPath("$.description", is(nullValue())));
    }

    @Test
    public void getShop_shouldFail_whenClientLogged() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shopName")
                .withDescription("shopDescription")
                .build();
        shopRepository.save(shopEntity);

        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        owner.setShop(shopEntity);
        userRepository.save(owner);

        final String token = getTokenForUser(owner);
        mvc.perform(get("/shops")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));
    }

    @Test
    public void getShop_shouldFail_whenEmployeeLogged() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shopName")
                .withDescription("shopDescription")
                .build();
        shopRepository.save(shopEntity);

        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);
        owner.setShop(shopEntity);
        userRepository.save(owner);

        final String token = getTokenForUser(owner);
        mvc.perform(get("/shops")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));
    }

    @Test
    public void addShop() throws Exception {
        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        userRepository.save(owner);
        final String token = getTokenForUser(owner);
        ShopVO shopVO = aShopVO()
                .withShopName("shop name")
                .withDescription("description")
                .build();

        mvc.perform(post("/shops")
                .header("Authorization", token)
                .content(asJsonString(shopVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<ShopEntity> shops = shopRepository.findAll();
        assertThat(shops).hasSize(1);
        assertThat(shops.get(0))
                .isEqualToIgnoringGivenFields(shopVO, "id");
    }

    @Test
    public void addShop_shouldFailWhenInvalidShopName() throws Exception {
        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        userRepository.save(owner);
        final String token = getTokenForUser(owner);
        ShopVO shopVO = aShopVO()
                .withDescription("description")
                .build();

        mvc.perform(post("/shops")
                .header("Authorization", token)
                .content(asJsonString(shopVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Numele magazinului este obligatoriu.")));

        assertThat(shopRepository.findAll().isEmpty());
    }

    @Test
    public void addShop_shouldFailWhenInvalidShopDescription() throws Exception {
        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        userRepository.save(owner);
        final String token = getTokenForUser(owner);
        ShopVO shopVO = aShopVO()
                .withShopName("name")
                .build();

        mvc.perform(post("/shops")
                .header("Authorization", token)
                .content(asJsonString(shopVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Descrierea este obligatoriu.")));

        assertThat(shopRepository.findAll().isEmpty());
    }

    @Test
    public void addShop_whenOwnerAlreadyHasShop() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shop name test")
                .withDescription("a description")
                .build();
        shopRepository.save(shopEntity);

        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        owner.setShop(shopEntity);
        userRepository.save(owner);
        final String token = getTokenForUser(owner);
        ShopVO shopVO = aShopVO()
                .withShopName("shop name")
                .withDescription("description")
                .build();

        mvc.perform(post("/shops")
                .header("Authorization", token)
                .content(asJsonString(shopVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Este deja configurata o locatie pentru acest user.")));

        List<ShopEntity> shops = shopRepository.findAll();
        assertThat(shops).hasSize(1);
        assertThat(shops.get(0)).isEqualToIgnoringGivenFields(shopEntity, "id");
    }

    @Test
    public void addShop_whenClientLoggedShouldFail() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        final String token = getTokenForUser(client);
        ShopVO shopVO = aShopVO()
                .withShopName("shop name")
                .withDescription("description")
                .build();

        mvc.perform(post("/shops")
                .header("Authorization", token)
                .content(asJsonString(shopVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));

        assertThat(shopRepository.findAll()).isEmpty();
    }

    @Test
    public void addShop_whenEmployeeLoggedShouldFail() throws Exception {
        UserEntity employee = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);
        userRepository.save(employee);
        final String token = getTokenForUser(employee);
        ShopVO shopVO = aShopVO()
                .withShopName("shop name")
                .withDescription("description")
                .build();

        mvc.perform(post("/shops")
                .header("Authorization", token)
                .content(asJsonString(shopVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));

        assertThat(shopRepository.findAll()).isEmpty();
    }

    @Test
    public void updateShop() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shop name test")
                .withDescription("a description")
                .build();
        shopRepository.save(shopEntity);

        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        owner.setShop(shopEntity);
        userRepository.save(owner);
        final String token = getTokenForUser(owner);
        ShopVO shopVO = aShopVO()
                .withId(shopEntity.getId())
                .withShopName("shop name")
                .withDescription("description")
                .build();

        mvc.perform(put("/shops")
                .header("Authorization", token)
                .content(asJsonString(shopVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<ShopEntity> shops = shopRepository.findAll();
        assertThat(shops).hasSize(1);
        assertThat(shops.get(0).getId()).isEqualTo(shopVO.getId());
        assertThat(shops.get(0).getShopName()).isEqualTo(shopVO.getShopName());
        assertThat(shops.get(0).getDescription()).isEqualTo(shopVO.getDescription());
    }

    @Test
    public void updateShop_whenNotExistsShouldFail() throws Exception {
        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        userRepository.save(owner);
        final String token = getTokenForUser(owner);
        ShopVO shopVO = aShopVO()
                .withId(1)
                .withShopName("shop name")
                .withDescription("description")
                .build();

        mvc.perform(put("/shops")
                .header("Authorization", token)
                .content(asJsonString(shopVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Ownerul nu are configurata nicio locatie.")));
        ;

        List<ShopEntity> shops = shopRepository.findAll();
        assertThat(shops).isEmpty();
    }

    @Test
    public void whenEmployeeLoggedShouldFail() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shop name test")
                .withDescription("a description")
                .build();
        shopRepository.save(shopEntity);

        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);
        owner.setShop(shopEntity);
        userRepository.save(owner);
        final String token = getTokenForUser(owner);
        ShopVO shopVO = aShopVO()
                .withId(1)
                .withShopName("shop name")
                .withDescription("description")
                .build();

        mvc.perform(put("/shops")
                .header("Authorization", token)
                .content(asJsonString(shopVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));
        ;
    }

    @Test
    public void whenClientLoggedShouldFail() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shop name test")
                .withDescription("a description")
                .build();
        shopRepository.save(shopEntity);

        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        client.setShop(shopEntity);
        userRepository.save(client);
        final String token = getTokenForUser(client);
        ShopVO shopVO = aShopVO()
                .withId(1)
                .withShopName("shop name")
                .withDescription("description")
                .build();

        mvc.perform(put("/shops")
                .header("Authorization", token)
                .content(asJsonString(shopVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));
        ;
    }

    @Test
    public void getAllShops() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shop name test")
                .withDescription("a description")
                .build();
        ShopEntity shopEntity2 = aShopEntity()
                .withShopName("shop name test2")
                .withDescription("a description")
                .build();
        shopRepository.saveAll(newArrayList(shopEntity, shopEntity2));
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);

        final String token = getTokenForUser(client);

        mvc.perform(get("/shops/all")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(shopEntity.getId())))
                .andExpect(jsonPath("$[0].shopName", is(shopEntity.getShopName())))
                .andExpect(jsonPath("$[0].description", is(shopEntity.getDescription())))
                .andExpect(jsonPath("$[1].id", is(shopEntity2.getId())))
                .andExpect(jsonPath("$[1].shopName", is(shopEntity2.getShopName())))
                .andExpect(jsonPath("$[1].description", is(shopEntity2.getDescription())));
    }

    @Test
    public void getShopEmployees() throws Exception {
        ShopEntity shopEntity = aShopEntity()
                .withShopName("shop name test")
                .withDescription("a description")
                .build();
        ShopEntity anotherShopEntity = aShopEntity()
                .withShopName("shop name test2")
                .withDescription("a description")
                .build();
        shopRepository.saveAll(newArrayList(shopEntity, anotherShopEntity));
        UserEntity owner = integrationTestHelper.getUserWithUserNameAndRole("anOwner", RolesEnum.ADMIN_ROLE);
        owner.setShop(shopEntity);
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("anEmployee");
        employeeEntity.setOwner(owner);
        EmployeeEntity employeeEntity2 = integrationTestHelper.getEmployeeWithUsername("anotherEmployee");
        employeeEntity2.setOwner(employeeEntity.getOwner());

        UserEntity anotherOwner = integrationTestHelper.getUserWithUserNameAndRole("anotherOwner", RolesEnum.ADMIN_ROLE);
        anotherOwner.setShop(anotherShopEntity);
        EmployeeEntity notDesiredEmployee = integrationTestHelper.getEmployeeWithUsername("notDesiredEmployee");
        notDesiredEmployee.setOwner(anotherOwner);
        userRepository.saveAll(newArrayList(owner, anotherOwner));
        employeeRepository.saveAll(newArrayList(employeeEntity, employeeEntity2, notDesiredEmployee));

        final String token = getTokenForUser(employeeEntity.getEmployeeDetails());

        mvc.perform(get("/shops/" + shopEntity.getId() + "/employees")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(3)))
                .andExpect(jsonPath("$[0].id", is(employeeEntity.getId())))
                .andExpect(jsonPath("$[1].id", is(employeeEntity2.getId())))
                .andExpect(jsonPath("$[2].id", is(owner.getId())));
    }

}
