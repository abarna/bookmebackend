package com.abarna.bookme.controllers;

import com.abarna.bookme.BookmeApplication;
import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.repositories.EmployeeRepository;
import com.abarna.bookme.repositories.RoleRepository;
import com.abarna.bookme.repositories.ShopRepository;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.utils.IntegrationTestHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static com.abarna.bookme.enums.RolesEnum.ADMIN_ROLE;
import static com.abarna.bookme.enums.RolesEnum.CLIENT_ROLE;
import static com.abarna.bookme.enums.RolesEnum.EMPLOYEE_ROLE;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = BookmeApplication.class)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
@AutoConfigureMockMvc(addFilters = false)
class OwnerControllerIntegrationTest extends AuthenticationUtils {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private IntegrationTestHelper integrationTestHelper;

    @BeforeEach
    void setUp() {
        employeeRepository.deleteAll();
        userRepository.deleteAll();
        shopRepository.deleteAll();
        roleRepository.deleteAll();
    }

    @Test
    void getOwnerEmployees() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("employee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        final String token = getTokenForUser(employee.getOwner());

        mvc.perform(get("/owner/employees")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(employee.getId())))
                .andExpect(jsonPath("$[0].employeeDetails.id", is(employee.getEmployeeDetails().getId())))
                .andExpect(jsonPath("$[0].employeeDetails.firstName", is(employee.getEmployeeDetails().getFirstName())))
                .andExpect(jsonPath("$[0].employeeDetails.lastName", is(employee.getEmployeeDetails().getLastName())))
                .andExpect(jsonPath("$[0].employeeDetails.email", is(employee.getEmployeeDetails().getEmail())))
                .andExpect(jsonPath("$[0].employeeDetails.username", is(employee.getEmployeeDetails().getUsername())))
                .andExpect(jsonPath("$[0].employeeDetails.roles[0]", is(EMPLOYEE_ROLE.name())))
                .andExpect(jsonPath("$[0].ownerId", is(employee.getOwner().getId())))
                .andExpect(status().isOk());
    }

    @Test
    public void getOwnerEmployees_emptyList() throws Exception {
        UserEntity owner = integrationTestHelper.getUserWithRole(ADMIN_ROLE);
        userRepository.save(owner);
        final String token = getTokenForUser(owner);

        mvc.perform(get("/owner/employees")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(new ArrayList())));
    }

    @Test
    public void getOwnerEmployees_shouldFailWhenClientLogged() throws Exception {
        UserEntity owner = integrationTestHelper.getUserWithRole(CLIENT_ROLE);
        userRepository.save(owner);
        final String token = getTokenForUser(owner);

        mvc.perform(get("/owner/employees")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));
    }

    @Test
    public void getOwnerEmployees_shouldFailWhenEmployeeLogged() throws Exception {
        UserEntity owner = integrationTestHelper.getUserWithRole(EMPLOYEE_ROLE);
        userRepository.save(owner);
        final String token = getTokenForUser(owner);

        mvc.perform(get("/owner/employees")
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Userul logat nu este owner.")));
    }
}
