package com.abarna.bookme.controllers;

import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.security.JwtTokenUtil;
import com.abarna.bookme.utils.IntegrationTestHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

public class AuthenticationUtils {

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private AuthenticationManager authenticationManager;

    String getTokenForUser(UserEntity userEntity) {
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userEntity.getUsername(),
                        IntegrationTestHelper.RANDOM_PASS
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);


        UserDetails userDetails = userDetailsService.loadUserByUsername(userEntity.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return "Bearer " + token;
    }
}
