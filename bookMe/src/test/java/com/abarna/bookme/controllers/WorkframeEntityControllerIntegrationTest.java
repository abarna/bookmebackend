package com.abarna.bookme.controllers;

import com.abarna.bookme.BookmeApplication;
import com.abarna.bookme.converters.WorkframeConverter;
import com.abarna.bookme.entities.AppointmentEntity;
import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.entities.WorkframeEntity;
import com.abarna.bookme.enums.RolesEnum;
import com.abarna.bookme.repositories.AppointmentRepository;
import com.abarna.bookme.repositories.EmployeeRepository;
import com.abarna.bookme.repositories.UserRepository;
import com.abarna.bookme.repositories.WorkframeRepository;
import com.abarna.bookme.util.DateUtils;
import com.abarna.bookme.utils.IntegrationTestHelper;
import com.abarna.bookme.vos.WorkframeVO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.abarna.bookme.entities.AppointmentEntityBuilder.anAppointmentEntity;
import static com.abarna.bookme.entities.WorkframeEntityBuilder.aWorkframeEntity;
import static com.abarna.bookme.enums.AppointmentTimeframeType.HALF;
import static com.abarna.bookme.enums.AppointmentTimeframeType.HOUR;
import static com.abarna.bookme.enums.AppointmentTimeframeType.QUARTER;
import static com.abarna.bookme.enums.RolesEnum.CLIENT_ROLE;
import static com.abarna.bookme.util.DateUtils.getDateWithHour;
import static com.abarna.bookme.util.DateUtils.getDateWithMaxHour;
import static com.abarna.bookme.util.DateUtils.getDateWithMinHour;
import static com.abarna.bookme.util.DateUtils.getNextNTimeFrames;
import static com.abarna.bookme.util.DateUtils.getNextTimeframe;
import static com.abarna.bookme.utils.IntegrationTestHelper.asJsonString;
import static com.abarna.bookme.vos.WorkframeVOBuilder.aWorkframeVO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = BookmeApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class WorkframeEntityControllerIntegrationTest extends AuthenticationUtils {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private IntegrationTestHelper integrationTestHelper;
    @Autowired
    private WorkframeConverter workframeConverter;
    @Autowired
    private WorkframeRepository workframeRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private AppointmentRepository appointmentRepository;

    private String formattedDate;

    @BeforeEach
    void setUp() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        formattedDate = formatter.format(getDateWithMinHour(new Date()));
    }

    @AfterEach
    void tearDown() {
        appointmentRepository.deleteAll();
        workframeRepository.deleteAll();
        employeeRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    @Transactional
    public void addWorkFrameAsEmployee() throws Exception {
        UserEntity employeeUser = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);
        userRepository.save(employeeUser);
        final String token = getTokenForUser(employeeUser);
        Date start = new Date();
        Calendar oneDayAddedCalendar = Calendar.getInstance();
        oneDayAddedCalendar.setTime(start);
        oneDayAddedCalendar.add(Calendar.DATE, 1);
        Calendar twoDaysAddedCalendar = Calendar.getInstance();
        twoDaysAddedCalendar.setTime(start);
        twoDaysAddedCalendar.add(Calendar.DATE, 2);
        Calendar threeDaysAddedCalendar = Calendar.getInstance();
        threeDaysAddedCalendar.setTime(start);
        threeDaysAddedCalendar.add(Calendar.DATE, 3);
        WorkframeEntity existingWorkframeInFutureWhichIsNotInterfeering = aWorkframeEntity()
                .withStartDate(twoDaysAddedCalendar.getTime())
                .withEndDate(threeDaysAddedCalendar.getTime())
                .withStartHour(getDateWithHour(new Date(), 15))
                .withEndHour(getDateWithHour(new Date(), 15))
                .withUserEntity(employeeUser)
                .build();
        workframeRepository.save(existingWorkframeInFutureWhichIsNotInterfeering);

        WorkframeVO workframeVO = aWorkframeVO()
                .withStartDate(new Date(start.getTime() + 5000))
                .withStartHour(DateUtils.getDateWithHour(start, 1))
                .withEndHour(DateUtils.getDateWithHour(start, 19))
                .withEndDate(oneDayAddedCalendar.getTime())
                .withWorkerId(employeeUser.getId())
                .build();

        mvc.perform(post("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        List<WorkframeEntity> workerWorkframeEntities = workframeRepository.findAll();
        assertThat(workerWorkframeEntities).hasSize(2);
        WorkframeEntity savedWorkframe = workerWorkframeEntities.stream()
                .filter(entity -> !entity.getId().equals(existingWorkframeInFutureWhichIsNotInterfeering.getId()))
                .findAny()
                .get();
        Calendar savedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(savedWorkframe.getStartDate());
        Calendar expectedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(workframeVO.getStartDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));
        savedDateCalendar.setTime(savedWorkframe.getEndDate());
        expectedDateCalendar.setTime(workframeVO.getEndDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));
        assertThat(savedWorkframe.getStartHour()).isEqualTo(workframeVO.getStartHour());
        assertThat(savedWorkframe.getEndHour()).isEqualTo(workframeVO.getEndHour());
        assertThat(savedWorkframe.getUserEntity().getId()).isEqualTo(workframeVO.getWorkerId());
    }

    @Test
    @Transactional
    public void addWorkFrameAsEmployee_whenWorkframeStartOverlapping_shouldFail() throws Exception {
        UserEntity employeeUser = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);
        userRepository.save(employeeUser);
        final String token = getTokenForUser(employeeUser);
        Date start = new Date();
        Calendar oneDayAddedCalendar = Calendar.getInstance();
        oneDayAddedCalendar.setTime(start);
        oneDayAddedCalendar.add(Calendar.DATE, 1);
        WorkframeEntity workframeEntity = aWorkframeEntity()
                .withUserEntity(employeeUser)
                .withStartDate(getDateWithMinHour(oneDayAddedCalendar.getTime()))
                .withEndDate(getDateWithMinHour(oneDayAddedCalendar.getTime()))
                .build();
        workframeRepository.save(workframeEntity);

        WorkframeVO workframeVO = aWorkframeVO()
                .withStartDate(getDateWithMinHour(oneDayAddedCalendar.getTime()))
                .withEndDate(getDateWithMinHour(oneDayAddedCalendar.getTime()))
                .withWorkerId(employeeUser.getId())
                .build();

        mvc.perform(post("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Ai deja un program stabilit pentru aceasta zi.")));
        assertThat(workframeRepository.findAll()).hasSize(1)
                .containsOnly(workframeEntity);
    }

    @Test
    @Transactional
    public void addWorkFrameAsEmployee_whenWorkframeEndOverlapping_shouldFail() throws Exception {
        UserEntity employeeUser = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);
        userRepository.save(employeeUser);
        final String token = getTokenForUser(employeeUser);
        Date start = new Date();
        Calendar oneDayAddedCalendar = Calendar.getInstance();
        oneDayAddedCalendar.setTime(start);
        oneDayAddedCalendar.add(Calendar.DATE, 1);
        Calendar twoDaysAddedCalendar = Calendar.getInstance();
        twoDaysAddedCalendar.setTime(start);
        twoDaysAddedCalendar.add(Calendar.DATE, 2);
        Calendar threeDaysAddedCalendar = Calendar.getInstance();
        threeDaysAddedCalendar.setTime(start);
        threeDaysAddedCalendar.add(Calendar.DATE, 3);
        Calendar fourDaysAddedCalendar = Calendar.getInstance();
        fourDaysAddedCalendar.setTime(start);
        fourDaysAddedCalendar.add(Calendar.DATE, 4);
        WorkframeEntity workframeEntity = aWorkframeEntity()
                .withUserEntity(employeeUser)
                .withStartDate(oneDayAddedCalendar.getTime())
                .withEndDate(threeDaysAddedCalendar.getTime())
                .build();
        workframeRepository.save(workframeEntity);

        WorkframeVO workframeVO = aWorkframeVO()
                .withStartDate(twoDaysAddedCalendar.getTime())
                .withEndDate(fourDaysAddedCalendar.getTime())
                .withWorkerId(employeeUser.getId())
                .build();

        mvc.perform(post("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Ai deja un program stabilit pentru aceasta zi.")));
        assertThat(workframeRepository.findAll()).hasSize(1)
                .containsOnly(workframeEntity);
    }

    @Test
    @Transactional
    public void addWorkFrameAsEmployee_whenWorkframeStartAndEndOverlapping_shouldFail() throws Exception {
        UserEntity employeeUser = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);
        userRepository.save(employeeUser);
        final String token = getTokenForUser(employeeUser);
        Date start = new Date();
        Calendar oneDayAgo = Calendar.getInstance();
        oneDayAgo.setTime(start);
        oneDayAgo.add(Calendar.DATE, -1);
        Calendar oneDayAddedCalendar = Calendar.getInstance();
        oneDayAddedCalendar.setTime(start);
        oneDayAddedCalendar.add(Calendar.DATE, 1);
        Calendar twoDaysAddedCalendar = Calendar.getInstance();
        twoDaysAddedCalendar.setTime(start);
        twoDaysAddedCalendar.add(Calendar.DATE, 2);
        WorkframeEntity workframeEntity = aWorkframeEntity()
                .withUserEntity(employeeUser)
                .withStartDate(start)
                .withEndDate(twoDaysAddedCalendar.getTime())
                .build();
        workframeRepository.save(workframeEntity);

        WorkframeVO workframeVO = aWorkframeVO()
                .withStartDate(getDateWithMinHour(oneDayAddedCalendar.getTime()))
                .withEndDate(twoDaysAddedCalendar.getTime())
                .withWorkerId(employeeUser.getId())
                .build();

        mvc.perform(post("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Ai deja un program stabilit pentru aceasta zi.")));
        assertThat(workframeRepository.findAll()).hasSize(1)
                .containsOnly(workframeEntity);
    }

    @Test
    public void addWorkFrameAsEmployee_forAnotherEmployeeShouldFail() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("anEmployee");
        EmployeeEntity anotherEmployee = integrationTestHelper.getEmployeeWithUsername("anotherEmployee");
        userRepository.saveAll(newArrayList(employee.getOwner(), anotherEmployee.getOwner()));
        employeeRepository.saveAll(newArrayList(employee, anotherEmployee));
        final String token = getTokenForUser(employee.getEmployeeDetails());
        Date start = new Date();

        WorkframeVO workframeVO = aWorkframeVO()
                .withStartDate(new Date(start.getTime() + 5000))
                .withStartHour(DateUtils.getDateWithHour(start, 9))
                .withEndHour(DateUtils.getDateWithHour(start, 18))
                .withEndDate(getDateWithMaxHour(start))
                .withWorkerId(anotherEmployee.getId())
                .build();

        mvc.perform(post("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Workframe-ul nu poate fi adaugat pentru acest angajat.")));
        assertThat(workframeRepository.findAll()).isEmpty();
    }

    @Test
    @Transactional
    public void addWorkFrameAsOwner() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("anEmployee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        final String token = getTokenForUser(employee.getOwner());
        Date start = getDateWithHour(new Date(), 3);

        WorkframeVO workframeVO = aWorkframeVO()
                .withStartDate(new Date(start.getTime() + 5000))
                .withStartHour(DateUtils.getDateWithHour(start, 9))
                .withEndHour(DateUtils.getDateWithHour(start, 18))
                .withEndDate(new Date(start.getTime() + 5000))
                .build();

        mvc.perform(post("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        workframeVO.setWorkerId(employee.getOwner().getId());
        List<WorkframeEntity> workerWorkframeEntities = workframeRepository.findAll();
        assertThat(workerWorkframeEntities).hasSize(1);
        assertThat(workerWorkframeEntities.get(0))
                .isEqualToIgnoringGivenFields(workframeConverter.toEntity(workframeVO), "id");
    }

    @Test
    @Transactional
    public void addWorkFrameAsOwner_forEmployee() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("anEmployee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        final String token = getTokenForUser(employee.getOwner());
        Date start = getDateWithHour(new Date(), 3);

        WorkframeVO workframeVO = aWorkframeVO()
                .withStartDate(new Date(start.getTime() + 5000))
                .withStartHour(DateUtils.getDateWithHour(start, 9))
                .withEndHour(DateUtils.getDateWithHour(start, 18))
                .withEndDate(new Date(start.getTime() + 5000))
                .withWorkerId(employee.getId())
                .build();

        mvc.perform(post("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<WorkframeEntity> workerWorkframeEntities = workframeRepository.findAll();
        assertThat(workerWorkframeEntities).hasSize(1);
        assertThat(workerWorkframeEntities.get(0))
                .isEqualToIgnoringGivenFields(workframeConverter.toEntity(workframeVO), "id");
    }

    @Test
    public void addWorkFrameAsOwner_whenForAnotherShopEmployee_shouldFail() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("anEmployee");
        EmployeeEntity anotherEmployee = integrationTestHelper.getEmployeeWithUsernameAndOwnerUsername("anotherEmployee", "anotherOwner");
        userRepository.saveAll(newArrayList(employee.getOwner(), anotherEmployee.getOwner()));
        employeeRepository.saveAll(newArrayList(employee, anotherEmployee));
        final String token = getTokenForUser(employee.getOwner());
        Date start = new Date();

        WorkframeVO workframeVO = aWorkframeVO()
                .withStartDate(new Date(start.getTime() + 5000))
                .withStartHour(DateUtils.getDateWithHour(start, 9))
                .withEndHour(DateUtils.getDateWithHour(start, 18))
                .withEndDate(getDateWithMaxHour(start))
                .withWorkerId(anotherEmployee.getId())
                .build();

        mvc.perform(post("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Angajatul este invalid.")));
        assertThat(workframeRepository.findAll()).isEmpty();
    }

    @Test
    public void addWorkFrameInThePast_shouldFail() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("anEmployee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        final String token = getTokenForUser(employee.getOwner());
        Date start = new Date();
        Calendar oneDayAgo = Calendar.getInstance();
        oneDayAgo.setTime(start);
        oneDayAgo.add(Calendar.DATE, -1);

        WorkframeVO workframeVO = aWorkframeVO()
                .withStartDate(oneDayAgo.getTime())
                .withStartHour(DateUtils.getDateWithHour(start, 9))
                .withEndHour(DateUtils.getDateWithHour(start, 18))
                .withEndDate(oneDayAgo.getTime())
                .withWorkerId(employee.getId())
                .build();

        mvc.perform(post("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Programul nu poate fi adaugat pentru o zi din trecut.")));
        assertThat(workframeRepository.findAll()).isEmpty();
    }

    @Test
    public void addWorkFrameAsClient_shouldFail() throws Exception {
        UserEntity userEntity = integrationTestHelper.getUserWithRole(CLIENT_ROLE);
        userRepository.save(userEntity);
        final String token = getTokenForUser(userEntity);
        Date start = new Date();

        WorkframeVO workframeVO = aWorkframeVO()
                .withStartDate(new Date(start.getTime() + 5000))
                .withStartHour(DateUtils.getDateWithHour(start, 9))
                .withEndHour(DateUtils.getDateWithHour(start, 18))
                .withEndDate(getDateWithMaxHour(start))
                .withWorkerId(userEntity.getId())
                .build();

        mvc.perform(post("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Nu ai drepturi sa adaugi acest workframe.")));
        assertThat(workframeRepository.findAll()).isEmpty();
    }

    @Test
    @Transactional
    public void getWorkframesAsOwner() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("anEmployee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        final String token = getTokenForUser(employee.getOwner());
        Date start = new Date();
        WorkframeEntity employeeWorkframe = aWorkframeEntity()
                .withUserEntity(employee.getEmployeeDetails())
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        WorkframeEntity ownerWorkframe = aWorkframeEntity()
                .withUserEntity(employee.getOwner())
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 6))
                .withEndHour(DateUtils.getDateWithHour(start, 16))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        workframeRepository.saveAll(newArrayList(employeeWorkframe, ownerWorkframe));

        mvc.perform(get("/workframes/" + employee.getOwner().getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(1)));
        List<WorkframeEntity> byUserEntity_id = workframeRepository.findByUserEntity_Id(employee.getOwner().getId());
        assertThat(byUserEntity_id).hasSize(1);
        assertThat(byUserEntity_id.get(0)).isEqualToComparingFieldByField(ownerWorkframe);
    }

    @Test
    @Transactional
    public void getWorkframesAsEmployee() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("anEmployee");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        final String token = getTokenForUser(employee.getOwner());
        Date start = new Date();
        WorkframeEntity employeeWorkframe = aWorkframeEntity()
                .withUserEntity(employee.getEmployeeDetails())
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        WorkframeEntity ownerWorkframe = aWorkframeEntity()
                .withUserEntity(employee.getOwner())
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 6))
                .withEndHour(DateUtils.getDateWithHour(start, 16))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        workframeRepository.saveAll(newArrayList(employeeWorkframe, ownerWorkframe));

        mvc.perform(get("/workframes/" + employee.getOwner().getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(1)));
        List<WorkframeEntity> byUserEntity_id = workframeRepository.findByUserEntity_Id(employee.getOwner().getId());
        assertThat(byUserEntity_id).hasSize(1);
        assertThat(byUserEntity_id.get(0)).isEqualToComparingFieldByField(ownerWorkframe);
    }

    @Test
    public void getWorkframesAsClient_shouldFail() throws Exception {
        UserEntity userEntity = integrationTestHelper.getUserWithRole(CLIENT_ROLE);
        userRepository.save(userEntity);
        final String token = getTokenForUser(userEntity);
        Date start = new Date();
        WorkframeEntity employeeWorkframe = aWorkframeEntity()
                .withUserEntity(userEntity)
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        WorkframeEntity ownerWorkframe = aWorkframeEntity()
                .withUserEntity(userEntity)
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 6))
                .withEndHour(DateUtils.getDateWithHour(start, 16))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        workframeRepository.saveAll(newArrayList(employeeWorkframe, ownerWorkframe));

        mvc.perform(get("/workframes/" + userEntity.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Nu ai drepturi pentru a verifica workframe-urile")));
    }

    @Test
    void getAvailableWorkframesForQuarter_whenNoAppointment() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        Date start = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(client)
                .withStartDate(getDateWithMinHour(start))
                .withStartHour(getDateWithMinHour(start))
                .withEndHour(getDateWithMaxHour(start))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + client.getId() + "?appointmentType=QUARTER&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(92)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForQuarter_whenAlreadyHaving2AppointmentsWithQuartersShouldNotReturnWorkframesAsAvailable() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        UserEntity employeeEntity = integrationTestHelper.getUserWithUserNameAndRole("asd2",
                RolesEnum.CLIENT_ROLE);
        UserEntity anotherIgnoredEmployee = integrationTestHelper.getUserWithUserNameAndRole("asd2",
                RolesEnum.CLIENT_ROLE);
        userRepository.saveAll(newArrayList(client, employeeEntity, anotherIgnoredEmployee));
        Date currentDate = new Date();
        Date firstBusyQuarter = getNextTimeframe(getDateWithHour(currentDate, 9), QUARTER);
        Date secondBusyQuarter = getNextTimeframe(firstBusyQuarter, QUARTER);
        Date thirdBusyQuarter = getNextTimeframe(secondBusyQuarter, QUARTER);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(firstBusyQuarter)
                .withEnd(getNextTimeframe(firstBusyQuarter, QUARTER))
                .build();

        AppointmentEntity appointmentEntity2 = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(secondBusyQuarter)
                .withEnd(getNextTimeframe(secondBusyQuarter, QUARTER))
                .build();
        AppointmentEntity appointmentForIgnoredEmployee = anAppointmentEntity()
                .withClient(client)
                .withWorker(anotherIgnoredEmployee)
                .withStart(getNextTimeframe(thirdBusyQuarter, QUARTER))
                .withEnd(getNextTimeframe(thirdBusyQuarter, QUARTER))
                .build();
        appointmentRepository.saveAll(newArrayList(appointmentEntity, appointmentEntity2, appointmentForIgnoredEmployee));
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity)
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + employeeEntity.getId() + "?appointmentType=QUARTER&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(90)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForQuarter_whenAlreadyHavingAnAppointmentWithHalfShouldNotReturnWorkframesAsAvailable() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        UserEntity employeeEntity = integrationTestHelper.getUserWithUserNameAndRole("asd2", RolesEnum.CLIENT_ROLE);
        UserEntity anotherIgnoredEmployee = integrationTestHelper.getUserWithUserNameAndRole("asd3", RolesEnum.CLIENT_ROLE);
        userRepository.saveAll(newArrayList(client, employeeEntity, anotherIgnoredEmployee));
        Date startBusyQuarter = getDateWithHour(new Date(), 9);
        Date endBusyQuarter = getNextTimeframe(startBusyQuarter, HALF);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(startBusyQuarter)
                .withEnd(endBusyQuarter)
                .build();
        appointmentRepository.save(appointmentEntity);
        Date currentDate = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity)
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + employeeEntity.getId() + "?appointmentType=QUARTER&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(90)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForQuarter_whenAlreadyHavingAnAppointmentWithHourShouldNotReturnWorkframesAsAvailable() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        UserEntity employeeEntity = integrationTestHelper.getUserWithUserNameAndRole("asd2", RolesEnum.CLIENT_ROLE);
        UserEntity anotherIgnoredEmployee = integrationTestHelper.getUserWithUserNameAndRole("asd3", RolesEnum.CLIENT_ROLE);
        userRepository.saveAll(newArrayList(client, employeeEntity, anotherIgnoredEmployee));
        Date startBusyQuarter = getDateWithHour(new Date(), 9);
        Date endBusyQuarter = getNextTimeframe(startBusyQuarter, HOUR);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(startBusyQuarter)
                .withEnd(endBusyQuarter)
                .build();
        appointmentRepository.save(appointmentEntity);
        Date currentDate = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity)
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + employeeEntity.getId() + "?appointmentType=QUARTER&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(88)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForHalf_whenNoAppointment() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        Date currentDate = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(client)
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + client.getId() + "?appointmentType=HALF&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(91)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForHalf_whenAlreadyHaving2QuarterAppointmentsShouldNotReturnWorkframesAsAvailable() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        UserEntity employeeEntity = integrationTestHelper.getUserWithUserNameAndRole("asd2", RolesEnum.CLIENT_ROLE);
        UserEntity anotherIgnoredEmployee = integrationTestHelper.getUserWithUserNameAndRole("asd3", RolesEnum.CLIENT_ROLE);
        userRepository.saveAll(newArrayList(client, employeeEntity, anotherIgnoredEmployee));
        Date startFirstBusyQuarter = getDateWithHour(new Date(), 9);
        Date endFirstBusyQuarter = getNextTimeframe(startFirstBusyQuarter, QUARTER);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(startFirstBusyQuarter)
                .withEnd(endFirstBusyQuarter)
                .build();
        List<Date> nextNQuarterTimeFrames = getNextNTimeFrames(getDateWithHour(new Date(), 9), QUARTER, 6);
        AppointmentEntity appointmentEntity2 = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(nextNQuarterTimeFrames.get(4))
                .withEnd(nextNQuarterTimeFrames.get(5))
                .build();
        AppointmentEntity appointmentForIgnoredEmployee = anAppointmentEntity()
                .withClient(client)
                .withWorker(anotherIgnoredEmployee)
                .withStart(getDateWithHour(startFirstBusyQuarter, 12))
                .withEnd(getDateWithHour(startFirstBusyQuarter, 13))
                .build();
        appointmentRepository.saveAll(newArrayList(appointmentEntity, appointmentEntity2, appointmentForIgnoredEmployee));
        Date currentDate = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity)
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + employeeEntity.getId() + "?appointmentType=HALF&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(87)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForHalf_whenAlreadyHavingAnAppointmentWithHalfShouldNotReturnWorkframesAsAvailable() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        UserEntity employeeEntity = integrationTestHelper.getUserWithUserNameAndRole("asd2", RolesEnum.CLIENT_ROLE);
        UserEntity anotherIgnoredEmployee = integrationTestHelper.getUserWithUserNameAndRole("asd3", RolesEnum.CLIENT_ROLE);
        userRepository.saveAll(newArrayList(client, employeeEntity, anotherIgnoredEmployee));
        Date startBusyQuarter = getDateWithHour(new Date(), 15);
        Date endBusyQuarter = getNextTimeframe(startBusyQuarter, HALF);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(startBusyQuarter)
                .withEnd(endBusyQuarter)
                .build();
        appointmentRepository.save(appointmentEntity);
        Date currentDate = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity)
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + employeeEntity.getId() + "?appointmentType=HALF&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(88)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForHalf_whenAlreadyHavingAnAppointmentWithHourShouldNotReturnWorkframesAsAvailable() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        UserEntity employeeEntity = integrationTestHelper.getUserWithUserNameAndRole("asd2", RolesEnum.CLIENT_ROLE);
        UserEntity anotherIgnoredEmployee = integrationTestHelper.getUserWithUserNameAndRole("asd3", RolesEnum.CLIENT_ROLE);
        userRepository.saveAll(newArrayList(client, employeeEntity, anotherIgnoredEmployee));
        Date startBusyQuarter = getDateWithHour(new Date(), 10);
        Date endBusyQuarter = getNextTimeframe(startBusyQuarter, HOUR);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(startBusyQuarter)
                .withEnd(endBusyQuarter)
                .build();
        appointmentRepository.save(appointmentEntity);
        Date currentDate = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity)
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + employeeEntity.getId() + "?appointmentType=HALF&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(86)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForHour_whenNoAppointment() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        userRepository.save(client);
        Date currentDate = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(client)
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + client.getId() + "?appointmentType=HOUR&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(89)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForHour_whenAlreadyHaving2AppointmentsShouldNotReturnWorkframesAsAvailable() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        UserEntity employeeEntity = integrationTestHelper.getUserWithUserNameAndRole("asd2", RolesEnum.CLIENT_ROLE);
        UserEntity anotherIgnoredEmployee = integrationTestHelper.getUserWithUserNameAndRole("asd3", RolesEnum.CLIENT_ROLE);
        userRepository.saveAll(newArrayList(client, employeeEntity, anotherIgnoredEmployee));
        Date firstBusyQuarter = getNextTimeframe(getDateWithHour(new Date(), 9), QUARTER);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(firstBusyQuarter)
                .withEnd(getNextTimeframe(firstBusyQuarter, QUARTER))
                .build();
        List<Date> nextNQuarterTimeFrames = getNextNTimeFrames(getDateWithHour(new Date(), 9), QUARTER, 6);
        AppointmentEntity appointmentEntity2 = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(nextNQuarterTimeFrames.get(4))
                .withEnd(nextNQuarterTimeFrames.get(5))
                .build();
        Date startAnotherBusyQuarter = getDateWithHour(firstBusyQuarter, 11);
        Date endAnotherBusyQuarter = getNextTimeframe(startAnotherBusyQuarter, QUARTER);
        AppointmentEntity appointmentForIgnoredEmployee = anAppointmentEntity()
                .withClient(client)
                .withWorker(anotherIgnoredEmployee)
                .withStart(startAnotherBusyQuarter)
                .withEnd(endAnotherBusyQuarter)
                .build();
        appointmentRepository.saveAll(newArrayList(appointmentEntity, appointmentEntity2, appointmentForIgnoredEmployee));
        Date currentDate = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity)
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + employeeEntity.getId() + "?appointmentType=HOUR&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(81)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForHour_whenAlreadyHavingAnAppointmentWithHalfShouldNotReturnWorkframesAsAvailable() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        UserEntity employeeEntity = integrationTestHelper.getUserWithUserNameAndRole("asd2", RolesEnum.CLIENT_ROLE);
        UserEntity anotherIgnoredEmployee = integrationTestHelper.getUserWithUserNameAndRole("asd3", RolesEnum.CLIENT_ROLE);
        userRepository.saveAll(newArrayList(client, employeeEntity, anotherIgnoredEmployee));
        Date startBusyQuarter = getDateWithHour(new Date(), 15);
        Date endBusyQuarter = getNextTimeframe(startBusyQuarter, HALF);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(startBusyQuarter)
                .withEnd(endBusyQuarter)
                .build();
        appointmentRepository.save(appointmentEntity);
        Date currentDate = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity)
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + employeeEntity.getId() + "?appointmentType=HOUR&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(84)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForHour_whenAlreadyHavingAnAppointmentWithHourShouldNotReturnWorkframesAsAvailable() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        UserEntity employeeEntity = integrationTestHelper.getUserWithUserNameAndRole("asd2", RolesEnum.CLIENT_ROLE);
        UserEntity anotherIgnoredEmployee = integrationTestHelper.getUserWithUserNameAndRole("asd3", RolesEnum.CLIENT_ROLE);
        userRepository.saveAll(newArrayList(client, employeeEntity, anotherIgnoredEmployee));
        Date dateWithTenHour = getDateWithHour(new Date(), 10);
        Date startBusyQuarter = getNextTimeframe(dateWithTenHour, QUARTER);
        Date endBusyQuarter = getNextTimeframe(startBusyQuarter, HOUR);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(startBusyQuarter)
                .withEnd(endBusyQuarter)
                .build();
        appointmentRepository.save(appointmentEntity);
        Date currentDate = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withUserEntity(employeeEntity)
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + employeeEntity.getId() + "?appointmentType=HOUR&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.length()", is(82)))
                .andExpect(status().isOk());
    }

    @Test
    void getAvailableWorkframesForHalf_noWorkframeSetForEmployee_shouldThrowException() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        UserEntity employeeEntity = integrationTestHelper.getUserWithUserNameAndRole("asd2", RolesEnum.CLIENT_ROLE);
        UserEntity anotherIgnoredEmployee = integrationTestHelper.getUserWithUserNameAndRole("asd3", RolesEnum.CLIENT_ROLE);
        userRepository.saveAll(newArrayList(client, employeeEntity, anotherIgnoredEmployee));
        Date startBusyQuarter = getDateWithHour(new Date(), 15);
        Date endBusyQuarter = getNextTimeframe(startBusyQuarter, HALF);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(startBusyQuarter)
                .withEnd(endBusyQuarter)
                .build();
        appointmentRepository.save(appointmentEntity);
        final String token = getTokenForUser(client);

        mvc.perform(get("/workframes/available/" + employeeEntity.getId() + "?appointmentType=HALF&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Persoana selectata nu lucreaza in aceasta zi. Momentan nicio data nu este disponibila.")));
    }

    @Test
    void getAvailableWorkframesForHalf_noWorkframeSetForEmployeeButHasOneInTheFuture_shouldThrowException() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(RolesEnum.CLIENT_ROLE);
        UserEntity employeeEntity = integrationTestHelper.getUserWithUserNameAndRole("asd2", RolesEnum.CLIENT_ROLE);
        UserEntity anotherIgnoredEmployee = integrationTestHelper.getUserWithUserNameAndRole("asd3", RolesEnum.CLIENT_ROLE);
        userRepository.saveAll(newArrayList(client, employeeEntity, anotherIgnoredEmployee));
        Date startBusyQuarter = getDateWithHour(new Date(), 15);
        Date endBusyQuarter = getNextTimeframe(startBusyQuarter, HALF);
        AppointmentEntity appointmentEntity = anAppointmentEntity()
                .withClient(client)
                .withWorker(employeeEntity)
                .withStart(startBusyQuarter)
                .withEnd(endBusyQuarter)
                .build();
        appointmentRepository.save(appointmentEntity);
        Calendar oneDayAddedCalendar = Calendar.getInstance();
        oneDayAddedCalendar.setTime(startBusyQuarter);
        oneDayAddedCalendar.add(Calendar.DATE, 1);
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(getDateWithMinHour(oneDayAddedCalendar.getTime()))
                .withStartHour(getDateWithMinHour(oneDayAddedCalendar.getTime()))
                .withEndHour(getDateWithMaxHour(oneDayAddedCalendar.getTime()))
                .withEndDate(getDateWithMaxHour(oneDayAddedCalendar.getTime()))
                .withUserEntity(employeeEntity)
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        mvc.perform(get("/workframes/available/" + employeeEntity.getId() + "?appointmentType=HALF&date=" + formattedDate)
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Persoana selectata nu lucreaza in aceasta zi. " +
                        "Prima zi disponibila este " + formatter.format(getDateWithMinHour(oneDayAddedCalendar.getTime())))));
    }

    @Test
    public void updateWorkframes_ownerUpdatesHisOwnWorkframe() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("asd");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        Date currentDate = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(getDateWithMinHour(currentDate))
                .withStartHour(getDateWithMinHour(currentDate))
                .withEndHour(getDateWithMaxHour(currentDate))
                .withEndDate(getDateWithMaxHour(currentDate))
                .withUserEntity(employee.getOwner())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(employee.getOwner());

        Date start = new Date();
        WorkframeVO workframeVO = aWorkframeVO()
                .withId(workframe.getId())
                .withWorkerId(employee.getOwner().getId())
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 1))
                .withEndHour(DateUtils.getDateWithHour(start, 2))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        mvc.perform(put("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<WorkframeEntity> workframes = workframeRepository.findAll();
        assertThat(workframes).hasSize(1);
        assertThat(workframes.get(0).getId()).isEqualTo(workframeVO.getId());
        assertThat(workframes.get(0).getUserEntity().getId()).isEqualTo(employee.getOwner().getId());
        Calendar savedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(workframes.get(0).getStartDate());
        Calendar expectedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(workframeVO.getStartDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));

        savedDateCalendar.setTime(workframes.get(0).getEndDate());
        expectedDateCalendar.setTime(workframeVO.getEndDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));
        assertThat(workframes.get(0).getStartHour().getTime()).isEqualTo(workframeVO.getStartHour().getTime());
        assertThat(workframes.get(0).getEndHour().getTime()).isEqualTo(workframeVO.getEndHour().getTime());
    }

    @Test
    public void updateWorkframes_ownerUpdatesValidEmployeeWorkframe() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("asd");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        Date start = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .withUserEntity(employee.getOwner())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(employee.getOwner());

        WorkframeVO workframeVO = aWorkframeVO()
                .withId(workframe.getId())
                .withWorkerId(employee.getEmployeeDetails().getId())
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 1))
                .withEndHour(DateUtils.getDateWithHour(start, 2))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        mvc.perform(put("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<WorkframeEntity> workframes = workframeRepository.findAll();
        assertThat(workframes).hasSize(1);
        assertThat(workframes.get(0).getId()).isEqualTo(workframeVO.getId());
        assertThat(workframes.get(0).getUserEntity().getId()).isEqualTo(employee.getId());
        Calendar savedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(workframes.get(0).getStartDate());
        Calendar expectedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(workframeVO.getStartDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));

        savedDateCalendar.setTime(workframes.get(0).getEndDate());
        expectedDateCalendar.setTime(workframeVO.getEndDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));

        assertThat(workframes.get(0).getStartHour().getTime()).isEqualTo(workframeVO.getStartHour().getTime());
        assertThat(workframes.get(0).getEndHour().getTime()).isEqualTo(workframeVO.getEndHour().getTime());
    }

    @Test
    public void updateWorkframes_ownerUpdatesInvalidEmployeeWorkframe() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("asd");
        EmployeeEntity invalidEmployer = integrationTestHelper.getEmployeeWithUsernameAndOwnerUsername("anotherEmp", "anotherOwner");
        userRepository.saveAll(newArrayList(employee.getOwner(), invalidEmployer.getOwner()));
        employeeRepository.saveAll(newArrayList(employee, invalidEmployer));
        Date start = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .withUserEntity(employee.getEmployeeDetails())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(employee.getOwner());

        WorkframeVO workframeVO = aWorkframeVO()
                .withId(workframe.getId())
                .withWorkerId(invalidEmployer.getEmployeeDetails().getId())
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 1))
                .withEndHour(DateUtils.getDateWithHour(start, 2))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        mvc.perform(put("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Acest workframe nu iti apartine.")));

        List<WorkframeEntity> workframes = workframeRepository.findAll();
        assertThat(workframes).hasSize(1);
        assertThat(workframes.get(0).getId()).isEqualTo(workframe.getId());
        assertThat(workframes.get(0).getUserEntity().getId()).isEqualTo(employee.getEmployeeDetails().getId());
        assertThat(workframes.get(0).getStartHour().getTime()).isEqualTo(workframe.getStartHour().getTime());
        assertThat(workframes.get(0).getEndHour().getTime()).isEqualTo(workframe.getEndHour().getTime());

        Calendar savedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(workframes.get(0).getStartDate());
        Calendar expectedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(workframe.getStartDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));

        savedDateCalendar.setTime(workframes.get(0).getEndDate());
        expectedDateCalendar.setTime(workframe.getEndDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));
    }

    @Test
    public void updateWorkframes_employeeUpdatesValidWorkframe() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("asd");
        userRepository.save(employee.getOwner());
        employeeRepository.save(employee);
        Date start = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .withUserEntity(employee.getEmployeeDetails())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(employee.getEmployeeDetails());

        WorkframeVO workframeVO = aWorkframeVO()
                .withId(workframe.getId())
                .withWorkerId(employee.getEmployeeDetails().getId())
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 1))
                .withEndHour(DateUtils.getDateWithHour(start, 2))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        mvc.perform(put("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<WorkframeEntity> workframes = workframeRepository.findAll();
        assertThat(workframes).hasSize(1);
        assertThat(workframes.get(0).getId()).isEqualTo(workframeVO.getId());
        assertThat(workframes.get(0).getUserEntity().getId()).isEqualTo(employee.getId());
        assertThat(workframes.get(0).getStartHour().getTime()).isEqualTo(workframeVO.getStartHour().getTime());
        assertThat(workframes.get(0).getEndHour().getTime()).isEqualTo(workframeVO.getEndHour().getTime());

        Calendar savedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(workframes.get(0).getStartDate());
        Calendar expectedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(workframeVO.getStartDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));

        savedDateCalendar.setTime(workframes.get(0).getEndDate());
        expectedDateCalendar.setTime(workframeVO.getEndDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));
    }

    @Test
    public void updateWorkframes_employeeUpdatesInvalidWorkframe() throws Exception {
        EmployeeEntity employee = integrationTestHelper.getEmployeeWithUsername("asd");
        EmployeeEntity invalidEmployer = integrationTestHelper.getEmployeeWithUsernameAndOwnerUsername("anotherEmp", "anotherOwner");
        userRepository.saveAll(newArrayList(employee.getOwner(), invalidEmployer.getOwner()));
        employeeRepository.saveAll(newArrayList(employee, invalidEmployer));
        Date start = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .withUserEntity(employee.getEmployeeDetails())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(employee.getEmployeeDetails());

        WorkframeVO workframeVO = aWorkframeVO()
                .withId(workframe.getId())
                .withWorkerId(invalidEmployer.getEmployeeDetails().getId())
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 1))
                .withEndHour(DateUtils.getDateWithHour(start, 2))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        mvc.perform(put("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Acest workframe nu iti apartine.")));

        List<WorkframeEntity> workframes = workframeRepository.findAll();
        assertThat(workframes).hasSize(1);
        assertThat(workframes.get(0).getId()).isEqualTo(workframe.getId());
        assertThat(workframes.get(0).getUserEntity().getId()).isEqualTo(employee.getEmployeeDetails().getId());

        Calendar savedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(workframes.get(0).getStartDate());
        Calendar expectedDateCalendar = Calendar.getInstance();
        savedDateCalendar.setTime(workframe.getStartDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));

        savedDateCalendar.setTime(workframes.get(0).getEndDate());
        expectedDateCalendar.setTime(workframe.getEndDate());
        assertThat(savedDateCalendar.get(Calendar.DATE)).isEqualTo(expectedDateCalendar.get(Calendar.DATE));

        assertThat(workframes.get(0).getStartHour().getTime()).isEqualTo(workframe.getStartHour().getTime());
        assertThat(workframes.get(0).getEndHour().getTime()).isEqualTo(workframe.getEndHour().getTime());
    }

    @Test
    public void updateWorkframes_whenClient_shouldFail() throws Exception {
        UserEntity client = integrationTestHelper.getUserWithRole(CLIENT_ROLE);
        userRepository.save(client);
        Date start = new Date();
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(new Date(start.getTime()))
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .withUserEntity(client)
                .build();
        workframeRepository.save(workframe);

        final String token = getTokenForUser(client);

        WorkframeVO workframeVO = aWorkframeVO()
                .withId(client.getId())
                .withWorkerId(client.getId())
                .withStartDate(start)
                .withStartHour(DateUtils.getDateWithHour(start, 1))
                .withEndHour(DateUtils.getDateWithHour(start, 2))
                .withEndDate(getDateWithMaxHour(start))
                .build();
        mvc.perform(put("/workframes")
                .header("Authorization", token)
                .content(asJsonString(workframeVO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Nu ai drepturi sa modifici acest workframe.")));
        ;

    }

    @Test
    public void ownerDeleteItsOwnWorkframe() throws Exception {
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("employee");
        Date start = new Date();
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("anUser", CLIENT_ROLE);
        userRepository.saveAll(newArrayList(employeeEntity.getOwner(), client));
        employeeRepository.save(employeeEntity);
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(new Date(start.getTime()))
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .withUserEntity(employeeEntity.getOwner())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(employeeEntity.getOwner());

        mvc.perform(delete("/workframes/" + workframe.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(workframeRepository.findAll()).isEmpty();
    }

    @Test
    public void ownerDeleteEmployeeWorkframe() throws Exception {
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("employee");
        Date start = new Date();
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("anUser", CLIENT_ROLE);
        userRepository.saveAll(newArrayList(employeeEntity.getOwner(), client));
        employeeRepository.save(employeeEntity);
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(new Date(start.getTime()))
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .withUserEntity(employeeEntity.getEmployeeDetails())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(employeeEntity.getOwner());

        mvc.perform(delete("/workframes/" + workframe.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(workframeRepository.findAll()).isEmpty();
    }

    @Test
    public void ownerDeleteAnotherInvalidEmployeeWorkframe() throws Exception {
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("employee");
        EmployeeEntity anotherEmp = integrationTestHelper.getEmployeeWithUsernameAndOwnerUsername("anotherEmp",
                "anotherOwner");
        Date start = new Date();
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("anUser", CLIENT_ROLE);
        userRepository.saveAll(newArrayList(employeeEntity.getOwner(), anotherEmp.getOwner(), client));
        employeeRepository.saveAll(newArrayList(employeeEntity, anotherEmp));
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(new Date(start.getTime()))
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .withUserEntity(anotherEmp.getEmployeeDetails())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(employeeEntity.getOwner());

        mvc.perform(delete("/workframes/" + workframe.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Acest workframe nu iti apartine.")));

        assertThat(workframeRepository.findAll()).hasSize(1);
    }

    @Test
    public void employeeDeleteItsOwnWorkframe() throws Exception {
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("employee");
        Date start = new Date();
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("anUser", CLIENT_ROLE);
        userRepository.saveAll(newArrayList(employeeEntity.getOwner(), client));
        employeeRepository.save(employeeEntity);
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(new Date(start.getTime()))
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .withUserEntity(employeeEntity.getEmployeeDetails())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(employeeEntity.getEmployeeDetails());

        mvc.perform(delete("/workframes/" + workframe.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(workframeRepository.findAll()).isEmpty();
    }

    @Test
    public void employeeDeleteInvalidWorkframe() throws Exception {
        EmployeeEntity employeeEntity = integrationTestHelper.getEmployeeWithUsername("employee");
        EmployeeEntity anotherEmp = integrationTestHelper.getEmployeeWithUsernameAndOwnerUsername("anotherEmp",
                "anotherOwner");
        Date start = new Date();
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("anUser", CLIENT_ROLE);
        userRepository.saveAll(newArrayList(employeeEntity.getOwner(), anotherEmp.getOwner(), client));
        employeeRepository.saveAll(newArrayList(employeeEntity, anotherEmp));
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(new Date(start.getTime()))
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .withUserEntity(anotherEmp.getEmployeeDetails())
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(employeeEntity.getEmployeeDetails());

        mvc.perform(delete("/workframes/" + workframe.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Nu poti sterge programul unui alt angajat.")));

        assertThat(workframeRepository.findAll()).hasSize(1);
    }

    @Test
    public void whenClientDeletesWorkframe_throwException() throws Exception {
        Date start = new Date();
        UserEntity client = integrationTestHelper.getUserWithUserNameAndRole("anUser", CLIENT_ROLE);
        userRepository.save(client);
        WorkframeEntity workframe = aWorkframeEntity()
                .withStartDate(new Date(start.getTime()))
                .withStartHour(DateUtils.getDateWithHour(start, 5))
                .withEndHour(DateUtils.getDateWithHour(start, 15))
                .withEndDate(getDateWithMaxHour(start))
                .withUserEntity(client)
                .build();
        workframeRepository.save(workframe);
        final String token = getTokenForUser(client);

        mvc.perform(delete("/workframes/" + workframe.getId())
                .header("Authorization", token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0]", is("Nu ai drepturi sa stergi acest program.")));

        assertThat(workframeRepository.findAll()).hasSize(1);
    }
}
