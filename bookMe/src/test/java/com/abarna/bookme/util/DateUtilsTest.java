package com.abarna.bookme.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.abarna.bookme.enums.AppointmentTimeframeType.QUARTER;
import static com.abarna.bookme.util.DateUtils.getDateWithMaxHour;
import static com.abarna.bookme.util.DateUtils.getDateWithMinHour;
import static com.abarna.bookme.util.DateUtils.getNextTimeframe;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class DateUtilsTest {

    @Test
    public void minHours() {
        Date dateWithMinHour = getDateWithMinHour(new Date());
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateWithMinHour);
        assertThat(cal.get(Calendar.HOUR_OF_DAY)).isEqualTo(0);
        assertThat(cal.get(Calendar.MINUTE)).isEqualTo(0);
        assertThat(cal.get(Calendar.SECOND)).isEqualTo(0);
        assertThat(cal.get(Calendar.MILLISECOND)).isEqualTo(0);
    }

    @Test
    public void maxHours() {
        Date dateWithMinHour = getDateWithMaxHour(new Date());
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateWithMinHour);
        assertThat(cal.get(Calendar.HOUR_OF_DAY)).isEqualTo(23);
        assertThat(cal.get(Calendar.MINUTE)).isEqualTo(59);
        assertThat(cal.get(Calendar.SECOND)).isEqualTo(59);
        assertThat(cal.get(Calendar.MILLISECOND)).isEqualTo(999);
    }

    @Test
    public void getNTimeFrames() {
        Date dateWithMinHour = getDateWithMinHour(new Date());
        List<Date> nextNTimeframes = DateUtils.getNextNTimeFrames(dateWithMinHour, QUARTER, 4);
        assertThat(nextNTimeframes).hasSize(4);

        Date firstTimeframe = getNextTimeframe(dateWithMinHour, QUARTER);
        assertThat(nextNTimeframes).contains(firstTimeframe);

        Date secondTimeframe = getNextTimeframe(firstTimeframe, QUARTER);
        assertThat(nextNTimeframes).contains(secondTimeframe);

        Date thirdTimeframe = getNextTimeframe(secondTimeframe, QUARTER);
        assertThat(nextNTimeframes).contains(thirdTimeframe);

        assertThat(nextNTimeframes).contains(getNextTimeframe(thirdTimeframe, QUARTER));
    }
}
