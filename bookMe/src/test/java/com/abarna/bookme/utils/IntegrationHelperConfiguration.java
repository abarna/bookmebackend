package com.abarna.bookme.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.mockito.Mockito.mock;

@Configuration
@ComponentScan(basePackageClasses = {IntegrationTestHelper.class})
@Profile({ "repoIT" })
public class IntegrationHelperConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder(){
        return mock(PasswordEncoder.class);
    }
}
