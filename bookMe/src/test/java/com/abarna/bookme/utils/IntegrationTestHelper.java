package com.abarna.bookme.utils;

import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.RoleEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.enums.RolesEnum;
import com.abarna.bookme.repositories.EmployeeRepository;
import com.abarna.bookme.repositories.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.abarna.bookme.entities.EmployeeEntityBuilder.anEmployeeEntity;
import static com.abarna.bookme.entities.UserEntityBuilder.anUserEntity;

@Component
public class IntegrationTestHelper {
    public static final String RANDOM_PASS = "aRandomPass";
    @Autowired
    private PasswordEncoder passwordEncoder;

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public UserEntity getUserWithRole(RolesEnum rolesEnum) {
        List<RoleEntity> employeeRoles = new ArrayList<>();
        RoleEntity employeeRole = new RoleEntity();
        employeeRole.setRole(rolesEnum);
        employeeRoles.add(employeeRole);
        UserEntity userEntity = anUserEntity()
                .withEmail("asda")
                .withFirstName("asda")
                .withLastName("asda")
                .withRoles(employeeRoles)
                .withEncryptedPassword(passwordEncoder.encode(RANDOM_PASS))
                .withUsername("user")
                .withValidated(true)
                .build();
        if (RolesEnum.ADMIN_ROLE.equals(rolesEnum)) {
            userEntity.setIsOwner(true);
        }
        return userEntity;
    }

    public UserEntity getUserWithUserNameAndRole(String username, RolesEnum rolesEnum) {
        UserEntity user = getUserWithRole(rolesEnum);
        user.setUsername(username);
        return user;
    }

    public EmployeeEntity getEmployeeWithUsername(String username) {
        UserEntity employeeDetails = getUserWithUserNameAndRole(username,
                RolesEnum.EMPLOYEE_ROLE);
        UserEntity owner = getUserWithRole(RolesEnum.ADMIN_ROLE);
        owner.setUsername("owner");
        return anEmployeeEntity()
                .withOwner(owner)
                .withEmployeeDetails(employeeDetails)
                .build();
    }

    public EmployeeEntity getEmployeeWithUsernameAndOwnerUsername(String username, String ownerUsername) {
        EmployeeEntity employeeWithUsernameAndPassword = getEmployeeWithUsername(username);
        employeeWithUsernameAndPassword.getOwner().setUsername(ownerUsername);
        return employeeWithUsernameAndPassword;
    }
}
