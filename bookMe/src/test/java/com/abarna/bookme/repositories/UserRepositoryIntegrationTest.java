package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.RoleEntity;
import com.abarna.bookme.entities.ShopEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.enums.RolesEnum;
import com.abarna.bookme.utils.IntegrationHelperConfiguration;
import com.abarna.bookme.utils.IntegrationTestHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static com.abarna.bookme.entities.ShopEntityBuilder.aShopEntity;
import static com.abarna.bookme.entities.UserEntityBuilder.anUserEntity;
import static com.abarna.bookme.enums.RolesEnum.ADMIN_ROLE;
import static com.abarna.bookme.enums.RolesEnum.EMPLOYEE_ROLE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(IntegrationHelperConfiguration.class)
@ActiveProfiles("repoIT")
public class UserRepositoryIntegrationTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private IntegrationTestHelper integrationTestHelper;

    @After
    public void setUp() {
        userRepository.deleteAll();
        shopRepository.deleteAll();
    }

    @Test
    public void findOne() {
        UserEntity owner = integrationTestHelper.getUserWithRole(EMPLOYEE_ROLE);
        UserEntity notDesiredUser = integrationTestHelper.getUserWithUserNameAndRole("asd", EMPLOYEE_ROLE);

        userRepository.saveAll(newArrayList(owner, notDesiredUser));

        assertThat(userRepository.findByUsername(owner.getUsername()).get()).isEqualTo(owner);
    }

    @Test
    public void findByShopId() {
        ShopEntity shopEntity = aShopEntity().build();
        ShopEntity shopEntity2 = aShopEntity().build();
        shopRepository.saveAll(newArrayList(shopEntity, shopEntity2));

        UserEntity owner = integrationTestHelper.getUserWithUserNameAndRole("user", EMPLOYEE_ROLE);
        owner.setShop(shopEntity);

        UserEntity notDesiredUser = integrationTestHelper.getUserWithUserNameAndRole("anotherUser", EMPLOYEE_ROLE);
        notDesiredUser.setShop(shopEntity2);

        userRepository.saveAll(newArrayList(owner, notDesiredUser));

        assertThat(userRepository.findByShop_Id(shopEntity.getId()))
                .isEqualTo(owner);
    }

    @Test
    public void findShopOwner() {
        ShopEntity shopEntity = aShopEntity().withId(1).build();
        shopRepository.save(shopEntity);

        UserEntity owner = integrationTestHelper.getUserWithRole(ADMIN_ROLE);
        owner.setShop(shopEntity);
        owner.setIsOwner(true);
        UserEntity notDesiredUser = integrationTestHelper.getUserWithRole(EMPLOYEE_ROLE);
        userRepository.saveAll(newArrayList(owner, notDesiredUser));

        assertThat(userRepository.findShopOwner(shopEntity.getId()))
                .isEqualTo(owner);
    }
}
