package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.entities.WorkframeEntity;
import com.abarna.bookme.enums.RolesEnum;
import com.abarna.bookme.util.DateUtils;
import com.abarna.bookme.utils.IntegrationHelperConfiguration;
import com.abarna.bookme.utils.IntegrationTestHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.abarna.bookme.entities.WorkframeEntityBuilder.aWorkframeEntity;
import static com.abarna.bookme.enums.AppointmentTimeframeType.HOUR;
import static com.abarna.bookme.util.DateUtils.getDateWithHour;
import static com.abarna.bookme.util.DateUtils.getDateWithMinHour;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(IntegrationHelperConfiguration.class)
@ActiveProfiles("repoIT")
public class WorkframeEntityRepositoryIntegrationTest {

    @Autowired
    private WorkframeRepository workframeRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private IntegrationTestHelper integrationTestHelper;

    @Test
    public void getEmployeeSchedulerByDate() {
        UserEntity userEntity = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);
        userRepository.save(userEntity);
        Date start = getDateWithMinHour(new Date());
        Calendar cal = Calendar.getInstance();
        cal.setTime(start);
        cal.add(Calendar.DATE, 5);
        WorkframeEntity workframeEntity = aWorkframeEntity()
                .withUserEntity(userEntity)
                .withStartDate(start)
                .withStartHour(getDateWithHour(start, 9))
                .withEndHour(getDateWithHour(start, 18))
                .withEndDate(cal.getTime())
                .build();

        WorkframeEntity workframeEntity2 = aWorkframeEntity()
                .withUserEntity(userEntity)
                .withStartDate(workframeEntity.getEndDate())
                .withEndDate(DateUtils.getNextTimeframe(workframeEntity.getEndDate(), HOUR))
                .build();
        workframeRepository.saveAll(newArrayList(workframeEntity, workframeEntity2));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String stringDate = dateFormat.format(start);
        assertThat(workframeRepository.findByUserEntity_IdAndDate(userEntity.getId(), stringDate).get())
                .isEqualTo(workframeEntity);
    }

    @Test
    public void findAvailablesByUserEntity_IdAndDate() {
        UserEntity userEntity = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);
        userRepository.save(userEntity);
        Date start = new Date();
        Calendar twoDaysAddedCalendar = Calendar.getInstance();
        twoDaysAddedCalendar.setTime(start);
        twoDaysAddedCalendar.add(Calendar.DATE, 2);
        Calendar twoDaysAgoCalendar = Calendar.getInstance();
        twoDaysAgoCalendar.setTime(start);
        twoDaysAgoCalendar.add(Calendar.DATE, -2);
        Calendar fourDaysAddedCalendar = Calendar.getInstance();
        fourDaysAddedCalendar.setTime(start);
        fourDaysAddedCalendar.add(Calendar.DATE, 4);
        WorkframeEntity twoDaysAgoWorkframe = aWorkframeEntity()
                .withUserEntity(userEntity)
                .withStartDate(twoDaysAgoCalendar.getTime())
                .withStartHour(getDateWithHour(twoDaysAgoCalendar.getTime(), 9))
                .withEndHour(getDateWithHour(twoDaysAgoCalendar.getTime(), 18))
                .withEndDate(start)
                .build();
        WorkframeEntity fromTodayFor2Days = aWorkframeEntity()
                .withUserEntity(userEntity)
                .withStartDate(start)
                .withStartHour(getDateWithHour(start, 9))
                .withEndHour(getDateWithHour(start, 18))
                .withEndDate(twoDaysAddedCalendar.getTime())
                .build();

        WorkframeEntity fromTwoDaysAddedToFourDaysAdded = aWorkframeEntity()
                .withUserEntity(userEntity)
                .withStartDate(twoDaysAddedCalendar.getTime())
                .withStartHour(getDateWithHour(start, 9))
                .withEndHour(getDateWithHour(start, 18))
                .withEndDate(fourDaysAddedCalendar.getTime())
                .build();
        workframeRepository.saveAll(newArrayList(twoDaysAgoWorkframe, fromTodayFor2Days, fromTwoDaysAddedToFourDaysAdded));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String stringDate = dateFormat.format(start);
        assertThat(workframeRepository.findAvailableWorkframesByUserEntity_IdAndDate(userEntity.getId(), stringDate))
                .containsOnly(fromTodayFor2Days, fromTwoDaysAddedToFourDaysAdded);
    }

    @Test
    public void getNextWorkframeAvailable() {
        Date start = new Date();
        UserEntity userEntity = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);
        userRepository.save(userEntity);
        Calendar twoDaysAddedCalendar = Calendar.getInstance();
        twoDaysAddedCalendar.setTime(start);
        twoDaysAddedCalendar.add(Calendar.DATE, 2);
        Calendar threeDaysAddedCalendar = Calendar.getInstance();
        threeDaysAddedCalendar.setTime(start);
        threeDaysAddedCalendar.add(Calendar.DATE, 3);
        WorkframeEntity twoDaysAddedWorkframe = aWorkframeEntity()
                .withUserEntity(userEntity)
                .withStartDate(twoDaysAddedCalendar.getTime())
                .withStartHour(getDateWithHour(twoDaysAddedCalendar.getTime(), 9))
                .withEndHour(getDateWithHour(twoDaysAddedCalendar.getTime(), 18))
                .withEndDate(twoDaysAddedCalendar.getTime())
                .build();
        WorkframeEntity anotherWorkframe = aWorkframeEntity()
                .withUserEntity(userEntity)
                .withStartDate(threeDaysAddedCalendar.getTime())
                .withStartHour(getDateWithHour(threeDaysAddedCalendar.getTime(), 9))
                .withEndHour(getDateWithHour(threeDaysAddedCalendar.getTime(), 18))
                .withEndDate(threeDaysAddedCalendar.getTime())
                .build();
        workframeRepository.save(twoDaysAddedWorkframe);

        assertThat(workframeRepository.findFirstByUserEntity_IdAndStartDateIsGreaterThanEqualOrderByStartDate(
                userEntity.getId(),
                new Date()
        )).isEqualTo(twoDaysAddedWorkframe);
    }
}
