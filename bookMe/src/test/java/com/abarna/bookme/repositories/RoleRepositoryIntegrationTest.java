package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.RoleEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.enums.RolesEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static com.abarna.bookme.entities.UserEntityBuilder.anUserEntity;
import static com.abarna.bookme.enums.RolesEnum.ADMIN_ROLE;
import static com.abarna.bookme.enums.RolesEnum.EMPLOYEE_ROLE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RoleRepositoryIntegrationTest {

    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void findEmployeeRole() {
        RoleEntity employeeRole = new RoleEntity();
        employeeRole.setRole(EMPLOYEE_ROLE);

        RoleEntity adminRole = new RoleEntity();
        adminRole.setRole(ADMIN_ROLE);

        roleRepository.saveAll(newArrayList(employeeRole, adminRole));

        assertThat(roleRepository.findByRole(EMPLOYEE_ROLE).get()).isEqualTo(employeeRole);
    }

}
