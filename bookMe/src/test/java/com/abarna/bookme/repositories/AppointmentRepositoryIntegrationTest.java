package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.AppointmentEntity;
import com.abarna.bookme.entities.AppointmentEntityBuilder;
import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.ShopEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.util.DateUtils;
import com.abarna.bookme.utils.IntegrationHelperConfiguration;
import com.abarna.bookme.utils.IntegrationTestHelper;
import org.junit.After;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static com.abarna.bookme.entities.AppointmentEntityBuilder.anAppointmentEntity;
import static com.abarna.bookme.entities.ShopEntityBuilder.aShopEntity;
import static com.abarna.bookme.enums.RolesEnum.CLIENT_ROLE;
import static com.abarna.bookme.enums.RolesEnum.EMPLOYEE_ROLE;
import static com.abarna.bookme.util.DateUtils.getDateWithMaxHour;
import static com.abarna.bookme.util.DateUtils.getDateWithMinHour;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.DateUtil.now;
import static org.assertj.core.util.Lists.newArrayList;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(IntegrationHelperConfiguration.class)
@ActiveProfiles("repoIT")
public class AppointmentRepositoryIntegrationTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private AppointmentRepository appointmentRepository;
    @Autowired
    private IntegrationTestHelper integrationTestHelper;

    @AfterEach
    public void tearDown() {
        userRepository.deleteAll();
        employeeRepository.deleteAll();
        appointmentRepository.deleteAll();
    }

    @Test
    public void findAppointmentsByEmployeeId_afterDate() {
        UserEntity employee = integrationTestHelper.getUserWithRole(EMPLOYEE_ROLE);
        UserEntity anotherEmployee = integrationTestHelper.getUserWithUserNameAndRole("anotherEmp",
                EMPLOYEE_ROLE);
        UserEntity aClient = integrationTestHelper.getUserWithUserNameAndRole("asd", CLIENT_ROLE);
        userRepository.saveAll(newArrayList(employee, anotherEmployee, aClient));
        Date currentDate = new Date();

        AppointmentEntity appointment = anAppointmentEntity()
                .withClient(aClient)
                .withWorker(employee)
                .withStart(currentDate)
                .build();
        AppointmentEntity anotherAppointment = anAppointmentEntity()
                .withClient(aClient)
                .withWorker(anotherEmployee)
                .withStart(currentDate)
                .build();

        AppointmentEntity appointmentWithWrongDate = anAppointmentEntity()
                .withClient(aClient)
                .withWorker(employee)
                .withStart(new Date(getDateWithMinHour(currentDate).getTime() - 1000))
                .withEnd(currentDate)
                .build();

        appointmentRepository.saveAll(newArrayList(appointment, anotherAppointment, appointmentWithWrongDate));

        assertThat(appointmentRepository.findByWorker_IdAndStartGreaterThanEqualAndStartLessThanEqual(employee.getId(),
                getDateWithMinHour(currentDate), getDateWithMaxHour(currentDate)))
                .containsOnly(appointment);
    }

    @Test
    public void getWorkerAppointmentsEmptyList() {
        UserEntity employee = integrationTestHelper.getUserWithRole(EMPLOYEE_ROLE);
        UserEntity anotherEmployee = integrationTestHelper.getUserWithUserNameAndRole("anotherEmp",
                EMPLOYEE_ROLE);
        userRepository.saveAll(newArrayList(employee, anotherEmployee));

        AppointmentEntity appointment = anAppointmentEntity()
                .withClient(employee)
                .withWorker(employee)
                .withStart(getDateWithMaxHour(new Date()))
                .build();
        AppointmentEntity appointmentInThePast = anAppointmentEntity()
                .withClient(employee)
                .withWorker(anotherEmployee)
                .withStart(getDateWithMinHour(new Date()))
                .build();

        AppointmentEntity appointmentWithAnotherEmployee = anAppointmentEntity()
                .withClient(anotherEmployee)
                .withWorker(anotherEmployee)
                .withStart(getDateWithMaxHour(new Date()))
                .build();
        appointmentRepository.saveAll(newArrayList(appointment, appointmentInThePast, appointmentWithAnotherEmployee));

        assertThat(appointmentRepository.findByWorker_IdAndStartGreaterThanEqual(employee.getId(), new Date())).containsOnly(appointment);

    }

    @Test
    public void getWorkerAppointmentsShouldRetrieveOnlyTheOnesFromFuture() {
        UserEntity client = integrationTestHelper.getUserWithRole(CLIENT_ROLE);
        UserEntity employee = integrationTestHelper.getUserWithUserNameAndRole("emp",
                EMPLOYEE_ROLE);
        UserEntity anotherEmp = integrationTestHelper.getUserWithUserNameAndRole("anotherEmp",
                EMPLOYEE_ROLE);
        userRepository.saveAll(newArrayList(client, employee, anotherEmp));

        AppointmentEntity appointmentInThePast = anAppointmentEntity()
                .withClient(client)
                .withWorker(employee)
                .withStart(getDateWithMinHour(new Date()))
                .build();

        AppointmentEntity appointmentWithAnotherEmployee = anAppointmentEntity()
                .withClient(employee)
                .withWorker(anotherEmp)
                .withStart(getDateWithMaxHour(new Date()))
                .build();
        appointmentRepository.saveAll(newArrayList(appointmentInThePast, appointmentWithAnotherEmployee));

        assertThat(appointmentRepository.findByWorker_IdAndStartGreaterThanEqual(employee.getId(), new Date())).isEmpty();
    }

    @Test
    public void getClientAppointmentsShouldRetrieveOnlyTheOnesFromFuture() {
        UserEntity client = integrationTestHelper.getUserWithRole(CLIENT_ROLE);
        UserEntity employee = integrationTestHelper.getUserWithUserNameAndRole("anotherEmp",
                EMPLOYEE_ROLE);
        userRepository.saveAll(newArrayList(client, employee));

        AppointmentEntity appointment = anAppointmentEntity()
                .withClient(client)
                .withWorker(employee)
                .withStart(getDateWithMaxHour(new Date()))
                .build();
        AppointmentEntity appointmentInThePast = anAppointmentEntity()
                .withClient(employee)
                .withWorker(employee)
                .withStart(getDateWithMinHour(new Date()))
                .build();

        AppointmentEntity appointmentWithAnotherEmployee = anAppointmentEntity()
                .withClient(employee)
                .withWorker(employee)
                .withStart(getDateWithMaxHour(new Date()))
                .build();
        appointmentRepository.saveAll(newArrayList(appointment, appointmentInThePast, appointmentWithAnotherEmployee));

        assertThat(appointmentRepository.findByClient_IdAndStartGreaterThanEqual(client.getId(), new Date())).containsOnly(appointment);
    }
}
