package com.abarna.bookme.repositories;

import com.abarna.bookme.entities.EmployeeEntity;
import com.abarna.bookme.entities.UserEntity;
import com.abarna.bookme.enums.RolesEnum;
import com.abarna.bookme.utils.IntegrationHelperConfiguration;
import com.abarna.bookme.utils.IntegrationTestHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static com.abarna.bookme.entities.EmployeeEntityBuilder.anEmployeeEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(IntegrationHelperConfiguration.class)
@ActiveProfiles("repoIT")
public class EmployeeRepositoryIntegrationTest {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private IntegrationTestHelper integrationTestHelper;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void findByOwnerId() {
        UserEntity owner = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        UserEntity employeeDetails = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);

        EmployeeEntity employeeEntity = anEmployeeEntity()
                .withEmployeeDetails(employeeDetails)
                .withOwner(owner)
                .build();
        // add not desired employee with another owner
        UserEntity owner2 = integrationTestHelper.getUserWithRole(RolesEnum.ADMIN_ROLE);
        UserEntity employeeDetails2 = integrationTestHelper.getUserWithRole(RolesEnum.EMPLOYEE_ROLE);

        EmployeeEntity notDesiredEmployee = anEmployeeEntity()
                .withEmployeeDetails(employeeDetails2)
                .withOwner(owner2)
                .build();
        userRepository.saveAll(newArrayList(employeeEntity.getOwner(),notDesiredEmployee.getOwner()));
        employeeRepository.saveAll(newArrayList(employeeEntity, notDesiredEmployee));

        assertThat(employeeRepository.findByOwner_Id(owner.getId())).containsOnly(employeeEntity);
    }

}
